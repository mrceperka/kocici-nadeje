-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `address`;
CREATE TABLE `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `street` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `animal`;
CREATE TABLE `animal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  `gender` tinyint(4) NOT NULL,
  `state` varchar(64) NOT NULL DEFAULT 'new',
  `castrated` tinyint(4) NOT NULL,
  `animal_type_id` int(11) NOT NULL,
  `depo_id` int(11) DEFAULT NULL,
  `created_by_id` int(11) NOT NULL,
  `profile_image_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `depo_id` (`depo_id`),
  KEY `animal_type_id` (`animal_type_id`),
  KEY `created_by_id` (`created_by_id`),
  KEY `profile_image_id` (`profile_image_id`),
  CONSTRAINT `animal_ibfk_1` FOREIGN KEY (`depo_id`) REFERENCES `depo` (`id`),
  CONSTRAINT `animal_ibfk_2` FOREIGN KEY (`animal_type_id`) REFERENCES `animal_type` (`id`),
  CONSTRAINT `animal_ibfk_3` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`),
  CONSTRAINT `animal_ibfk_4` FOREIGN KEY (`profile_image_id`) REFERENCES `user_image` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `animal_type`;
CREATE TABLE `animal_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `popularity` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `animal_x_handicap`;
CREATE TABLE `animal_x_handicap` (
  `animal_id` int(11) NOT NULL,
  `handicap_id` int(11) NOT NULL,
  PRIMARY KEY (`animal_id`,`handicap_id`),
  KEY `handicap_id` (`handicap_id`),
  KEY `animal_id` (`animal_id`),
  CONSTRAINT `animal_x_handicap_ibfk_1` FOREIGN KEY (`animal_id`) REFERENCES `animal` (`id`),
  CONSTRAINT `animal_x_handicap_ibfk_2` FOREIGN KEY (`handicap_id`) REFERENCES `handicap` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `animal_x_story`;
CREATE TABLE `animal_x_story` (
  `animal_id` int(11) NOT NULL,
  `story_id` int(11) NOT NULL,
  PRIMARY KEY (`animal_id`,`story_id`),
  KEY `story_id` (`story_id`),
  CONSTRAINT `animal_x_story_ibfk_1` FOREIGN KEY (`animal_id`) REFERENCES `animal` (`id`),
  CONSTRAINT `animal_x_story_ibfk_2` FOREIGN KEY (`story_id`) REFERENCES `story` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `animal_x_user_image`;
CREATE TABLE `animal_x_user_image` (
  `animal_id` int(11) NOT NULL,
  `user_image_id` int(11) NOT NULL,
  PRIMARY KEY (`animal_id`,`user_image_id`),
  KEY `user_image_id` (`user_image_id`),
  CONSTRAINT `animal_x_user_image_ibfk_1` FOREIGN KEY (`animal_id`) REFERENCES `animal` (`id`),
  CONSTRAINT `animal_x_user_image_ibfk_2` FOREIGN KEY (`user_image_id`) REFERENCES `user_image` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `contact`;
CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `telephone` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `created_by_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by_id` (`created_by_id`),
  CONSTRAINT `contact_ibfk_1` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `depo`;
CREATE TABLE `depo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `capacity` int(11) NOT NULL,
  `address_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `address_id` (`address_id`),
  CONSTRAINT `depo_ibfk_1` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `depo_x_contact`;
CREATE TABLE `depo_x_contact` (
  `depo_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  PRIMARY KEY (`depo_id`,`contact_id`),
  KEY `contact_id` (`contact_id`),
  CONSTRAINT `depo_x_contact_ibfk_1` FOREIGN KEY (`depo_id`) REFERENCES `depo` (`id`),
  CONSTRAINT `depo_x_contact_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `handicap`;
CREATE TABLE `handicap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `severity` tinyint(4) NOT NULL DEFAULT '1',
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `static_content`;
CREATE TABLE `static_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text,
  `section` tinyint(3) unsigned NOT NULL,
  `position` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `story`;
CREATE TABLE `story` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `type` tinyint(4) NOT NULL,
  `created_by_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `creator_id` (`created_by_id`),
  CONSTRAINT `story_ibfk_1` FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `user_depo`;
CREATE TABLE `user_depo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `depo_id` int(11) NOT NULL,
  `relation` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `depo_id` (`depo_id`),
  CONSTRAINT `user_depo_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `user_depo_ibfk_2` FOREIGN KEY (`depo_id`) REFERENCES `depo` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `user_image`;
CREATE TABLE `user_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hash` varchar(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(10) NOT NULL,
  `user_id` int(11) NOT NULL,
  `source_x` int(11) DEFAULT NULL,
  `source_y` int(11) DEFAULT NULL,
  `source_width` int(11) DEFAULT NULL,
  `source_height` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_image_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `user_x_contact`;
CREATE TABLE `user_x_contact` (
  `user_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`contact_id`),
  KEY `contact_id` (`contact_id`),
  CONSTRAINT `user_x_contact_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `user_x_contact_ibfk_2` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 2017-02-14 21:00:02