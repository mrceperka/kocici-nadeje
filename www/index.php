<?php

if (false) {
	require_once __DIR__ . '/.maintenance.php';
	exit;
}

define('WWW_DIR', __DIR__ . '/');
define('ROOT_DIR', __DIR__ . "/../../");
define('APP_DIR', __DIR__ . "/../");

header_remove("Server");
header_remove("X-Powered-By");

header("Connection: Keep-Alive");

ob_start("ob_gzhandler");

$container = require __DIR__ . '/../app/bootstrap.php';

$container->getByType(Nette\Application\Application::class)
	->run();
