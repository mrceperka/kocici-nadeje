$(document).ready(function () {
    $.nette.init();
    $(".button-collapse").sideNav();
    $('.dropdown').dropdown({
            inDuration: 300,
            outDuration: 225,
            constrain_width: false, // Does not change width of dropdown to that of the activator
            hover: true, // Activate on hover
            gutter: 0, // Spacing from edge
            belowOrigin: false, // Displays dropdown below the button
            alignment: 'left' // Displays dropdown with edge aligned to the left of button
        }
    );
    $('select').material_select();
    $('.modal').modal();

    lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true,
        'albumLabel': 'Obrázek %1 z %2'
    });

    /*//Nette.validateForm not present fix
    try {
        if(typeof Nette.validateForm === 'function') {
            //leave it as it is
        } else {
            Nette = {};
            Nette.validateForm = function () {
                return true;
            }
        }
    } catch (e) {
        Nette = {};
        Nette.validateForm = function () {
            return true;
        }
    }*/

});
