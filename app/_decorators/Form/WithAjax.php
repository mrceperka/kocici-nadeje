<?php
namespace Kocky\Decorators\Form;

use Nette\Forms\Form;

class WithAjax implements IFormDecorator
{
	public static function decorate(Form $form, $data = []) {
		$classes = $form->elementPrototype->getAttribute('class');
		$form->elementPrototype->setAttribute('class', $classes . ' ajax');
		return $form;
	}
}