<?php
namespace Kocky\Decorators\Form;
use Nette\Forms\Form;

interface IFormDecorator {
	public static function decorate(Form $form, $data);
}