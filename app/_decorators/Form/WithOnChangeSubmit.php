<?php
namespace Kocky\Decorators\Form;

use Nette\Forms\Form;

class WithOnChangeSubmit implements IFormDecorator
{
	public static function decorate(Form $form, $selector) {
		$component = $form->getComponent($selector);
		$id = 'decorated-frm-' . uniqid();
		
		if($form->getElementPrototype()->id === null) {
			$form->getElementPrototype()->id = $id;
		} else {
			$id = $form->getElementPrototype()->id;
		}
		
		$component->setAttribute(
			'onchange',
			'(function() {
			 	var form = document.getElementById("' . $id . '");
			 	if(form) {
			 		var submit = form.querySelector("input[type=submit");
			 		if(submit) {
			 			if (submit.onclick) {
						   submit.onclick();
						} else if (submit.click) {
						   submit.click();
						}
			 		} else {
			 			form.submit();
			 			console.warn("form id: ' . $id . ' appears to have no submit button");
			 		}
			 	} else {
			 		console.warn("form id: ' . $id . ' was not found");
				}
			}).call(this);'
		);
		
		return $form;
	}
}