<?php
namespace Kocky\AnimalTypeModule\Presenters;

use Kocky\Components\Control\AnimalType\IAnimalTypeControlFactory;
use Kocky\Model\Entity\AnimalType;
use Kocky\Model\Repository\AnimalTypeRepository;
use Nette\Application\BadRequestException;

class DefaultPresenter extends BasePresenter
{
	/** @var  AnimalTypeRepository @inject */
	public $animalTypeRepo;
	
	/** @var  IAnimalTypeControlFactory @inject */
	public $animalTypeControlFactory;
	
	/** @var  AnimalType */
	public $animalType;
	
	public function actionList()
	{
		$this->template->animalTypes = $this->animalTypeRepo->findAll();
	}
	
	public function actionEdit($id)
	{
		$this->animalType = $this->animalTypeRepo->getById($id);
		if(!$this->animalType) {
			throw new BadRequestException(404, 'AnimalType does not exist');
		}
	}
	
	public function renderEdit($id)
	{
		$this->template->animalType = $this->animalType;
	}
	
	public function createComponentAddAnimalType()
	{
		return $this->animalTypeControlFactory->create();
	}
	
	public function createComponentEditAnimalType()
	{
		$atc = $this->animalTypeControlFactory->create();
		$atc->setAnimalType($this->animalType);
		return $atc;
	}
}