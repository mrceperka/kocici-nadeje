<?php

require __DIR__ . '/../vendor/autoload.php';

$configurator = new Nette\Configurator;

function dd(){
	\Tracy\Debugger::$maxDepth = 10;
	\Tracy\Debugger::barDump(func_get_args());
}

//$configurator->setDebugMode('23.75.345.200'); // enable for your remote IP
if(file_exists(__DIR__ . '/.debug_on')) {
	$configurator->setDebugMode(true);
} else {
	$configurator->setDebugMode(false);
}

$configurator->enableDebugger(__DIR__ . '/../log');

$configurator->setTimeZone('Europe/Prague');

if(php_sapi_name() === 'cli') {
	$configurator->setTempDirectory(__DIR__ . '/../tempcli');
} else {
	$configurator->setTempDirectory(__DIR__ . '/../temp');
}

$robotLoader = $configurator->createRobotLoader()
	->addDirectory(__DIR__)
	->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');
//$configurator->addConfig(__DIR__ . '/config/config.local.neon');



\Tracy\Debugger::$onFatalError[] = function () use ($robotLoader) {
	$robotLoader->rebuild();
};


$container = $configurator->createContainer();
$container->getService('application')->errorPresenter = 'Common:Error';

return $container;
