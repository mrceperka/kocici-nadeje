<?php
namespace Kocky\DepoModule\Presenters;

use Kocky\Components\Control\Depo\Member\AE\IDDepoMemberEditControlFactory;
use Kocky\Model\Entity\UserDepo;
use Kocky\Model\Repository\UserDepoRepository;
use Nette\Application\BadRequestException;

class MemberPresenter extends BasePresenter
{
	/** @var  UserDepo */
	public $userDepo;
	
	/** @var  UserDepoRepository @inject */
	public $userDepoRepo;
	
	/** @var  IDDepoMemberEditControlFactory @inject */
	public $depoMemberEditControlFactory;
	
	
	public function actionEdit($id)
	{
		$this->userDepo = $this->userDepoRepo->getById($id);
		if ($this->userDepo) {
			if ($this->authorizator->isAllowed($this->user, $this->userDepo->depo, 'editMember')) {
				
			} else {
				$this->flashMessage('Nedostatečná oprávnění');
				$this->redirect(':Common:Homepage:default');
			}
		} else {
			throw new BadRequestException('UserDepo not found', 404);
		}
		
		
	}
	
	public function renderEdit($id)
	{
		$this->template->u = $this->userDepo->user;
		$this->template->depo = $this->userDepo->depo;
		$this->template->udp = $this->userDepo;
	}
	
	public function createComponentEditForm()
	{
		$c = $this->depoMemberEditControlFactory->create();
		$c->setUserDepo($this->userDepo);
		return $c;
	}
	
}