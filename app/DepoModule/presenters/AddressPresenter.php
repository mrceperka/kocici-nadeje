<?php
namespace Kocky\DepoModule\Presenters;

use Kocky\Components\Control\Depo\Address\IAddressControlFactory;
use Kocky\Model\Entity\Address;
use Kocky\Model\Entity\Depo;
use Kocky\Model\Repository\AddressRepository;
use Kocky\Model\Repository\DepoRepository;
use Nette\Application\BadRequestException;

class AddressPresenter extends BasePresenter
{
	/** @var  DepoRepository @inject */
	public $depoRepo;
	
	/** @var  IAddressControlFactory @inject */
	public $addressControlFactory;
	
	/** @var  AddressRepository @inject */
	public $addressRepo;
	
	/** @var  Depo */
	public $depo;
	
	/** @var Address */
	public $address;
	
	public function actionAdd($id)
	{
		$this->depo = $this->depoRepo->getById($id);
		if(!$this->depo) {
			throw new BadRequestException('Depo does not exits');
		}
		if($this->depo->address !== null) {
			$this->flashMessage('Depozitum již jednu adresu má', 'warn');
			$this->redirect(':Depo:Default:default', ['id' => $this->depo->id]);
		}
	}
	
	public function renderAdd($id)
	{
		$this->template->depo = $this->depo;
	}
	
	public function actionEdit($id, $id2)
	{
		$this->depo = $this->depoRepo->getById($id);
		if(!$this->depo) {
			throw new BadRequestException('Depo does not exits');
		}
		
		$this->address = $this->addressRepo->getById($id2);
		if(!$this->address) {
			throw new BadRequestException('Address does not exits');
		}
	}
	
	public function renderEdit($id, $id2)
	{
		$this->template->depo = $this->depo;
		$this->template->address = $this->address;
	}
	
	public function createComponentAddDepoAddress()
	{
		$adc = $this->addressControlFactory->create();
		$adc->setDepo($this->depo);
		return $adc;
	}
	
	public function createComponentEditDepoAddress()
	{
		$adc = $this->addressControlFactory->create();
		$adc->setDepo($this->depo);
		$adc->setAddress($this->address);
		
		return $adc;
	}
	
}