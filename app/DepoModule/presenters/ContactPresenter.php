<?php
namespace Kocky\DepoModule\Presenters;

use Kocky\Components\Control\Depo\Contact\IDepoContactControlFactory;
use Kocky\Model\Entity\Contact;
use Kocky\Model\Entity\Depo;
use Kocky\Model\Repository\ContactRepository;
use Kocky\Model\Repository\DepoRepository;
use Kocky\Model\Service\DepoService;
use Nette\Application\BadRequestException;

class ContactPresenter extends BasePresenter
{
	/** @var  IDepoContactControlFactory @inject */
	public $depoContactControlFactory;
	
	/** @var  DepoRepository @inject */
	public $depoRepo;
	
	/** @var  ContactRepository @inject */
	public $contactRepo;
	
	/** @var  DepoService @inject */
	public $depoService;
	
	/** @var  Contact */
	public $contact;
	
	/** @var  Depo */
	public $depo;
	
	public function actionAdd($id)
	{
		$this->depo = $this->depoRepo->getById($id);
		if (!$this->depo) {
			throw new BadRequestException(404, 'Depo does not exist');
		}
		
		if (!$this->authorizator->isAllowed($this->user, $this->depo, 'addContact')) {
			$this->flashMessage('Nedostatečná oprávnění', 'error');
			$this->redirect(':Depo:Default:default', $id);
		}
	}
	
	public function renderAdd($id)
	{
		$this->template->depo = $this->depo;
	}
	
	public function actionEdit($id, $id2)
	{
		$this->depo = $this->depoRepo->getById($id);
		if (!$this->depo) {
			throw new BadRequestException('Depo does not exits');
		}
		
		$this->contact = $this->contactRepo->getById($id2);
		if (!$this->contact) {
			throw new BadRequestException('Contact does not exits');
		}
		
		if ($this->depo->contacts->has($this->contact) === false) {
			$this->flashMessage('Depozitum nemá takový kontakt', 'error');
			$this->redirect(':Depo:Default:default', $id);
		}
		
		if (!$this->authorizator->isAllowed($this->user, $this->depo, 'editContact')) {
			$this->flashMessage('Nedostatečná oprávnění', 'error');
			$this->redirect(':Depo:Default:default', $id);
		}
	}
	
	public function renderEdit($id, $id2)
	{
		$this->template->depo = $this->depo;
	}
	
	public function createComponentAddDepoContact()
	{
		$dcc = $this->depoContactControlFactory->create();
		$dcc->setDepo($this->depo);
		return $dcc;
	}
	
	public function createComponentEditDepoContact()
	{
		$adc = $this->depoContactControlFactory->create();
		$adc->setDepo($this->depo);
		$adc->setContact($this->contact);
		
		return $adc;
	}
}