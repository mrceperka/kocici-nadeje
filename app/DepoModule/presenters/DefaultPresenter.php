<?php
namespace Kocky\DepoModule\Presenters;


use Kocky\Components\Control\Depo\AE\IDepoAEControlFactory;
use Kocky\Components\Control\Depo\Detail\IDepoDetailControlFactory;
use Kocky\Components\Control\Depo\Roster\IDepoRosterControlFactory;
use Kocky\Model\Entity\Depo;
use Kocky\Model\Repository\DepoRepository;
use Nette\Application\BadRequestException;

class DefaultPresenter extends BasePresenter
{
	/** @var  Depo */
	public $depo;
	
	/** @var  DepoRepository @inject */
	public $depoRepo;
	
	/** @var  IDepoAEControlFactory @inject */
	public $depoControlFactory;
	
	/** @var  IDepoDetailControlFactory @inject */
	public $depoDetailControlFactory;
	
	/** @var  IDepoRosterControlFactory @inject */
	public $depoRosterControlFactory;
	
	public function actionAdd()
	{
		if (!$this->authorizator->isAllowed($this->user, 'depo', 'add')) {
			$this->flashMessage('Nedostatečná oprávnění', 'error');
			$this->redirect('list');
		}
	}
	
	public function actionDefault($id)
	{
		$this->depo = $this->depoRepo->getById($id);
		if (!$this->depo) {
			throw new BadRequestException('Depo does not exit');
		}
	}
	
	public function actionEdit($id)
	{
		$this->depo = $this->depoRepo->getById($id);
		if (!$this->depo) {
			throw new BadRequestException('Depo does not exit');
		}
		
		if (!$this->authorizator->isAllowed($this->user, $this->depo, 'edit')) {
			$this->flashMessage('Nedostatečná oprávnění', 'error');
			$this->redirect('list');
		}
	}
	
	public function renderDefault($id)
	{
		$this->template->depo = $this->depo;
		$this->template->isAllowed = false;
		if ($this->user->isLoggedIn() && ($this->depo->hasOwner($this->user->getEntity()) || $this->user->isInRole('admin'))) {
			$this->template->isAllowed = true;
		}
	}
	
	public function renderEdit($id)
	{
		$this->template->depo = $this->depo;
	}
	
	
	public function createComponentAddDepo()
	{
		return $this->depoControlFactory->create();
	}
	
	public function createComponentEditDepo()
	{
		$edc = $this->depoControlFactory->create();
		$edc->setDepo($this->depo);
		return $edc;
	}
	
	public function createComponentDepoDetail()
	{
		$ddc = $this->depoDetailControlFactory->create();
		$ddc->setDepo($this->depo);
		return $ddc;
	}
	
	public function createComponentDepoList()
	{
		$drc = $this->depoRosterControlFactory->create();
		$drc->setDepos($this->depoRepo->findAll());
		return $drc;
	}
	
	
}