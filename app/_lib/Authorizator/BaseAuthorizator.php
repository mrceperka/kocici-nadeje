<?php
namespace Kocky\Lib\Authenticator;

use Kocky\Lib\Authenticator\Actions\AnimalActions;
use Kocky\Lib\Authenticator\Actions\ContactActions;
use Kocky\Lib\Authenticator\Actions\DepoActions;
use Kocky\Lib\Authenticator\Actions\StaticContentActions;
use Kocky\Lib\Nette\Security\NextrasUser;
use Kocky\Model\Entity\Animal;
use Kocky\Model\Entity\Contact;
use Kocky\Model\Entity\Depo;
use Kocky\Model\Entity\StaticContent;
use Nette\Object;
use Nextras\Orm\Entity\IEntity;

class BaseAuthorizator extends Object implements IAuthorizator
{
	/** @var  AnimalActions */
	protected $animalActions;
	
	/** @var  DepoActions */
	protected $depoActions;
	
	/** @var  ContactActions */
	protected $contactActions;
	
	/** @var  StaticContentActions */
	protected $staticContentActions;
	
	/**
	 * BaseAuthorizator constructor.
	 * @param AnimalActions $animalActions
	 * @param DepoActions $depoActions
	 * @param ContactActions $contactActions
	 * @param StaticContentActions $staticContentActions
	 */
	public function __construct(AnimalActions $animalActions, DepoActions $depoActions, ContactActions $contactActions, StaticContentActions $staticContentActions)
	{
		$this->animalActions = $animalActions;
		$this->depoActions = $depoActions;
		$this->contactActions = $contactActions;
		$this->staticContentActions = $staticContentActions;
	}
	
	
	public function isAllowed(NextrasUser $user, $resource, $privilege)
	{
		if ($user->isInRole('admin')) return true;
		
		if ($resource instanceof Animal) {
			return $this->animalActions->isAllowed($user, $resource, $privilege);
		}
		if ($resource === 'animal') {
			return $this->animalActions->isAllowedStatic($user, $privilege);
		}
		
		if ($resource instanceof Depo) {
			return $this->depoActions->isAllowed($user, $resource, $privilege);
		}
		if ($resource === 'depo') {
			return $this->depoActions->isAllowedStatic($user, $privilege);
		}
		
		if ($resource instanceof Contact) {
			return $this->contactActions->isAllowed($user, $resource, $privilege);
		}
		if ($resource === 'contact') {
			return $this->contactActions->isAllowedStatic($user, $privilege);
		}
		
		if ($resource instanceof StaticContent) {
			return $this->staticContentActions->isAllowed($user, $resource, $privilege);
		}
		
		if ($resource === 'staticContent') {
			return $this->staticContentActions->isAllowedStatic($user, $privilege);
		}
		
		// $resource === 'user' && $privilege === 'edit'
		
		
		return false;
	}
	
	public function addAction(IEntity $allowedResource, $allowedStaticString)
	{
		//refactor
		//add via actions via config
		//support access via class name and resource name
	}
}