<?php

namespace Kocky\Lib\Authenticator;

use Kocky\Lib\Nette\Security\NextrasUser;

interface IAuthorizator
{
	function isAllowed(NextrasUser $user, $resource, $privilege);
}
