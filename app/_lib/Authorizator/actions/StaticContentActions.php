<?php
namespace Kocky\Lib\Authenticator\Actions;

use Kocky\Lib\Nette\Security\NextrasUser;
use Kocky\Model\Entity\StaticContent;
use Nette\Object;

/**
 * Class StaticContentActions
 * @package Kocky\Lib\Authenticator\Actions
 */
class StaticContentActions extends Object implements IAuthAction
{
	/**
	 * Should not reach this point, allowed only for admins
	 * @param NextrasUser $user
	 * @param StaticContent $staticContent
	 * @param $privilege
	 * @return bool
	 */
	public function isAllowed($user, $staticContent, $privilege)
	{
		return false;
	}
	
	/**
	 * Should not reach this point, allowed only for admins
	 * @param NextrasUser $user
	 * @param $privilege
	 * @return bool
	 */
	public function isAllowedStatic($user, $privilege)
	{
		return false;
	}
}