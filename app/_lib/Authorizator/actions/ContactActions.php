<?php
namespace Kocky\Lib\Authenticator\Actions;

use Kocky\Lib\Nette\Security\NextrasUser;
use Kocky\Model\Entity\Contact;
use Nette\Object;

/**
 * Class ContactActions
 * @package Kocky\Lib\Authenticator\Actions
 */
class ContactActions extends Object implements IAuthAction
{
	/**
	 * @param NextrasUser $user
	 * @param Contact $contact
	 * @param $privilege
	 * @return bool
	 */
	public function isAllowed($user, $contact, $privilege)
	{
		if($user->isLoggedIn() === false) return false;
		
		if($privilege === 'delete') {
			return $this->canDelete($user, $contact);
		}
		
		if($privilege === 'edit') {
			return $this->canEdit($user, $contact);
		}
		
		return false;
	}
	
	/**
	 * @param NextrasUser $user
	 * @param $privilege
	 * @return bool
	 */
	public function isAllowedStatic($user, $privilege)
	{
		return false;
	}
	
	private function canDelete(NextrasUser $user, Contact $contact)
	{
		return $user->getEntity()->contacts->has($contact);
	}
	
	private function canEdit(NextrasUser $user, Contact $contact)
	{
		return $user->getEntity()->contacts->has($contact);
	}
}