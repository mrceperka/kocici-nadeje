<?php
namespace Kocky\Lib\Authenticator\Actions;
/**
 * Interface IAuthAction
 * @package Kocky\Lib\Authenticator\Actions
 */
interface IAuthAction
{
	public function isAllowed($user, $resource, $privilege);
	
	public function isAllowedStatic($user, $privilege);
}