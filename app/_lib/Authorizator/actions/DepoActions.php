<?php
namespace Kocky\Lib\Authenticator\Actions;

use Kocky\Lib\Nette\Security\NextrasUser;
use Kocky\Model\Entity\Depo;
use Nette\Object;

/**
 * Class DepoActions
 * @package Kocky\Lib\Authenticator\Actions
 */
class DepoActions extends Object implements IAuthAction
{
	/**
	 * @param NextrasUser $user
	 * @param Depo $depo
	 * @param $privilege
	 * @return bool
	 */
	public function isAllowed($user, $depo, $privilege)
	{
		if($user->isLoggedIn() === false) return false;
		if($privilege === 'edit') {
			return $this->canEdit($user, $depo);
		}
		
		if(in_array($privilege, [
			'addAddress',
			'editAddress',
			
			'addContact',
			'editContact',
			'deleteContact',
			
			'addAnimal',
			'editAnimal',
			'deleteAnimal',
			
			'viewMembers',
		])) {
			return $this->canEdit($user, $depo);
		}
		
		return false;
	}
	
	/**
	 * @param NextrasUser $user
	 * @param $privilege
	 * @return bool
	 */
	public function isAllowedStatic($user, $privilege)
	{
		if($user->isLoggedIn() === false) return false;
		
		if($privilege === 'add') {
			return $this->canAdd($user);
		}
		
		if($privilege === 'delete') {
			return $this->canDelete($user);
		}
		
		return false;
	}
	
	private function canEdit(NextrasUser $user, Depo $depo)
	{
		return $user->getEntity()->ownsDepo($depo);
	}
	
	private function canAdd(NextrasUser $user)
	{
		return $user->isInRole('admin');
	}
	
	private function canDelete(NextrasUser $user)
	{
		return $user->isInRole('admin');
	}
}