<?php
namespace Kocky\Lib\Authenticator\Actions;

use Kocky\Lib\Nette\Security\NextrasUser;
use Kocky\Model\Entity\Animal;
use Nette\Object;

class AnimalActions extends Object implements IAuthAction
{
	/**
	 * @param NextrasUser $user
	 * @param Animal $animal
	 * @param $privilege
	 * @return bool
	 */
	public function isAllowed($user, $animal, $privilege)
	{
		if($user->isLoggedIn() === false) return false;
		
		if($privilege === 'edit') {
			if($animal->depo !== null) {
				return $this->canEdit($user, $animal);
			}
		}
		
		return false;
	}
	
	/**
	 * @param NextrasUser $user
	 * @param $privilege
	 * @return bool
	 */
	public function isAllowedStatic($user, $privilege)
	{
		if($user->isLoggedIn() === false) return false;
		
		if($privilege === 'add') {
			return $this->canAdd($user);
		}
		
		return false;
	}
	
	private function canEdit(NextrasUser $user, Animal $animal)
	{
		return $animal->depo !== null && $animal->depo->hasOwner($user->getEntity());
	}
	
	private function canAdd(NextrasUser $user)
	{
		return $user->getEntity()->ownsDepo();
		//return $user->getEntity()->ownsDepoWithFreeCapacity();
	}
}