<?php
namespace Kocky\Lib\Console\Data;

use Kocky\Lib\Console\ACommand;
use Kocky\Model\Entity\AnimalType;
use Kocky\Model\Entity\Depo;
use Kocky\Model\Entity\User;
use Kocky\Model\Entity\UserDepo;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CreateDummyDataCommand
 * @package Kocky\Lib\Console\Data
 */
class CreateDummyDataCommand extends ACommand
{
	protected function configure()
	{
		$this->setName('data:dummy')
			->setDescription('Creates dummy data for kocicinadeje');
	}
	
	/**
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @return int
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$this->createAdmin();
		$this->createAnimalTypes();
		
		$this->createAll();
		
		
		$output->writeln('Done');
	}
	
	protected function createAnimalTypes()
	{
		$ats = [
			[
				'Kočka',
				10
			],
			[
				'Pes',
				9
			],
			[
				'Pták',
				8
			],
			[
				'Myš',
				7
			]
		];
		
		foreach ($ats as $at) {
			$ant = new AnimalType();
			$ant->name = $at[0];
			$ant->popularity = $at[1];
			$this->orm->animalTypeRepo->persist($ant);
		}
		$this->orm->animalTypeRepo->flush();
	}
	
	protected function createAdmin()
	{
		$admin = new User();
		$admin->email = 'admin@admin.local';
		$admin->setPassword($admin->email);
		$admin->role = 'admin';
		
		$this->orm->userRepo->persistAndFlush($admin);
	}
	
	protected function createAll()
	{
		$depos = [
			[
				'name' => 'Depo 1',
				'capacity' => 20
			],
			[
				'name' => 'Depo 2',
				'capacity' => 10
			],
			[
				'name' => 'Depo 3',
				'capacity' => 50,
			]
		];
		
		foreach ($depos as $dp) {
			$depo = new Depo();
			$depo->name = $dp['name'];
			$depo->capacity = $dp['capacity'];
			$this->orm->depoRepo->persistAndFlush($depo);
			
			for($i = 0; $i < rand(1, 2); $i++) {
				$u = new User();
				$u->email = 'owner' . $i . '@' . $depo->name . '.local';
				$u->setPassword($u->email);
				$u->role = 'depozitor';
				$this->orm->userRepo->persistAndFlush($u);
				
				$ud = new UserDepo();
				$ud->user = $u;
				$ud->depo = $depo;
				$ud->relation = UserDepo::RELATION_OWNER;
				$this->orm->userDepoRepo->persistAndFlush($ud);
			}
			
			for($i = 0; $i < 3; $i++) {
				$u = new User();
				$u->email = 'member' . $i . '@' . $depo->name . '.local';
				$u->setPassword($u->email);
				$u->role = 'depozitor';
				$this->orm->userRepo->persistAndFlush($u);
				
				$ud = new UserDepo();
				$ud->user = $u;
				$ud->depo = $depo;
				$ud->relation = UserDepo::RELATION_MEMBER;
				$this->orm->userDepoRepo->persistAndFlush($ud);
			}
			$this->orm->userRepo->flush();
		}
	}
	
}