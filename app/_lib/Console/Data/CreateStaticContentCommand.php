<?php
namespace Kocky\Lib\Console\Data;

use Kocky\Lib\Console\ACommand;
use Kocky\Model\Entity\StaticContent;
use Kocky\Model\Repository\StaticContentRepository;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CreateStaticContentCommand
 * @package Kocky\Lib\Console\Data
 */
class CreateStaticContentCommand extends ACommand
{
	/** @var  StaticContentRepository */
	private $staticContentRepo;
	
	/** @var  ProgressBar */
	private $progressBar;
	
	protected function configure()
	{
		$this->setName('data:static')
			->setDescription('Creates static content for kocicinadeje');
	}
	
	protected function initialize(InputInterface $input, OutputInterface $output)
	{
		parent::initialize($input, $output);
		$this->staticContentRepo = $this->orm->staticContentRepo;
		$this->progressBar = new ProgressBar($output, 3);
	}

	
	/**
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @return int
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$this->createFooterData();
		$output->writeln('');
		return 0;
	}
	
	private function createFooterData()
	{
		$content = new StaticContent();
		$content->section = StaticContent::FOOTER;
		$content->text = 'markdown footer';
		$content->position = 1;
		$this->staticContentRepo->persistAndFlush($content);
		$this->progressBar->advance();
		
		$content = new StaticContent();
		$content->section = StaticContent::FOOTER;
		$content->position = 2;
		$content->text = 'Pomozte opuštěným a týraným zvířatům';
		$this->staticContentRepo->persistAndFlush($content);
		$this->progressBar->advance();
		
		$content = new StaticContent();
		$content->section = StaticContent::FOOTER;
		$content->position = 3;
		$content->text = 'Kontakty';
		$this->staticContentRepo->persistAndFlush($content);
		$this->progressBar->advance();
	}

}