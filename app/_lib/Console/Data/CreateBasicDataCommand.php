<?php
namespace Kocky\Lib\Console\Data;

use Kocky\Lib\Console\ACommand;
use Kocky\Model\Entity\AnimalType;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CreateBasicDataCommand
 * @package Kocky\Lib\Console\Data
 */
class CreateBasicDataCommand extends ACommand
{
	protected function configure()
	{
		$this->setName('data:create-basic-data')
			->setDescription('Creates basic data for kocicinadeje');
	}
	
	/**
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @return int
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$this->createAnimalTypes();
		
		$output->writeln('Done');
	}
	
	protected function createAnimalTypes()
	{
		$ats = [
			[
				'Kočka',
				10
			],
			[
				'Pes',
				9
			]
		];
		
		foreach ($ats as $at) {
			$ant = new AnimalType();
			$ant->name = $at[0];
			$ant->popularity = $at[1];
			$this->orm->animalTypeRepo->persist($ant);
		}
		$this->orm->animalTypeRepo->flush();
	}
}