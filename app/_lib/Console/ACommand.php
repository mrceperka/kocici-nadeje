<?php
namespace Kocky\Lib\Console;

use Kocky\Model\Orm;
use Nette\DI\Container;
use Symfony\Component\Console\Command\Command;

/**
 * Class ACommand
 * @package Kocky\Lib\Console
 */
abstract class ACommand extends Command
{
	/** @var  Orm @inject */
	public $orm;
	
	/** @var  Container @inject */
	public $di;
}