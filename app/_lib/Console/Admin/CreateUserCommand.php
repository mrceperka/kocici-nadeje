<?php
namespace Kocky\Lib\Console\Admin;

use Kocky\Lib\Console\ACommand;
use Kocky\Model\Entity\User;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CreateBasicDataCommand
 * @package Kocky\Lib\Console\Data
 */
class CreateUserCommand extends ACommand
{
	protected function configure()
	{
		$this->setName('admin:create-user')
			->setDescription('Creates user');
		
		$this->addArgument('email', InputArgument::REQUIRED, 'email of user');
		$this->addArgument('password', InputArgument::REQUIRED, 'password user');
		$this->addArgument('role', InputArgument::REQUIRED, 'role of user');
	}
	
	/**
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @return int
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$email = $input->getArgument('email');
		$password = $input->getArgument('password');
		$role = $input->getArgument('role');
		
		$this->createUser($email, $password, $role, $output);
		
		$output->writeln("\tDone");
	}
	
	protected function createUser($email, $password, $role, OutputInterface $output)
	{
		if ($this->orm->userRepo->getOneByEmail($email)) {
			$output->writeln('User with email: "' . $email . '" already exists');
		} else {
			$user = new User();
			$user->email = $email;
			$user->setPassword($password);
			$user->role = $role;
			
			$this->orm->userRepo->persistAndFlush($user);
			$output->writeln("\tEmail: " . $email);
			$output->writeln("\tRoles: " . $role);
			$output->writeln("\tPassword: " . $password);
		}
		
		
	}
}