<?php

namespace Kocky\Lib\Nette\Security;


use Kocky\Model\Entity\User;

class NextrasIdentity extends \Nette\Security\Identity
{
	/**
	 * NextrasIdentity constructor.
	 * @param User $entity
	 * @param null $data
	 */
	public function __construct(User $entity, $data = null)
	{
		parent::__construct($entity->id, $entity->role, $data);
	}
}