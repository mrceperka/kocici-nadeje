<?php


namespace Kocky\Lib\Nette\Security;





use CPTeam\Bridges\Nette\Security\TUser;
use CPTeam\Packages\BlogPackage\Mapping\IBlogModuleUserEntityProvider;
use Kocky\Model\Entity\User;
use Kocky\Model\Repository\UserRepository;
use Nette\Security\IAuthenticator;
use Nette\Security\IAuthorizator;
use Nette\Security\IUserStorage;

/**
 * Class NextrasUser
 * @package Kocky\Lib\Nette\Security
 */
class NextrasUser extends \Nette\Security\User
{
	
	/** @var  UserRepository */
	private $userRepository;
	
	/** @var User */
	private $entity = null;
	

	public function __construct(IUserStorage $storage, IAuthenticator $authenticator = null, IAuthorizator $authorizator = null, UserRepository $userRepository)
	{
		parent::__construct($storage, $authenticator, $authorizator);
		$this->userRepository = $userRepository;
	}
	
	
	/**
	 * @param bool $refresh
	 *
	 * @return User|\Nextras\Orm\Entity\IEntity|null
	 */
	public function getEntity($refresh = false)
	{
		if ($this->isLoggedIn() == false) {
			return NULL;
		}
		
		if ($refresh || $this->entity === NULL) {
			$this->entity = $this->userRepository->getById($this->getId());
		}
		
		return $this->entity;
	}
	
	
}