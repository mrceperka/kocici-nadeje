<?php
namespace Kocky\Lib\Authenticator;
use Kocky\Lib\Nette\Security\NextrasIdentity;
use Kocky\Model\Entity\User;
use Kocky\Model\Repository\UserRepository;
use Nette\Object;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nextras\Orm\Entity\IEntity;

/**
 * Class BaseAuthenticator
 * @package Kocky\Lib\Authenticator
 */
class BaseAuthenticator extends Object implements IAuthenticator
{
	/** @var  UserRepository */
	private $userRepo;
	
	/**
	 * BaseAuthenticator constructor.
	 * @param UserRepository $userRepo
	 */
	public function __construct(UserRepository $userRepo)
	{
		$this->userRepo = $userRepo;
	}
	
	
	public function authenticate(array $credentials)
	{
		$email = $credentials['email'];
		$password = $credentials['password'];
		/** @var User $user */
		$user = $this->userRepo->getOneByEmail($email);
		if($user) {
			if ($user->verifyPassword($password)) {
				return new NextrasIdentity($user, $user->toArray(IEntity::TO_ARRAY_RELATIONSHIP_AS_ID));
			} else {
				throw new AuthenticationException("Bad password");
			}
		} else {
			throw new AuthenticationException('User or email address ' . $email . ' does not exist');
		}
	}
}