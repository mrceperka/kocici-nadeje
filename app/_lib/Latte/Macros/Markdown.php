<?php
namespace Kocky\Lib\Latte\Macros;

use Latte\MacroNode;
use Latte\Macros\MacroSet;
use Latte\PhpWriter;

class MarkdownMacros extends MacroSet
{
	/** @var  \Parsedown */
	private static $parser;

	/**
	 * @param \Latte\Compiler $compiler
	 * @return MacroSet
	 */
	public static function install(\Latte\Compiler $compiler)
	{
		$me = new static($compiler);
		$me->addMacro('md', array($me, 'parseDown'));
		return $me;
	}

	public static function parseDown(MacroNode $node, PhpWriter $phpWriter)
	{
		$cmd = 'echo ' . __CLASS__ . '::getMarkdownParser()->text(%node.word)';
		return $phpWriter->write($cmd);
	}

	/**
	 * @return mixed
	 */
	public static function getMarkdownParser()
	{
		if(static::$parser === null){
			static::$parser = new \Parsedown();
		}
		return static::$parser;
	}
}
