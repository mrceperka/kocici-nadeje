<?php
namespace Kocky\AdminModule\Presenters;

abstract class BasePresenter extends \Kocky\CommonModule\Presenters\BasePresenter
{
	public function startup()
	{
		parent::startup();
		
		if($this->user->isLoggedIn() == false || $this->user->isInRole('admin') === false) {
			$this->flashMessage('Nedostatečné oprávnění', 'warn');
			$this->redirect(':Common:Homepage:default');
		}
	}
}