<?php
namespace Kocky\Components\Form\Login;

use Kocky\Components\Form\BaseFormFactory;

class LoginFormFactory
{
	/** @var  BaseFormFactory */
	private $baseFormFactory;
	
	/**
	 * RegisterFormFactory constructor.
	 * @param BaseFormFactory $baseFormFactory
	 */
	public function __construct(BaseFormFactory $baseFormFactory)
	{
		$this->baseFormFactory = $baseFormFactory;
	}
	
	public function create()
	{
		$form = $this->baseFormFactory->create();
		
		$form->addProtection();
		$form->addEmail('email', 'Email')
			->setRequired('Vyplňte email prosím');
		
		$form->addPassword('password', 'Heslo')
			->setRequired('Vyplňte heslo prosím');
		
		$form->addSubmit('submit', 'Přihlásit');
		
		return $form;
	}
}