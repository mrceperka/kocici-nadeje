<?php

namespace Kocky\Components\Form\Contact;


use Kocky\Components\Form\BaseFormFactory;
use Nette\Application\UI\Form;

class ContactFormFactory
{
	/** @var  BaseFormFactory */
	private $baseFormFactory;
	
	/**
	 * DepoContactFormFactory constructor.
	 * @param BaseFormFactory $baseFormFactory
	 */
	public function __construct(BaseFormFactory $baseFormFactory)
	{
		$this->baseFormFactory = $baseFormFactory;
	}
	
	
	public function create()
	{
		$form = $this->baseFormFactory->create();
		$form->addText('name', 'Jméno');
		
		$form->addInteger('telephone', 'Telefon')
			->addRule(Form::LENGTH, 'Telefon musí obsahuovat přesně 9 číslic 123456789', 9);
		
		$form->addEmail('email', 'Email');
		
		$form->addText('facebook', 'Profil na Facebooku')
			->addRule(Form::PATTERN, 'Odkaz musí směřovat na https://www.facebook.com', '^https://www.facebook.com/.*')
			->setRequired(false);
		
		$form->addText('url', 'Odkaz na osobní webové stránky');
		
		$form->addSubmit('submit', 'Vytvořit');
		return $form;
	}
	
	
}