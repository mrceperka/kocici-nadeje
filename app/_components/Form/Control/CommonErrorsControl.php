<?php
namespace Kocky\Components\Form\Control;

use Nette\Application\UI\Control;

/**
 * Class CommonErrorsControl
 * @package Kocky\Components\Form\Control
 */
class CommonErrorsControl extends Control
{
	const NAME = 'common_errors';
	private $errors = [];
	
	public function render()
	{
		$this->template->errors = $this->errors;
		$this->template->setFile(__DIR__ . '/commonErrors.latte');
		$this->template->render();
	}
	
	public function addError($message)
	{
		$this->errors[] = $message;
	}
}

interface ICommonErrorsFactory
{
	/**
	 * @return CommonErrorsControl
	 */
	public function create();
}