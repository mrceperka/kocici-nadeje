<?php

namespace Kocky\Components\Form\Animal;


use Kocky\Components\Form\BaseFormFactory;
use Kocky\Lib\Nette\Security\NextrasUser;
use Kocky\Model\Entity\Animal;
use Kocky\Model\Entity\Depo;
use Kocky\Model\Repository\AnimalTypeRepository;
use Kocky\Model\Repository\DepoRepository;
use Kocky\Model\Repository\HandicapRepository;
use Nette\Forms\Form;
use Nette\Utils\Html;
use Nextras\Orm\Collection\ICollection;

class AnimalFormFactory
{
	/** @var  BaseFormFactory */
	private $baseFormFactory;
	
	/** @var  DepoRepository */
	private $depoRepo;
	
	/** @var  AnimalTypeRepository */
	private $animalTypeRepo;
	
	/** @var  HandicapRepository */
	private $handicapRepo;
	
	/** @var  NextrasUser */
	private $user;
	
	/**
	 * AnimalFormFactory constructor.
	 * @param BaseFormFactory $baseFormFactory
	 * @param DepoRepository $depoRepo
	 * @param AnimalTypeRepository $animalTypeRepo
	 * @param HandicapRepository $handicapRepo
	 * @param NextrasUser $user
	 */
	public function __construct(BaseFormFactory $baseFormFactory, DepoRepository $depoRepo, AnimalTypeRepository $animalTypeRepo, HandicapRepository $handicapRepo, NextrasUser $user)
	{
		$this->baseFormFactory = $baseFormFactory;
		$this->depoRepo = $depoRepo;
		$this->animalTypeRepo = $animalTypeRepo;
		$this->handicapRepo = $handicapRepo;
		$this->user = $user;
	}
	
	
	public function create()
	{
		$form = $this->baseFormFactory->create();
		
		$selectPrompt = Html::el('option');
		$selectPrompt->setAttribute('selected', true);
		
		$form->addText('name', 'Jméno')
			->setRequired('Vyplňte jméno prosím');
		
		$form->addSelect(
			'gender',
			'Pohlaví',
			[
				Animal::GENDER_MALE => 'Samec',
				Animal::GENDER_FEMALE => 'Samice',
				Animal::GENDER_UNKNOWN => 'Neznámé'
			]
		)
			->setDefaultValue(Animal::GENDER_UNKNOWN)
			->setRequired('Vyberte pohlaví prosím');
		
		$form->addSelect(
			'state',
			'Stav',
			[
				Animal::STATE_NEW => 'Nově přidáno',
				Animal::STATE_TAKEAWAY_READY => 'K odběru',
				Animal::STATE_VIRTUAL_READY => 'K virtuální adopci',
				Animal::STATE_PLACED => 'Umístěno'
			])
			->setRequired('Vyberte stav prosím')
			->setDefaultValue(Animal::STATE_NEW);
		
		$form->addCheckbox('castrated', 'Kastrováno');
		
		
		$selectPrompt = clone $selectPrompt;
		$selectPrompt->setText('Vyberte druh');
		$form->addSelect('type', 'Druh', $this->animalTypeRepo->findAll()->orderBy('popularity', ICollection::DESC)->fetchPairs('id', 'name'))
			->setPrompt($selectPrompt)
			->setRequired('Vyberte druh prosím');
		
		$form->addInteger('age', 'Odhadovaný věk')
			->addRule(Form::RANGE, 'Odhadovaný věk zvířete není v běžném rozsahu', [0, 50])
			->setRequired('Vyplňte odhadovaný prosím');
		
		
		if($this->user->isInRole('admin')) {
			$depos = $this->depoRepo->findAll();
			
		} else {
			$depos = $this->user->getEntity()->getDepos();
		}
		
		$result = [];
		/** @var Depo $depo */
		foreach ($depos as $depo) {
			$option = Html::el('option');
			$option->setAttribute('value', $depo->id);
			$option->setText($depo->name);
			if($depo->freeCapacity() === 0) {
				$option->setAttribute('disabled', true);
			}
			$result[$depo->id] = $option;
		}
		$selectPrompt = clone $selectPrompt;
		$selectPrompt->setText('Vyberte depozitum');
		
		$form->addSelect('depo', 'Depozitum', $result)
			->setPrompt($selectPrompt);
			//->setRequired('Vyberte prosím depozitum');
		
		$form->addMultiSelect('handicaps', 'Handicapy', $this->handicapRepo->findAll()->fetchPairs('id', 'name'));
		
		$form->addMultiUpload('images', 'Obrázky')
			->addRule(Form::IMAGE, 'Pouze obrázky type JPEG, PNG, GIF prosím')
			->setRequired(false);

		$form->addText('info', 'Informace');
		
		$form->addSubmit('submit', 'Vytvořit');
		
		return $form;
	}
}