<?php

namespace Kocky\Components\Form\Story;


use Kocky\Components\Form\BaseFormFactory;

class StoryFormFactory
{
	/** @var  BaseFormFactory */
	private $baseFormFactory;
	
	/**
	 * StoryFormFactory constructor.
	 * @param BaseFormFactory $baseFormFactory
	 */
	public function __construct(BaseFormFactory $baseFormFactory)
	{
		$this->baseFormFactory = $baseFormFactory;
	}
	
	public function create()
	{
		$form = $this->baseFormFactory->create();
		$form->addTextArea('text', 'Informace')
			->setAttribute('class', 'materialize-textarea')
			->setRequired('Vyplňtě prosím text');
		
		$form->addSubmit('submit', 'Přidat');
		
		return $form;
	}
}