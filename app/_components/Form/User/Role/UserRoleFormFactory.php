<?php

namespace Kocky\Components\Form\User\Role;


use Kocky\Components\Form\BaseFormFactory;
use Kocky\Model\Entity\User;

class UserRoleFormFactory
{
	/** @var  BaseFormFactory */
	private $baseFormFactory;
	
	/**
	 * UserRoleFormFactory constructor.
	 * @param BaseFormFactory $baseFormFactory
	 */
	public function __construct(BaseFormFactory $baseFormFactory)
	{
		$this->baseFormFactory = $baseFormFactory;
	}
	
	
	public function create()
	{
		$form = $this->baseFormFactory->create();
		$form->addMultiSelect(
			'roles',
			'Role',
			[
				User::ROLE_ADMIN => User::ROLE_ADMIN,
				User::ROLE_DEPOZITOR => User::ROLE_DEPOZITOR,
				User::ROLE_MODERATOR => User::ROLE_MODERATOR,
				User::ROLE_USER => User::ROLE_USER,
			]
		)
			->setRequired('Vyberte prosím roli');
		
		$form->addSubmit('submit', 'Uložit');
		
		return $form;
	}
}