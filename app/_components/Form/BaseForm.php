<?php

namespace Kocky\Components\Form;

use Kocky\Components\Form\Control\CommonErrorsControl;
use Kocky\Components\Form\Renderer\BaseFormRenderer;
use Nette\Application\UI\Form;
use Nette\ComponentModel\IContainer;

/**
 * Class ABaseForm
 * @package Kocky\Components\Form
 */
class BaseForm extends Form
{
	public function __construct(IContainer $parent = null, $name = null)
	{
		parent::__construct($parent, $name);
		
		$this->setRenderer(new BaseFormRenderer());
		$this->addComponent(new CommonErrorsControl(), CommonErrorsControl::NAME);
		
		$this->elementPrototype->setAttribute('novalidate', true);
	}
	
	/**
	 * @return CommonErrorsControl
	 */
	public function getCommonErrorsControl()
	{
		$component = $this->getComponent(CommonErrorsControl::NAME);
		if($component === null) {
			throw new \LogicException('CommonErrors component is not bound to the form');
		}
		return $this->getComponent(CommonErrorsControl::NAME);
	}
	
	public function addCommonError($message)
	{
		$this->getCommonErrorsControl()->addError($message);
	}
	
	
}