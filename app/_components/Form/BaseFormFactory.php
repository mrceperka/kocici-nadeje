<?php


namespace Kocky\Components\Form;


class BaseFormFactory
{
	public function create()
	{
		return new BaseForm();
	}
}