<?php

namespace Kocky\Components\Form\Depo;


use Kocky\Components\Form\BaseFormFactory;

class DepoAnimalFormFactory
{
	/** @var  BaseFormFactory */
	private $baseFormFactory;
	
	/**
	 * DepoAnimalFormFactory constructor.
	 * @param BaseFormFactory $baseFormFactory
	 */
	public function __construct(BaseFormFactory $baseFormFactory)
	{
		$this->baseFormFactory = $baseFormFactory;
	}
	
	
	public function create()
	{
		$form = $this->baseFormFactory->create();
		$form->addMultiSelect('animals', 'Zvířata')
			->setRequired('Vyberte prosím alespoň jednoho zvíře');
		
		$form->addSubmit('submit', 'Přidat');
		return $form;
	}
	

}