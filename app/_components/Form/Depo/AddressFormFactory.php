<?php

namespace Kocky\Components\Form\Depo;


use Kocky\Components\Form\BaseFormFactory;
use Kocky\Model\Entity\Address;
use Nette\Forms\Form;

class AddressFormFactory
{
	/** @var  BaseFormFactory */
	private $baseFormFactory;
	
	/**
	 * AddAddressFormFactory constructor.
	 * @param BaseFormFactory $baseFormFactory
	 */
	public function __construct(BaseFormFactory $baseFormFactory)
	{
		$this->baseFormFactory = $baseFormFactory;
	}
	
	
	public function create()
	{
		$form = $this->baseFormFactory->create();
		
		$form->addText('city', 'Město')
			->setRequired('Vyplňte město prosím');
		
		$form->addText('street', 'Ulice a čp.');
			//->setRequired('Vyplňte ulici a čp prosím');
		
		$form->addInteger('zip', 'PSČ')
			->addRule(Form::LENGTH, 'PSČ musí obsahovat 5 číslic', 5)
			->setRequired('Vyplňte prosím PSČ');
		
		$form->addSelect('country', 'Země', [Address::COUNTRY_CS => 'Česká republika', Address::COUNTRY_SK => 'Slovenská republika'])
			->setDefaultValue(Address::COUNTRY_CS)
			->setRequired('Vyberte zemi prosím');
		
		$form->addSubmit('submit', 'Vytvořit');
		return $form;
	}
}