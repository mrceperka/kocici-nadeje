<?php

namespace Kocky\Components\Form\Depo;


use Kocky\Components\Form\BaseFormFactory;
use Nette\Forms\Form;

class DepoFormFactory
{
	/** @var  BaseFormFactory */
	private $baseFormFactory;
	
	/**
	 * RegisterFormFactory constructor.
	 * @param BaseFormFactory $baseFormFactory
	 */
	public function __construct(BaseFormFactory $baseFormFactory)
	{
		$this->baseFormFactory = $baseFormFactory;
	}
	
	public function create()
	{
		$form = $this->baseFormFactory->create();
		$form->addText('name', 'Název')
			->setRequired('Vyplňte název prosím');
		
		$form->addInteger('capacity', 'Kapacita')
			->addRule(Form::MIN, 'Kapacita nemůže být nulová', 1)
			->setRequired('Vyplňte kapacitu prosím');
		
		$form->addSubmit('submit', 'Vytvořit');
		return $form;
	}
}