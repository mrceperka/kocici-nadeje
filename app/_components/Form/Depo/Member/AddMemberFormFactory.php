<?php

namespace Kocky\Components\Form\Depo\Member;


use Kocky\Components\Form\BaseFormFactory;

class AddMemberFormFactory
{
	/** @var  BaseFormFactory */
	private $baseFormFactory;
	
	/**
	 * MemberFormFactory constructor.
	 * @param BaseFormFactory $baseFormFactory
	 */
	public function __construct(BaseFormFactory $baseFormFactory)
	{
		$this->baseFormFactory = $baseFormFactory;
	}
	
	
	public function create()
	{
		$form = $this->baseFormFactory->create();
		$form->addMultiSelect('users', 'Uživatelé')
			->setRequired('Vyberte prosím alespoň jednoho uživatele');
		
		$form->addSubmit('submit', 'Přidat');
		return $form;
	}
}