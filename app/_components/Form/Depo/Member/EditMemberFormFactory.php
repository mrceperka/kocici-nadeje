<?php

namespace Kocky\Components\Form\Depo\Member;


use Kocky\Components\Form\BaseFormFactory;
use Kocky\Model\Entity\UserDepo;

class EditMemberFormFactory
{
	/** @var  BaseFormFactory */
	private $baseFormFactory;
	
	/**
	 * MemberFormFactory constructor.
	 * @param BaseFormFactory $baseFormFactory
	 */
	public function __construct(BaseFormFactory $baseFormFactory)
	{
		$this->baseFormFactory = $baseFormFactory;
	}
	
	
	public function create()
	{
		$form = $this->baseFormFactory->create();
		$form->addSelect('relation', 'Vztah', [
			UserDepo::RELATION_OWNER => 'vlastník',
			UserDepo::RELATION_MEMBER => 'člen',
		])
			->setRequired('Zvolte prosím vztah');
		
		$form->addSubmit('submit', 'Uložit');
		return $form;
	}
}