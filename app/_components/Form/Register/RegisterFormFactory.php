<?php

namespace Kocky\Components\Form\Register;

use Kocky\Components\Form\BaseFormFactory;
use Nette\Forms\Form;

class RegisterFormFactory
{
	/** @var  BaseFormFactory */
	private $baseFormFactory;
	
	/**
	 * RegisterFormFactory constructor.
	 * @param BaseFormFactory $baseFormFactory
	 */
	public function __construct(BaseFormFactory $baseFormFactory)
	{
		$this->baseFormFactory = $baseFormFactory;
	}
	
	
	public function create()
	{
		$form = $this->baseFormFactory->create();
		
		$form->addProtection();
		$form->addEmail('email', 'Email')
			->setRequired('Vyplňte email prosím');
		
		$form->addPassword('password', 'Heslo')
			->addRule(Form::MIN_LENGTH, 'Minimální délka hesla je 8 znaků', 8)
			->setRequired('Vyplňte heslo prosím');
		
		$form->addPassword('password_again', 'Heslo znovu')
			->addRule(Form::EQUAL, 'Hesla se neshodují', $form['password'])
			->setRequired('Obě hesla musí být vyplněna');
			
		$form->addSubmit('submit', 'Registrovat');
		
		return $form;
		
	}
}