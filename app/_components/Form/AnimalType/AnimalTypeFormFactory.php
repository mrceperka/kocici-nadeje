<?php

namespace Kocky\Components\Form\AnimalType;


use Kocky\Components\Form\BaseFormFactory;
use Nette\Forms\Form;

class AnimalTypeFormFactory
{
	/** @var  BaseFormFactory */
	private $baseFormFactory;
	
	/**
	 * AnimalTypeFormFactory constructor.
	 * @param BaseFormFactory $baseFormFactory
	 */
	public function __construct(BaseFormFactory $baseFormFactory)
	{
		$this->baseFormFactory = $baseFormFactory;
	}
	
	
	public function create()
	{
		$form = $this->baseFormFactory->create();
		$form->addText('name', 'Název')
			->setRequired('Vyplňte název prosím');
		
		$form->addInteger('popularity', 'Popularita')
			->addRule(Form::RANGE, 'Popularita smí nabývat rozsahu 1 až 10', [1, 10])
			->setDefaultValue(1)
			->setRequired('Vyplňte popularitu prosím');
		
		$form->addSubmit('submit', 'Vytvořit');
		
		return $form;
	}
}