<?php

namespace Kocky\Components\Form\Renderer;


use Nette\Forms\Rendering\DefaultFormRenderer;

/**
 * Class BaseFormRenderer
 * @package Kocky\Components\Form\Renderer
 */
class BaseFormRenderer extends DefaultFormRenderer
{
	
	
	/**
	 *  /--- form.container
	 *
	 *    /--- error.container
	 *      .... error.item [.class]
	 *    \---
	 *
	 *    /--- hidden.container
	 *      .... HIDDEN CONTROLS
	 *    \---
	 *
	 *    /--- group.container
	 *      .... group.label
	 *      .... group.description
	 *
	 *      /--- controls.container
	 *
	 *        /--- pair.container [.required .optional .odd]
	 *
	 *          /--- label.container
	 *            .... LABEL
	 *            .... label.suffix
	 *            .... label.requiredsuffix
	 *          \---
	 *
	 *          /--- control.container [.odd]
	 *            .... CONTROL [.required .text .password .file .submit .button]
	 *            .... control.requiredsuffix
	 *            .... control.description
	 *            .... control.errorcontainer + control.erroritem
	 *          \---
	 *        \---
	 *      \---
	 *    \---
	 *  \--
	 *
	 * @var array of HTML tags */
	public $wrappers = array(
		'form' => array(
			'container' => 'div class=row',
		),
		
		'error' => array(
			'container' => 'ul class=error',
			'item' => 'li',
		),
		
		'group' => array(
			'container' => 'div',
			'label' => 'div class=groupTitle',
			'description' => 'p',
		),
		
		'controls' => array(
			'container' => null,
		),
		
		'pair' => array(
			'container' => 'div class="input-field col s12"',
			'.required' => 'required',
			'.optional' => NULL,
			'.odd' => NULL,
			'.error' => NULL,
		),
		
		'control' => array(
			'container' => 'div class="row"',
			'.odd' => NULL,
			
			'description' => 'small',
			'requiredsuffix' => '',
			'errorcontainer' => 'span class=error',
			'erroritem' => '',
			
			'.required' => 'required',
			'.text' => 'text',
			'.password' => 'text',
			'.file' => 'text',
			'.submit' => 'btn',
			'.image' => 'imagebutton',
			'.button' => 'btn',
		),
		
		'label' => array(
			'container' => null,
			'suffix' => NULL,
			'requiredsuffix' => null,
		),
		
		'hidden' => array(
			'container' => 'div',
		),
	);
	
}