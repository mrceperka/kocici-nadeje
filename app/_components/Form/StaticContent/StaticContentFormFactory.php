<?php

namespace Kocky\Components\Form\StaticContent;


use Kocky\Components\Form\BaseFormFactory;

/**
 * Class StaticContentFormFactory
 * @package Kocky\Components\Form\Story
 */
class StaticContentFormFactory
{
	/** @var  BaseFormFactory */
	private $baseFormFactory;
	
	/**
	 * StaticContentFormFactory constructor.
	 * @param BaseFormFactory $baseFormFactory
	 */
	public function __construct(BaseFormFactory $baseFormFactory)
	{
		$this->baseFormFactory = $baseFormFactory;
	}
	
	
	public function create()
	{
		$form = $this->baseFormFactory->create();
		$form->addTextArea('text', 'Obsah')
			->setAttribute('class', 'materialize-textarea')
			->setRequired('Vyplňtě prosím text');
		
		$form->addSubmit('submit', 'Přidat');
		
		return $form;
	}
}