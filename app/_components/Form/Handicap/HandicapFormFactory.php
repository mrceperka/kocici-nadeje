<?php

namespace Kocky\Components\Form\Handicap;


use Kocky\Components\Form\BaseFormFactory;
use Nette\Forms\Form;

class HandicapFormFactory
{
	/** @var  BaseFormFactory */
	private $baseFormFactory;
	
	/**
	 * AnimalTypeFormFactory constructor.
	 * @param BaseFormFactory $baseFormFactory
	 */
	public function __construct(BaseFormFactory $baseFormFactory)
	{
		$this->baseFormFactory = $baseFormFactory;
	}
	
	
	public function create()
	{
		$form = $this->baseFormFactory->create();
		$form->addText('name', 'Název')
			->setRequired('Vyplňte název prosím');
		
		$form->addInteger('severity', 'Závažnost')
			->addRule(Form::RANGE, 'Závažnost smí nabývat rozsahu 1 až 10', [1, 10])
			->setDefaultValue(1)
			->setRequired('Vyplňte závažnost prosím');
		
		$form->addText('description', 'Krátký popisek');
		
		$form->addSubmit('submit', 'Vytvořit');
		
		return $form;
	}
}