<?php

namespace Kocky\Components\Filter\Animal;


use Kocky\Components\Form\BaseFormFactory;
use Kocky\Model\Entity\Animal;
use Kocky\Model\Repository\AnimalTypeRepository;
use Kocky\Model\Repository\DepoRepository;
use Kocky\Model\Repository\HandicapRepository;
use Nextras\Orm\Collection\ICollection;

class AnimalFilterFormFactory
{
	/** @var  BaseFormFactory */
	private $baseFormFactory;
	
	/** @var  DepoRepository */
	private $depoRepo;
	
	/** @var  AnimalTypeRepository */
	private $animalTypeRepo;
	
	/** @var  HandicapRepository */
	private $handicapRepo;
	
	/**
	 * AnimalFilterFactory constructor.
	 * @param BaseFormFactory $baseFormFactory
	 * @param DepoRepository $depoRepo
	 * @param AnimalTypeRepository $animalTypeRepo
	 * @param HandicapRepository $handicapRepo
	 */
	public function __construct(BaseFormFactory $baseFormFactory, DepoRepository $depoRepo, AnimalTypeRepository $animalTypeRepo, HandicapRepository $handicapRepo)
	{
		$this->baseFormFactory = $baseFormFactory;
		$this->depoRepo = $depoRepo;
		$this->animalTypeRepo = $animalTypeRepo;
		$this->handicapRepo = $handicapRepo;
	}
	
	
	public function create()
	{
		$form = $this->baseFormFactory->create();
		$form->setMethod('GET');
		$form->addText('name', 'Jméno')
			->setDefaultValue('');
		
		$form->addSelect(
			'gender',
			'Pohlaví',
			[
				'dc' => 'Nezáleží',
				Animal::GENDER_MALE => 'Samec',
				Animal::GENDER_FEMALE => 'Samice',
			]
		)
			->setDefaultValue('dc')
			->setRequired('Vyberte pohlaví prosím');
		
		$form->addSelect(
			'state',
			'Stav',
			[
				'dc' => 'Nezáleží',
				Animal::STATE_NEW => 'Nově přidáno',
				Animal::STATE_TAKEAWAY_READY => 'K odběru',
				Animal::STATE_VIRTUAL_READY => 'K virtuální adopci',
				Animal::STATE_PLACED => 'Umístěno',
			])
			->setRequired('Vyberte stav prosím')
			->setDefaultValue('dc');
		
		$form->addCheckbox('castrated', 'Kastrováno');
		
		$form->addMultiSelect('types', 'Druhy', $this->animalTypeRepo->findAll()->orderBy('popularity', ICollection::DESC)->fetchPairs('id', 'name'));
		
		
		$form->addSelect('depo', 'Depozitum', $this->depoRepo->findAll()->fetchPairs('id', 'name'))
			->setPrompt('Vyberte depozitum');
		
		$form->addMultiSelect('handicaps', 'Handicapy', $this->handicapRepo->findAll()->fetchPairs('id', 'name'));
		
		$form->addSubmit('submit', 'Filtrovat');
		$form->addSubmit('reset', 'Reset');
		
		return $form;
	}
}