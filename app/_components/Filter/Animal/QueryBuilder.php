<?php
namespace Kocky\Components\Filter\Animal;

use Kocky\Model\Repository\AnimalRepository;
use Nette\Application\UI\Form;
use Nextras\Dbal\QueryBuilder\QueryBuilder as QB;

/**
 * Class QueryBuilder
 * @package Kocky\Components\Filter\Animal
 */
class QueryBuilder
{
	/**
	 * @param \Nette\Application\UI\Form $form
	 * @param \Kocky\Model\Repository\AnimalRepository $repo
	 * @return \Nextras\Dbal\QueryBuilder\QueryBuilder
	 */
	public static function build(Form $form, AnimalRepository $repo) : QB
	{
		$values = $form->getValues();
		/** @var QB $qb */
		$qb = $repo->getMapper()->builder();
		$qb->from('[animal]', 'a');
		
		if ($values->castrated === true) {
			$qb->andWhere('castrated = %b', true);
		}
		if ($values->types) {
			$qb->andWhere('animal_type_id IN %i[]', $values->types);
		}
		if ($values->handicaps) {
			$qb->innerJoin('a', '[animal_x_handicap]', 'h', '[a.id] = [h.animal_id] AND [h.handicap_id] IN %i[]', $values->handicaps);
		}
		if ($values->depo) {
			$qb->andWhere('depo_id = %i', $values->depo);
		}
		if ($values->gender !== 'dc') {
			$qb->andWhere('gender = %i', $values->gender);
		}
		if ($values->state !== 'dc') {
			$qb->andWhere('state = %s', $values->state);
		}
		$trimmedName = trim($values->name);
		if ($trimmedName !== '') {
			$qb->andWhere('name LIKE %s', '%' . $trimmedName . '%');
		}
		
		return $qb;
	}
}
