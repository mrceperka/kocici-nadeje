<?php

namespace Kocky\Components\Filter\Animal;

use Nette\Application\UI\Control;

class AnimalFilterControl extends Control
{
	/** @var  AnimalFilterFormFactory */
	protected $animalFilterFormFactory;
	
	/**
	 * AnimalFilterControl constructor.
	 * @param AnimalFilterFormFactory $animalFilterFormFactory
	 */
	public function __construct(AnimalFilterFormFactory $animalFilterFormFactory)
	{
		$this->animalFilterFormFactory = $animalFilterFormFactory;
	}
	
	
	public function render()
	{
		$this->template->setFile(__DIR__ . '/filter.latte');
		$this->template->render();
	}
	
	public function createComponentFilter()
	{
		$filter = $this->animalFilterFormFactory->create();
		return $filter;
	}
	
	public function getFilter()
	{
		return $this->getComponent('filter');
	}
	
}

interface IAnimalFilterControlFactory
{
	/**
	 * @return AnimalFilterControl
	 */
	public function create();
}