<?php

namespace Kocky\Components\Control\Account\Contact;


use Kocky\Lib\Authenticator\BaseAuthorizator;
use Kocky\Lib\Nette\Security\NextrasUser;
use Kocky\Model\Repository\ContactRepository;
use Kocky\Model\Service\UserService;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Control;
use Nextras\Application\UI\SecuredLinksControlTrait;

class UserContactListControl extends Control
{
	use SecuredLinksControlTrait;
	
	/** @var  UserService */
	protected $userService;
	
	/** @var  NextrasUser */
	protected $user;
	
	/** @var  ContactRepository */
	protected $contactRepo;
	
	/** @var  BaseAuthorizator */
	protected $authorizator;
	
	/**
	 * UserContactListControl constructor.
	 * @param UserService $userService
	 * @param NextrasUser $user
	 * @param ContactRepository $contactRepo
	 * @param BaseAuthorizator $authorizator
	 */
	public function __construct(UserService $userService, NextrasUser $user, ContactRepository $contactRepo, BaseAuthorizator $authorizator)
	{
		$this->userService = $userService;
		$this->user = $user;
		$this->contactRepo = $contactRepo;
		$this->authorizator = $authorizator;
	}
	
	
	public function render()
	{
		$this->template->contacts = $this->user->getEntity()->contacts;
		
		$this->template->setFile(__DIR__ . '/userContactList.latte');
		$this->template->render();
	}
	
	
	/**
	 * @secured
	 * @param $contactId
	 * @throws BadRequestException
	 */
	public function handleDeleteContact($contactId)
	{
		$contact = $this->contactRepo->getById($contactId);
		if (!$contact) {
			throw new BadRequestException('Contact does not exits');
		}
		
		if ($this->authorizator->isAllowed($this->user, $contact, 'delete')) {
			$this->userService->deleteContact($this->user->getEntity(), $contact);
			$this->getPresenter()->flashMessage('Kontakt byl odstraněn');
		} else {
			$this->getPresenter()->flashMessage('Nemáte takový kontakt', 'error');
		}
		
		$this->getPresenter()->redirect(':Account:Default:default');
	}
	
	
}

interface IUserContactListControlFactory
{
	/**
	 * @return UserContactListControl
	 */
	public function create();
}