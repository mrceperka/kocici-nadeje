<?php

namespace Kocky\Components\Control\Account\Contact\AE;


use Kocky\Components\Form\BaseForm;
use Kocky\Components\Form\Contact\ContactFormFactory;
use Kocky\Lib\Nette\Security\NextrasUser;
use Kocky\Model\Entity\Contact;
use Kocky\Model\Service\ContactService;
use Kocky\Model\Service\UserService;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class UserContactAEControl extends Control
{
	
	/** @var  ContactFormFactory */
	private $contactFormFactory;
	
	/** @var  UserService */
	private $userService;
	
	/** @var  ContactService */
	private $contactService;
	
	/** @var  NextrasUser */
	private $user;
	
	/** @var Contact */
	private $contact = null;
	
	/**
	 * UserContactControl constructor.
	 * @param ContactFormFactory $contactFormFactory
	 * @param UserService $userService
	 * @param ContactService $contactService
	 * @param NextrasUser $user
	 */
	public function __construct(ContactFormFactory $contactFormFactory, UserService $userService, ContactService $contactService, NextrasUser $user)
	{
		$this->contactFormFactory = $contactFormFactory;
		$this->userService = $userService;
		$this->contactService = $contactService;
		$this->user = $user;
	}
	
	
	public function render()
	{
		$this->template->setFile(__DIR__ . '/userContactAE.latte');
		$this->template->render();
	}
	
	private function makeAddForm(Form $form)
	{
		$form->onSuccess[] = function (BaseForm $form) {
			$this->userService->addContact($this->user->getEntity(), $form->getValues());
			$this->getPresenter()->flashMessage('Kontakt přidán', 'success');
			$this->getPresenter()->redirect(':Account:Default:default');
		};
		
		return $form;
	}
	
	
	public function createComponentContactForm()
	{
		$form = $this->contactFormFactory->create();
		
		if ($this->getPresenter()->getAction() == 'edit') {
			$form = $this->makeEditForm($form);
		} else {
			$form = $this->makeAddForm($form);
		}
		
		return $form;
	}
	
	private function makeEditForm(Form $form)
	{
		if ($this->contact == null) {
			throw new \LogicException('Contact not injected');
		}
		
		$form->getComponent('submit')->caption = 'Uložit';
		
		$form->setDefaults([
			'name' => $this->contact->name,
			'email' => $this->contact->email,
			'telephone' => $this->contact->telephone,
			'facebook' => $this->contact->facebook,
			'url' => $this->contact->url
		]);
		
		$form->onSuccess[] = function (BaseForm $form) {
			$this->contactService->edit($this->contact, $form->getValues());
			$this->getPresenter()->flashMessage('Kontakt byl upraven', 'success');
			$this->getPresenter()->redirect(':Account:Default:default');
		};
		
		return $form;
	}
	
	/**
	 * @param Contact $contact
	 */
	public function setContact(Contact $contact)
	{
		$this->contact = $contact;
	}
	
	
}

interface IUserContactAEControlFactory
{
	/**
	 * @return UserContactAEControl
	 */
	public function create();
}