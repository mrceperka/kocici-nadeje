<?php

namespace Kocky\Components\Control\AnimalType;


use Kocky\Components\Form\BaseForm;
use Kocky\Components\Form\Handicap\HandicapFormFactory;
use Kocky\Model\Entity\Handicap;
use Kocky\Model\Service\HandicapService;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class HandicapControl extends Control
{

	/** @var  HandicapFormFactory */
	private $handicapFormFactory;
	
	/** @var  HandicapService */
	private $handicapService;
	
	/** @var  Handicap */
	private $handicap;
	
	/**
	 * AddHandicapControl constructor.
	 * @param HandicapFormFactory $handicapFormFactory
	 * @param HandicapService $handicapService
	 */
	public function __construct(HandicapFormFactory $handicapFormFactory, HandicapService $handicapService)
	{
		$this->handicapFormFactory = $handicapFormFactory;
		$this->handicapService = $handicapService;
	}
	
	
	public function render()
	{
		$this->template->setFile(__DIR__ . '/handicap.latte');
		$this->template->render();
	}
	
	public function createComponentHandicapForm()
	{
		$form = $this->handicapFormFactory->create();
		if($this->getPresenter()->getAction() == 'edit') {
			$form = $this->makeEditForm($form);
		} else {
			$form = $this->makeAddForm($form);
		}
		return $form;
	}
	
	private function makeAddForm(Form $form) {
		$form->onSuccess[] = function(BaseForm $form) {
			$this->handicapService->add($form->getValues());
			$this->getPresenter()->flashMessage('Handicap byl vytvořen', 'success');
			$this->getPresenter()->redirect(':Handicap:Default:list');
		};
		
		return $form;
	}
	
	private function makeEditForm(Form $form)
	{
		if ($this->handicap == null) {
			throw new \LogicException('Handicap not injected');
		}
		
		$form->getComponent('submit')->caption = 'Uložit';
		$form->setDefaults([
			'name' => $this->handicap->name,
			'description' => $this->handicap->description,
			'severity' => $this->handicap->severity
		]);
		
		$form->onSuccess[] = function(BaseForm $form){
			$this->handicapService->edit($this->handicap, $form->getValues());
			$this->getPresenter()->flashMessage('Handicap byl upraven', 'success');
			$this->getPresenter()->redirect(':Handicap:Default:list');
		};
		
		return $form;
	}
	
	public function setHandicap(Handicap $handicap)
	{
		$this->handicap = $handicap;
	}
}

interface IHandicapControlFactory
{
	/**
	 * @return HandicapControl
	 */
	public function create();
}