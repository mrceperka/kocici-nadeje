<?php

namespace Kocky\Components\Control\User\Roster\Item;

use Kocky\Components\Form\User\Role\UserRoleFormFactory;
use Kocky\Decorators\Form\WithOnChangeSubmit;
use Kocky\Lib\Authenticator\BaseAuthorizator;
use Kocky\Lib\Nette\Security\NextrasUser;
use Kocky\Model\Entity\User;
use Kocky\Model\Service\UserService;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nextras\Application\UI\SecuredLinksControlTrait;

class UserRosterItemControl extends Control
{
	use SecuredLinksControlTrait;
	
	/** @var User */
	protected $user = null;
	
	/** @var  UserRoleFormFactory */
	protected $userRoleFormFactory;
	
	/** @var  BaseAuthorizator */
	protected $authorizator;
	
	/** @var  NextrasUser */
	protected $editor;
	
	/** @var  UserService */
	protected $userService;
	
	/**
	 * UserRosterItemControl constructor.
	 * @param UserRoleFormFactory $userRoleFormFactory
	 * @param BaseAuthorizator $authorizator
	 * @param NextrasUser $editor
	 * @param UserService $userService
	 */
	public function __construct(UserRoleFormFactory $userRoleFormFactory, BaseAuthorizator $authorizator, NextrasUser $editor, UserService $userService)
	{
		$this->userRoleFormFactory = $userRoleFormFactory;
		$this->authorizator = $authorizator;
		$this->editor = $editor;
		$this->userService = $userService;
	}
	
	
	public function render()
	{
		$this->template->u = $this->user;
		
		$this->template->setFile(__DIR__ . '/userRosterItem.latte');
		$this->template->render();
	}
	
	/**
	 * @param User $user
	 */
	public function setUser(User $user)
	{
		$this->user = $user;
	}
	
	public function createComponentRoleForm()
	{
		$form = $this->userRoleFormFactory->create();
		
		//$form = WithAjax::decorate($form);
		$form = WithOnChangeSubmit::decorate($form, 'roles');
		
		$form->getComponent('roles')->setValue($this->user->role);
		
		$form->onSuccess[] = function (Form $form) {
			if ($this->authorizator->isAllowed($this->editor, 'user', 'edit')) {
				$this->userService->setRoles($this->user, $form->getValues());
				$this->getPresenter()->flashMessage('Role uživatele byly upraveny', 'success');
			} else {
				$this->getPresenter()->flashMessage('Nedostatečná oprávnění');
			}
			
			$this->redirect('this');
		};
		
		return $form;
	}
	
	/**
	 * @secured
	 */
	public function handleDeactivate()
	{
		if ($this->authorizator->isAllowed($this->editor, 'user', 'deactivate')) {
			$this->userService->deactivate($this->user);
			$this->getPresenter()->flashMessage('Uživatel byl deaktivován');
		} else {
			$this->getPresenter()->flashMessage('Nedostatečná oprávnění');
		}
		$this->redirect('this');
	}
	
	/**
	 * @secured
	 */
	public function handleActivate()
	{
		if ($this->authorizator->isAllowed($this->editor, 'user', 'activate')) {
			$this->userService->activate($this->user);
			$this->getPresenter()->flashMessage('Uživatel byl aktivován');
		} else {
			$this->getPresenter()->flashMessage('Nedostatečná oprávnění');
		}
		$this->redirect('this');
	}
}

interface IUserRosterItemControlFactory
{
	/**
	 * @return UserRosterItemControl
	 */
	public function create();
}