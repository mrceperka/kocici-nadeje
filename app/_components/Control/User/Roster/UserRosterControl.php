<?php

namespace Kocky\Components\Control\User\Roster;


use Kocky\Components\Control\User\Roster\Item\IUserRosterItemControlFactory;
use Kocky\Model\Repository\UserRepository;
use Nette\Application\UI\Control;
use Nette\Application\UI\Multiplier;
use Nextras\Orm\Collection\ICollection;

class UserRosterControl extends Control
{
	/** @var  ICollection */
	protected $users;
	
	/** @var  UserRepository */
	protected $userRepo;
	
	/** @var  IUserRosterItemControlFactory */
	protected $userRosterItemFactory;
	
	/**
	 * UserRosterControl constructor.
	 * @param UserRepository $userRepo
	 * @param IUserRosterItemControlFactory $userRosterItemFactory
	 */
	public function __construct(UserRepository $userRepo, IUserRosterItemControlFactory $userRosterItemFactory)
	{
		$this->userRepo = $userRepo;
		$this->userRosterItemFactory = $userRosterItemFactory;
	}
	
	
	public function render()
	{
		$this->template->users = $this->users;
		
		$this->template->setFile(__DIR__ . '/userRoster.latte');
		$this->template->render();
	}
	
	public function createComponentRosterItem()
	{
		return new Multiplier(function ($id) {
			$control = $this->userRosterItemFactory->create();
			foreach ($this->users as $user) {
				if($user->id == $id) {
					$control->setUser($user);
					break;
				}
			}
			return $control;
		});
	}
	
	/**
	 * Users have to be indexed by theirs id
	 * @param ICollection $users
	 */
	public function setUsers(ICollection $users)
	{
		$this->users = $users;
	}
	
	
}

interface IUserRosterControlFactory
{
	/**
	 * @return UserRosterControl
	 */
	public function create();
}