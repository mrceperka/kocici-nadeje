<?php

namespace Kocky\Components\Control\User\Detail;


use Kocky\Lib\Authenticator\BaseAuthorizator;
use Kocky\Lib\Nette\Security\NextrasUser;
use Kocky\Model\Entity\User;
use Kocky\Model\Repository\UserRepository;
use Nette\Application\UI\Control;

class UserDetailControl extends Control
{
	/** @var  User */
	protected $u;
	
	/** @var  NextrasUser */
	protected $user;
	
	/** @var  UserRepository */
	protected $userRepo;
	
	/** @var  BaseAuthorizator */
	protected $authorizator;
	
	/**
	 * UserDetailControl constructor.
	 * @param NextrasUser $user
	 * @param UserRepository $userRepo
	 * @param BaseAuthorizator $authorizator
	 */
	public function __construct(NextrasUser $user, UserRepository $userRepo, BaseAuthorizator $authorizator)
	{
		$this->user = $user;
		$this->userRepo = $userRepo;
		$this->authorizator = $authorizator;
	}
	
	
	public function render()
	{
		$this->template->u = $this->u;
		
		$this->template->setFile(__DIR__ . '/userDetail.latte');
		$this->template->render();
	}
	
	/**
	 * @param User $user
	 */
	public function setUser(User $user)
	{
		$this->u = $user;
	}
	
	
}

interface IUserDetailControlFactory
{
	/**
	 * @return UserDetailControl
	 */
	public function create();
}