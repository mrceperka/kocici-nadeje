<?php

namespace Kocky\Components\Control\AnimalType;


use Kocky\Components\Form\AnimalType\AnimalTypeFormFactory;
use Kocky\Components\Form\BaseForm;
use Kocky\Model\Entity\AnimalType;
use Kocky\Model\Service\AnimalTypeService;
use Nette\Application\UI\Control;

class AnimalTypeControl extends Control
{
	/** @var  AnimalTypeFormFactory */
	private $animalTypeFormFactory;
	
	/** @var  AnimalTypeService */
	private $animalTypeService;
	
	/** @var  AnimalType */
	private $animalType;
	
	/**
	 * AddAnimalTypeControl constructor.
	 * @param AnimalTypeFormFactory $animalTypeFormFactory
	 * @param AnimalTypeService $animalTypeService
	 */
	public function __construct(AnimalTypeFormFactory $animalTypeFormFactory, AnimalTypeService $animalTypeService)
	{
		$this->animalTypeFormFactory = $animalTypeFormFactory;
		$this->animalTypeService = $animalTypeService;
	}
	
	
	public function render()
	{
		$this->template->action = $this->getPresenter()->getAction();
		$this->template->setFile(__DIR__ . '/animalType.latte');
		$this->template->render();
	}
	
	public function createComponentAddAnimalTypeForm()
	{
		$form = $this->animalTypeFormFactory->create();
		$form->onSuccess[] = function(BaseForm $form){
			$this->animalTypeService->add($form->getValues());
			$this->getPresenter()->flashMessage('Druh zvířete byl vytvořen', 'success');
			$this->getPresenter()->redirect(':AnimalType:Default:list');
		};
		
		return $form;
	}
	
	public function createComponentEditAnimalTypeForm()
	{
		if ($this->animalType == null) {
			throw new \LogicException('AnimalType not injected');
		}
		
		$form = $this->animalTypeFormFactory->create();
		$form->getComponent('submit')->caption = 'Uložit';
		$form->setDefaults([
			'name' => $this->animalType->name,
			'popularity' => $this->animalType->popularity
		]);
		
		$form->onSuccess[] = function(BaseForm $form){
			$this->animalTypeService->edit($this->animalType, $form->getValues());
			$this->getPresenter()->flashMessage('Druh zvířete byl upraven', 'success');
			$this->getPresenter()->redirect(':AnimalType:Default:list');
		};
		
		return $form;
	}
	
	/**
	 * @param AnimalType $animalType
	 */
	public function setAnimalType(AnimalType $animalType)
	{
		$this->animalType = $animalType;
	}
	
	
	
}

interface IAnimalTypeControlFactory
{
	/**
	 * @return AnimalTypeControl
	 */
	public function create();
}