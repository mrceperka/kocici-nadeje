<?php

namespace Kocky\Components\Control\Gallery;


use Kocky\Lib\Authenticator\BaseAuthorizator;
use Kocky\Lib\Nette\Security\NextrasUser;
use Kocky\Model\Entity\Animal;
use Kocky\Model\Repository\UserImageRepository;
use Kocky\Model\Service\AnimalService;
use Nette\Application\UI\Control;
use Nextras\Application\UI\SecuredLinksControlTrait;

class GalleryControl extends Control
{
	use SecuredLinksControlTrait;
	
	/** @var  Animal */
	protected $animal;
	
	/** @var  AnimalService */
	protected $animalService;
	
	/** @var  UserImageRepository */
	protected $userImageRepo;
	
	/** @var  NextrasUser */
	protected $user;
	
	/** @var  BaseAuthorizator */
	protected $authorizator;
	
	/**
	 * GalleryControl constructor.
	 * @param AnimalService $animalService
	 * @param UserImageRepository $userImageRepo
	 * @param NextrasUser $user
	 * @param BaseAuthorizator $authorizator
	 */
	public function __construct(AnimalService $animalService, UserImageRepository $userImageRepo, NextrasUser $user, BaseAuthorizator $authorizator)
	{
		$this->animalService = $animalService;
		$this->userImageRepo = $userImageRepo;
		$this->user = $user;
		$this->authorizator = $authorizator;
	}
	
	
	public function render()
	{
		$this->template->images = $this->animal->images;
		$this->template->animal = $this->animal;
		
		$this->template->authorizator = $this->authorizator;
		$this->template->setFile(__DIR__ . '/gallery.latte');
		$this->template->render();
	}
	
	/**
	 * @secured
	 * @param $id
	 */
	public function handleDelete($id)
	{
		if ($this->authorizator->isAllowed($this->user, $this->animal, 'edit')) {
			$image = $this->userImageRepo->getById($id);
			if($image) {
				$this->animalService->deleteImage($this->animal, $image);
			} else {
				$this->getPresenter()->flashMessage('Obrázek neexistuje', 'error');
			}
		} else {
			$this->getPresenter()->flashMessage('Nedostatečná oprávnění', 'error');
		}
		
		if($this->getPresenter()->isAjax()) {
			$this->redrawControl('animalImages');
		} else {
			$this->redirect('this');
		}
	}
	
	public function handleDefaultImage($id)
	{
		if ($this->authorizator->isAllowed($this->user, $this->animal, 'edit')) {
			$image = $this->userImageRepo->getById($id);
			if ($image) {
				$this->animalService->setProfileImage($this->animal, $image);
			} else {
				$this->getPresenter()->flashMessage('Obrázek neexistuje', 'error');
			}
		} else {
			$this->getPresenter()->flashMessage('Nedostatečná oprávnění', 'error');
		}
		$this->redirect('this');
	}
	
	/**
	 * @param Animal $animal
	 */
	public function setAnimal(Animal $animal)
	{
		$this->animal = $animal;
	}
	
	
	
}

interface IGalleryControlFactory
{
	/**
	 * @return GalleryControl
	 */
	public function create();
}