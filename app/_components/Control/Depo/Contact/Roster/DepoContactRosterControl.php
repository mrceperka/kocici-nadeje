<?php

namespace Kocky\Components\Control\Depo\Contact\Roster;


use Kocky\Lib\Authenticator\BaseAuthorizator;
use Kocky\Lib\Nette\Security\NextrasUser;
use Kocky\Model\Entity\Depo;
use Kocky\Model\Repository\ContactRepository;
use Kocky\Model\Service\DepoService;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Control;

class DepoContactRosterControl extends Control
{
	/** @var  Depo */
	protected $depo = null;
	
	/** @var  BaseAuthorizator */
	protected $authorizator;
	
	/** @var  NextrasUser */
	protected $user;
	
	/** @var  ContactRepository */
	protected $contactRepo;
	
	/** @var  DepoService */
	protected $depoService;
	
	/**
	 * DepoContactListControl constructor.
	 * @param BaseAuthorizator $authorizator
	 * @param NextrasUser $user
	 * @param ContactRepository $contactRepo
	 * @param DepoService $depoService
	 */
	public function __construct(BaseAuthorizator $authorizator, NextrasUser $user, ContactRepository $contactRepo, DepoService $depoService)
	{
		$this->authorizator = $authorizator;
		$this->user = $user;
		$this->contactRepo = $contactRepo;
		$this->depoService = $depoService;
	}
	
	
	public function render()
	{
		$this->template->contacts = $this->depo->contacts;
		$this->template->depo = $this->depo;
		
		$this->template->authorizator = $this->authorizator;
		$this->template->setFile(__DIR__ . '/depoContactRoster.latte');
		$this->template->render();
	}
	
	
	/**
	 * @secured
	 * @param $contactId
	 * @throws BadRequestException
	 */
	public function handleDeleteContact($contactId)
	{

		$contact = $this->contactRepo->getById($contactId);
		if (!$contact) {
			throw new BadRequestException('Contact does not exits');
		}
		
		if ($this->authorizator->isAllowed($this->user, $this->depo, 'deleteContact')) {
			if ($this->depo->contacts->has($contact) === false) {
				$this->getPresenter()->flashMessage('Depozitum nemá takový kontakt', 'error');
			} else {
				$this->depoService->deleteContact($this->depo, $contact);
				$this->getPresenter()->flashMessage('Kontakt byl odstraněn', 'warn');
			}
		} else {
			$this->getPresenter()->flashMessage('Nedostatečná oprávnění', 'error');
		}
		$this->getPresenter()->redirect(':Depo:Default:default', $this->depo->id);
	}
	
	/**
	 * @param Depo $depo
	 */
	public function setDepo(Depo $depo)
	{
		$this->depo = $depo;
	}
	
	
}

interface IDepoContactRosterControlFactory
{
	/**
	 * @return DepoContactRosterControl
	 */
	public function create();
}