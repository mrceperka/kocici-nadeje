<?php

namespace Kocky\Components\Control\Depo\Contact;


use Kocky\Components\Form\BaseForm;
use Kocky\Components\Form\Contact\ContactFormFactory;
use Kocky\Model\Entity\Contact;
use Kocky\Model\Entity\Depo;
use Kocky\Model\Service\ContactService;
use Kocky\Model\Service\DepoService;
use Nette\Application\UI\Control;

class DepoContactControl extends Control
{
	
	/** @var  ContactFormFactory */
	private $contactFormFactory;
	
	/** @var  DepoService */
	private $depoService;
	
	/** @var  ContactService */
	private $contactService;
	
	/** @var  Depo */
	private $depo = null;
	
	/** @var Contact */
	private $contact = null;
	
	/**
	 * DepoContactControl constructor.
	 * @param ContactFormFactory $contactFormFactory
	 * @param DepoService $depoService
	 * @param ContactService $contactService
	 */
	public function __construct(ContactFormFactory $contactFormFactory, DepoService $depoService, ContactService $contactService)
	{
		$this->contactFormFactory = $contactFormFactory;
		$this->depoService = $depoService;
		$this->contactService = $contactService;
	}
	
	
	public function render()
	{
		$this->template->action = $this->getPresenter()->getAction();
		$this->template->setFile(__DIR__ . '/depoContact.latte');
		$this->template->render();
	}
	
	public function createComponentAddContactForm()
	{
		if($this->depo == null) {
			throw new \LogicException('Depo not injected');
		}
		
		$form = $this->contactFormFactory->create();
		$form->onSuccess[] = function(BaseForm $form){
			$this->depo = $this->depoService->addContact($this->depo, $form->getValues());
			$this->getPresenter()->flashMessage('Kontakt přidán', 'success');
			$this->getPresenter()->redirect(':Depo:Default:default', ['id' => $this->depo->id]);
		};
		
		return $form;
	}
	
	public function createComponentEditContactForm()
	{
		if($this->depo == null || $this->contact == null) {
			throw new \LogicException('Depo or contact not injected');
		}
		
		$form = $this->contactFormFactory->create();
		$form->getComponent('submit')->caption = 'Uložit';
		
		$form->setDefaults([
			'name' => $this->contact->name,
			'email' => $this->contact->email,
			'telephone' => $this->contact->telephone,
			'facebook' => $this->contact->facebook,
			'url' => $this->contact->url
		]);
		
		$form->onSuccess[] = function(BaseForm $form){
			$this->contactService->edit($this->contact, $form->getValues());
			$this->getPresenter()->flashMessage('Kontakt depozita byl upraven', 'success');
			$this->getPresenter()->redirect(':Depo:Default:default', ['id' => $this->depo->id]);
		};
		
		return $form;
	}
	
	/**
	 * @param Depo $depo
	 */
	public function setDepo(Depo $depo)
	{
		$this->depo = $depo;
	}
	
	/**
	 * @param Contact $contact
	 */
	public function setContact(Contact $contact)
	{
		$this->contact = $contact;
	}
	
	
}

interface IDepoContactControlFactory
{
	/**
	 * @return DepoContactControl
	 */
	public function create();
}