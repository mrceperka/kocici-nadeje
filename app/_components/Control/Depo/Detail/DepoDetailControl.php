<?php

namespace Kocky\Components\Control\Depo\Detail;


use Kocky\Components\Control\Depo\Animal\IDepoAnimalControlFactory;
use Kocky\Components\Control\Depo\Animal\IDepoAnimalListControlFactory;
use Kocky\Components\Control\Depo\Contact\Roster\IDepoContactRosterControlFactory;
use Kocky\Components\Control\Depo\Member\AE\IDepoMemberAEControlFactory;
use Kocky\Components\Control\Depo\Member\Roster\IDepoMemberRosterControlFactory;
use Kocky\Components\Control\Depo\Roster\IDepoRosterControlFactory;
use Kocky\Lib\Authenticator\BaseAuthorizator;
use Kocky\Lib\Nette\Security\NextrasUser;
use Kocky\Model\Entity\Depo;
use Nette\Application\UI\Control;

class DepoDetailControl extends Control
{
	/** @var  Depo */
	protected $depo;
	
	/** @var  NextrasUser */
	protected $user;
	
	/** @var  BaseAuthorizator */
	protected $authorizator;
	
	/** @var  IDepoAnimalControlFactory */
	protected $depoAnimalControlFactory;
	
	/** @var  IDepoAnimalListControlFactory */
	protected $animalListControlFactory;
	
	/** @var  IDepoContactRosterControlFactory */
	protected $contactListControlFactory;
	
	/** @var  IDepoRosterControlFactory */
	protected $depoRosterControlFactory;
	
	/** @var  IDepoMemberRosterControlFactory */
	protected $depoMemberRosterControlFactory;
	
	/** @var  IDepoMemberAEControlFactory */
	protected $depoMemberAEControlFactory;
	
	/**
	 * DepoDetailControl constructor.
	 * @param NextrasUser $user
	 * @param BaseAuthorizator $authorizator
	 * @param IDepoAnimalControlFactory $depoAnimalControlFactory
	 * @param IDepoAnimalListControlFactory $animalListControlFactory
	 * @param IDepoContactRosterControlFactory $contactListControlFactory
	 * @param IDepoRosterControlFactory $depoRosterControlFactory
	 * @param IDepoMemberRosterControlFactory $depoMemberRosterControlFactory
	 * @param IDepoMemberAEControlFactory $depoMemberAEControlFactory
	 */
	public function __construct(NextrasUser $user, BaseAuthorizator $authorizator, IDepoAnimalControlFactory $depoAnimalControlFactory, IDepoAnimalListControlFactory $animalListControlFactory, IDepoContactRosterControlFactory $contactListControlFactory, IDepoRosterControlFactory $depoRosterControlFactory, IDepoMemberRosterControlFactory $depoMemberRosterControlFactory, IDepoMemberAEControlFactory $depoMemberAEControlFactory)
	{
		$this->user = $user;
		$this->authorizator = $authorizator;
		$this->depoAnimalControlFactory = $depoAnimalControlFactory;
		$this->animalListControlFactory = $animalListControlFactory;
		$this->contactListControlFactory = $contactListControlFactory;
		$this->depoRosterControlFactory = $depoRosterControlFactory;
		$this->depoMemberRosterControlFactory = $depoMemberRosterControlFactory;
		$this->depoMemberAEControlFactory = $depoMemberAEControlFactory;
	}
	
	
	public function render()
	{
		$this->template->depo = $this->depo;
		$this->template->authorizator = $this->authorizator;
		
		$this->template->setFile(__DIR__ . '/depoDetail.latte');
		$this->template->render();
	}
	
	public function createComponentAddAnimal()
	{
		$dac = $this->depoAnimalControlFactory->create();
		$dac->setDepo($this->depo);
		return $dac;
	}
	
	public function createComponentAnimalList()
	{
		$dalc = $this->animalListControlFactory->create();
		$dalc->setDepo($this->depo);
		return $dalc;
	}
	
	public function createComponentContactList()
	{
		$dclc = $this->contactListControlFactory->create();
		$dclc->setDepo($this->depo);
		return $dclc;
	}
	
	public function createComponentDepoRoster()
	{
		$drc = $this->depoRosterControlFactory->create();
		$drc->setDepos($this->depoRepo->findAll());
		return $drc;
	}
	
	public function createComponentDepoMemberRoster()
	{
		$dmrc = $this->depoMemberRosterControlFactory->create();
		$dmrc->setDepo($this->depo);
		return $dmrc;
	}
	
	public function createComponentDepoMemberAE()
	{
		$dmaec = $this->depoMemberAEControlFactory->create();
		$dmaec->setDepo($this->depo);
		return $dmaec;
	}
	
	/**
	 * @param Depo $depo
	 */
	public function setDepo(Depo $depo)
	{
		$this->depo = $depo;
	}
	
	
}

interface IDepoDetailControlFactory
{
	/**
	 * @return DepoDetailControl
	 */
	public function create();
}