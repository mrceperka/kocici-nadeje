<?php

namespace Kocky\Components\Control\Depo\Member\Roster\Item;


use Kocky\Lib\Authenticator\BaseAuthorizator;
use Kocky\Lib\Nette\Security\NextrasUser;
use Kocky\Model\AtLeastOneNeeded;
use Kocky\Model\Entity\UserDepo;
use Kocky\Model\Service\DepoService;
use Nette\Application\UI\Control;
use Nextras\Application\UI\SecuredLinksPresenterTrait;

class DepoMemberRosterItemControl extends Control
{
	use SecuredLinksPresenterTrait;
	
	/** @var  UserDepo */
	protected $userDepo;
	
	/** @var  NextrasUser */
	protected $user;
	
	/** @var  BaseAuthorizator */
	protected $authorizator;
	
	/** @var  DepoService */
	protected $depoService;
	
	/**
	 * DepoMemberRosterItemControl constructor.
	 * @param NextrasUser $user
	 * @param BaseAuthorizator $authorizator
	 * @param DepoService $depoService
	 */
	public function __construct(NextrasUser $user, BaseAuthorizator $authorizator, DepoService $depoService)
	{
		$this->user = $user;
		$this->authorizator = $authorizator;
		$this->depoService = $depoService;
	}
	
	
	public function render()
	{
		$this->template->userDepo = $this->userDepo;
		$this->template->authorizator = $this->authorizator;
		
		$this->template->setFile(__DIR__ . '/depoMemberRosterItem.latte');
		$this->template->render();
	}
	
	/**
	 * @param UserDepo $userDepo
	 */
	public function setUserDepo(UserDepo $userDepo)
	{
		$this->userDepo = $userDepo;
	}
	
	
	/**
	 * @secured
	 * @param $id
	 */
	public function handleDelete($id) {
		if($this->authorizator->isAllowed($this->user, $this->userDepo->depo, 'deleteMember')) {
			try {
				$this->depoService->deleteMember($this->userDepo);
				$this->getPresenter()->flashMessage('Člen byl odstraněn');
			} catch (AtLeastOneNeeded $e) {
				$this->getPresenter()->flashMessage('Depozitum musí mít alespoň jednoho člena');
			}
			
		} else {
			$this->getPresenter()->flashMessage('Nedostatečná oprávnění');
		}
		$this->redirect('this');
	}
	
	
}

interface IDepoMemberRosterItemControlFactory
{
	/**
	 * @return DepoMemberRosterItemControl
	 */
	public function create();
}