<?php

namespace Kocky\Components\Control\Depo\Member\Roster;


use Kocky\Components\Control\Depo\Member\Roster\Item\IDepoMemberRosterItemControlFactory;
use Kocky\Lib\Authenticator\BaseAuthorizator;
use Kocky\Model\Entity\Depo;
use Nette\Application\UI\Control;
use Nette\Application\UI\Multiplier;

class DepoMemberRosterControl extends Control
{
	/** @var  Depo */
	protected $depo;
	
	/** @var  BaseAuthorizator */
	protected $authorizator;
	
	/** @var  IDepoMemberRosterItemControlFactory */
	protected $depoMemberRosterItemFactory;
	
	/**
	 * DepoMemberRosterControl constructor.
	 * @param BaseAuthorizator $authorizator
	 * @param IDepoMemberRosterItemControlFactory $depoMemberRosterItemFactory
	 */
	public function __construct(BaseAuthorizator $authorizator, IDepoMemberRosterItemControlFactory $depoMemberRosterItemFactory)
	{
		$this->authorizator = $authorizator;
		$this->depoMemberRosterItemFactory = $depoMemberRosterItemFactory;
	}
	
	
	public function render()
	{
		$this->template->depo = $this->depo;
		$this->template->authorizator = $this->authorizator;
		
		$this->template->setFile(__DIR__ . '/depoMemberRoster.latte');
		$this->template->render();
	}
	
	/**
	 * @param Depo $depo
	 */
	public function setDepo(Depo $depo)
	{
		$this->depo = $depo;
	}
	
	public function createComponentRosterItem()
	{
		return new Multiplier(function ($id) {
			$control = $this->depoMemberRosterItemFactory->create();
			foreach ($this->depo->userDepos as $udp) {
				if($udp->id == $id) {
					$control->setUserDepo($udp);
					break;
				}
			}
			return $control;
		});
	}
	
	
}

interface IDepoMemberRosterControlFactory
{
	/**
	 * @return DepoMemberRosterControl
	 */
	public function create();
}