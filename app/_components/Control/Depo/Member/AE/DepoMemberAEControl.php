<?php

namespace Kocky\Components\Control\Depo\Member\AE;


use Kocky\Components\Form\Depo\Member\AddMemberFormFactory;
use Kocky\Lib\Authenticator\BaseAuthorizator;
use Kocky\Lib\Nette\Security\NextrasUser;
use Kocky\Model\Entity\Depo;
use Kocky\Model\Service\DepoService;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nextras\Orm\Collection\ICollection;

class DepoMemberAEControl extends Control
{
	/** @var  Depo */
	protected $depo;
	
	/** @var  ICollection */
	protected $users;
	
	/** @var  AddMemberFormFactory */
	protected $addMemberFormFactory;
	
	/** @var  DepoService */
	protected $depoService;
	
	/** @var  NextrasUser */
	protected $user;
	
	/** @var  BaseAuthorizator */
	protected $authorizator;
	
	/**
	 * DepoMemberAEControl constructor.
	 * @param AddMemberFormFactory $addMemberFormFactory
	 * @param DepoService $depoService
	 * @param NextrasUser $user
	 * @param BaseAuthorizator $authorizator
	 */
	public function __construct(AddMemberFormFactory $addMemberFormFactory, DepoService $depoService, NextrasUser $user, BaseAuthorizator $authorizator)
	{
		$this->addMemberFormFactory = $addMemberFormFactory;
		$this->depoService = $depoService;
		$this->user = $user;
		$this->authorizator = $authorizator;
	}
	
	
	public function render()
	{
		$this->template->users = $this->users;
		$this->template->depo = $this->depo;
		$this->template->authorizator = $this->authorizator;
		
		$this->template->setFile(__DIR__ . '/depoMemberAE.latte');
		$this->template->render();
	}
	
	public function createComponentMemberForm()
	{
		$form = $this->addMemberFormFactory->create();
		$form
			->getComponent('users')
			->setItems(
				$this->users->fetchPairs('id', 'email')
			);
		
		$form->onSuccess[] = function (Form $form) {
			if ($this->authorizator->isAllowed($this->user, $this->depo, 'addMember')) {
				$this->depoService->addMembers($this->depo, $form->getValues());
			} else {
				$this->getPresenter()->flashMessage('Nedostatečná oprávnění');
			}
			$this->redirect('this');
		};
		return $form;
		
	}
	
	/**
	 * @param Depo $depo
	 */
	public function setDepo(Depo $depo)
	{
		$this->depo = $depo;
		$this->users = $this->depoService->getUsersAddableToDepo($this->depo);
	}
	
	
}

interface IDepoMemberAEControlFactory
{
	/**
	 * @return DepoMemberAEControl
	 */
	public function create();
}