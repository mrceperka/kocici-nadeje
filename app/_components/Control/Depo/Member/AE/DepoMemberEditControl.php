<?php

namespace Kocky\Components\Control\Depo\Member\AE;


use Kocky\Components\Form\Depo\Member\EditMemberFormFactory;
use Kocky\Lib\Authenticator\BaseAuthorizator;
use Kocky\Lib\Nette\Security\NextrasUser;
use Kocky\Model\Entity\UserDepo;
use Kocky\Model\Service\DepoService;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class DepoMemberEditControl extends Control
{
	/** @var  UserDepo */
	protected $userDepo;
	
	/** @var  EditMemberFormFactory */
	protected $editMemberFormFactory;
	
	/** @var  DepoService */
	protected $depoService;
	
	/** @var  NextrasUser */
	protected $user;
	
	/** @var  BaseAuthorizator */
	protected $authorizator;
	
	/**
	 * DepoMemberEditControl constructor.
	 * @param EditMemberFormFactory $editMemberFormFactory
	 * @param DepoService $depoService
	 * @param NextrasUser $user
	 * @param BaseAuthorizator $authorizator
	 */
	public function __construct(EditMemberFormFactory $editMemberFormFactory, DepoService $depoService, NextrasUser $user, BaseAuthorizator $authorizator)
	{
		$this->editMemberFormFactory = $editMemberFormFactory;
		$this->depoService = $depoService;
		$this->user = $user;
		$this->authorizator = $authorizator;
	}
	
	
	public function render()
	{
		$this->template->depo = $this->userDepo->depo;
		$this->template->authorizator = $this->authorizator;
		
		$this->template->setFile(__DIR__ . '/depoMemberEdit.latte');
		$this->template->render();
	}
	
	public function createComponentMemberForm()
	{
		$form = $this->editMemberFormFactory->create();
		$form->getComponent('relation')->setValue($this->userDepo->relation);
		
		$form->onSuccess[] = function (Form $form) {
			if ($this->authorizator->isAllowed($this->user, $this->userDepo->depo, 'editMember')) {
				$this->depoService->editMemberRelation($this->userDepo, $form->getValues());
				$this->getPresenter()->flashMessage('Vztah uživatele k depozitu byl upraven');
			} else {
				$this->getPresenter()->flashMessage('Nedostatečná oprávnění');
			}
			$this->getPresenter()->redirect(':Depo:Default:default', $this->userDepo->depo->id);
		};
		
		return $form;
	}
	
	public function setUserDepo(UserDepo $userDepo)
	{
		$this->userDepo = $userDepo;
	}
	
}

interface IDDepoMemberEditControlFactory
{
	/**
	 * @return DepoMemberEditControl
	 */
	public function create();
}