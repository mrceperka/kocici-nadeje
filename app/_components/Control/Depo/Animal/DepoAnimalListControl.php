<?php

namespace Kocky\Components\Control\Depo\Animal;


use Kocky\Lib\Authenticator\BaseAuthorizator;
use Kocky\Lib\Nette\Security\NextrasUser;
use Kocky\Model\Entity\Depo;
use Kocky\Model\Repository\AnimalRepository;
use Kocky\Model\Service\DepoService;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Control;
use Nextras\Application\UI\SecuredLinksControlTrait;

class DepoAnimalListControl extends Control
{
	use SecuredLinksControlTrait;
	
	/** @var Depo */
	protected $depo = null;
	
	/** @var  BaseAuthorizator */
	protected $authorizator;
	
	/** @var  NextrasUser */
	protected $user;
	
	/** @var  DepoService */
	protected $depoService;
	
	/** @var  AnimalRepository */
	protected $animalRepo;
	
	/**
	 * DepoAnimalListControl constructor.
	 * @param BaseAuthorizator $authorizator
	 * @param NextrasUser $user
	 * @param DepoService $depoService
	 * @param AnimalRepository $animalRepo
	 */
	public function __construct(BaseAuthorizator $authorizator, NextrasUser $user, DepoService $depoService, AnimalRepository $animalRepo)
	{
		$this->authorizator = $authorizator;
		$this->user = $user;
		$this->depoService = $depoService;
		$this->animalRepo = $animalRepo;
	}
	
	
	public function render()
	{
		$this->template->animals = $this->depo->animals;
		$this->template->depo = $this->depo;
		
		$this->template->authorizator = $this->authorizator;
		$this->template->setFile(__DIR__ . '/depoAnimalList.latte');
		$this->template->render();
	}
	
	/**
	 * @secured
	 * @param $animalId
	 * @throws BadRequestException
	 */
	public function handleDeleteAnimal($animalId)
	{
		$animal = $this->animalRepo->getById($animalId);
		if(!$animal) throw new BadRequestException('Animal does not exist');
		
		if ($this->authorizator->isAllowed($this->user, $this->depo, 'deleteAnimal')) {
			if ($this->depo->animals->has($animal) === false) {
				$this->getPresenter()->flashMessage('Depozitum nemá takové zvíře', 'error');
			} else {
				$this->depoService->deleteAnimal($this->depo, $animal);
				$this->getPresenter()->flashMessage('Zvíře bylo odstraněno', 'warn');
			}
		} else {
			$this->getPresenter()->flashMessage('Nedostatečná oprávnění', 'error');
		}
		$this->getPresenter()->redirect(':Depo:Default:default', $this->depo->id);
	}
	
	/**
	 * @param Depo $depo
	 */
	public function setDepo(Depo $depo)
	{
		$this->depo = $depo;
	}
	
}

interface IDepoAnimalListControlFactory
{
	/**
	 * @return DepoAnimalListControl
	 */
	public function create();
}