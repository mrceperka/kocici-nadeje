<?php

namespace Kocky\Components\Control\Depo\Animal;


use Kocky\Components\Form\BaseForm;
use Kocky\Components\Form\Depo\DepoAnimalFormFactory;
use Kocky\Lib\Authenticator\BaseAuthorizator;
use Kocky\Lib\Nette\Security\NextrasUser;
use Kocky\Model\Entity\Depo;
use Kocky\Model\Repository\AnimalRepository;
use Kocky\Model\Service\DepoOverflow;
use Kocky\Model\Service\DepoService;
use Nette\Application\UI\Control;

class DepoAnimalControl extends Control
{
	/** @var  Depo */
	protected $depo = null;
	
	/** @var  DepoAnimalFormFactory */
	protected $daff;
	
	/** @var  DepoService */
	protected $depoService;
	
	/** @var  AnimalRepository */
	protected $animalRepo;
	
	/** @var  NextrasUser */
	protected $user;
	
	/** @var  BaseAuthorizator */
	protected $authorizator;
	
	/**
	 * DepoAnimalControl constructor.
	 * @param DepoAnimalFormFactory $daff
	 * @param DepoService $depoService
	 * @param AnimalRepository $animalRepo
	 * @param NextrasUser $user
	 * @param BaseAuthorizator $authorizator
	 */
	public function __construct(DepoAnimalFormFactory $daff, DepoService $depoService, AnimalRepository $animalRepo, NextrasUser $user, BaseAuthorizator $authorizator)
	{
		$this->daff = $daff;
		$this->depoService = $depoService;
		$this->animalRepo = $animalRepo;
		$this->user = $user;
		$this->authorizator = $authorizator;
	}
	
	
	public function render()
	{
		
		$this->template->depo = $this->depo;
		
		$this->template->authorizator = $this->authorizator;
		$this->template->setFile(__DIR__ . '/depoAnimal.latte');
		$this->template->render();
	}
	
	public function createComponentAddAnimal()
	{
		$form = $this->daff->create();
		$form->getComponent('animals')->setItems(
			$this->depo->getAddableAnimals()->fetchPairs('id', 'name')
		);;
		
		$form->onSuccess[] = function (BaseForm $form) {
			try {
				if($this->authorizator->isAllowed($this->user, $this->depo, 'addAnimal')) {
					$this->depoService->addAnimals($this->depo, $form->getValues());
					$this->getPresenter()->flashMessage('Přidáno ' . count($form->getValues()->animals) . ' zvířat');
				} else {
					$this->getPresenter()->flashMessage('Nedostatečná oprávnění', 'error');
				}
				$this->redirect('this');
			} catch (DepoOverflow $e) {
				$form->addCommonError('Nelze přidat více zvířat než je kapacita depozita');
			}
			
		};
		
		return $form;
	}
	
	
	/**
	 * @param Depo $depo
	 */
	public function setDepo(Depo $depo)
	{
		$this->depo = $depo;
	}
	
}

interface IDepoAnimalControlFactory
{
	/**
	 * @return DepoAnimalControl
	 */
	public function create();
}