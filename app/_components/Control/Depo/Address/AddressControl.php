<?php

namespace Kocky\Components\Control\Depo\Address;


use Kocky\Components\Form\BaseForm;
use Kocky\Components\Form\Depo\AddressFormFactory;
use Kocky\Model\Entity\Address;
use Kocky\Model\Entity\Depo;
use Kocky\Model\Service\AddressAlreadyExists;
use Kocky\Model\Service\AddressService;
use Kocky\Model\Service\DepoService;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class AddressControl extends Control
{
	
	/** @var  AddressFormFactory */
	private $addressFormFactory;
	
	/** @var  DepoService */
	private $depoService;
	
	/** @var  AddressService */
	private $addressService;
	
	/** @var  Depo */
	private $depo = null;
	
	/** @var Address */
	private $address = null;
	
	/**
	 * AddressControl constructor.
	 * @param AddressFormFactory $addressFormFactory
	 * @param DepoService $depoService
	 * @param AddressService $addressService
	 */
	public function __construct(AddressFormFactory $addressFormFactory, DepoService $depoService, AddressService $addressService)
	{
		$this->addressFormFactory = $addressFormFactory;
		$this->depoService = $depoService;
		$this->addressService = $addressService;
	}
	
	
	public function render()
	{
		$this->template->setFile(__DIR__ . '/address.latte');
		$this->template->render();
	}
	
	public function createComponentAddressForm()
	{
		if ($this->depo == null) {
			throw new \LogicException('Depo not injected');
		}
		
		$form = $this->addressFormFactory->create();
		
		if($this->getPresenter()->getAction() == 'edit') {
			$form = $this->makeEditForm($form);
		} else {
			$form = $this->makeAddForm($form);
		}
		
		return $form;
	}
	
	private function makeAddForm(Form $form)
	{
		$form->onSuccess[] = function (BaseForm $form) {
			try {
				$this->depoService->addAddress($this->depo, $form->getValues());
				$this->getPresenter()->flashMessage('Adresa přidána', 'success');
			} catch (AddressAlreadyExists $e) {
				$this->getPresenter()->flashMessage('Depozitum již jednu adresu má', 'warn');
			}
			
			$this->getPresenter()->redirect(':Depo:Default:default', ['id' => $this->depo->id]);
		};
		
		return $form;
	}
	
	private function makeEditForm(Form $form)
	{
		$form->getComponent('submit')->caption = 'Uložit';
		
		$form->setDefaults(
			[
				'city' => $this->address->city,
				'street' => $this->address->street,
				'zip' => $this->address->zip,
				'country' => $this->address->country
			]
		);
		
		$form->onSuccess[] = function (BaseForm $form) {
			$this->addressService->edit($this->address, $form->getValues());
			$this->getPresenter()->flashMessage('Adresa depozita byla upravena', 'success');
			$this->getPresenter()->redirect(':Depo:Default:default', ['id' => $this->depo->id]);
		};
		
		return $form;
	}
	
	/**
	 * @param Depo $depo
	 */
	public function setDepo(Depo $depo)
	{
		$this->depo = $depo;
	}
	
	/**
	 * @param Address $address
	 */
	public function setAddress(Address $address)
	{
		$this->address = $address;
	}
	
	
}

interface IAddressControlFactory
{
	/**
	 * @return AddressControl
	 */
	public function create();
}