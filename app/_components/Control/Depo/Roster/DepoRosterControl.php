<?php

namespace Kocky\Components\Control\Depo\Roster;


use Nette\Application\UI\Control;
use Nextras\Orm\Collection\ICollection;

class DepoRosterControl extends Control
{
	/** @var  ICollection */
	protected $depos;
	
	public function render()
	{
		$this->template->depos = $this->depos;
		$this->template->setFile(__DIR__ . '/depoRoster.latte');
		$this->template->render();
	}
	
	/**
	 * @param ICollection $depos
	 */
	public function setDepos(ICollection $depos)
	{
		$this->depos = $depos;
	}
}

interface IDepoRosterControlFactory
{
	/**
	 * @return DepoRosterControl
	 */
	public function create();
}