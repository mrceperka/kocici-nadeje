<?php

namespace Kocky\Components\Control\Depo\AE;


use Kocky\Components\Form\BaseForm;
use Kocky\Components\Form\Depo\DepoFormFactory;
use Kocky\Lib\Nette\Security\NextrasUser;
use Kocky\Model\Entity\Depo;
use Kocky\Model\Service\DepoService;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class DepoAEControl extends Control
{
	/** @var  NextrasUser */
	private $user;
	
	/** @var  DepoFormFactory */
	private $depoFormFactory;
	
	/** @var  DepoService */
	private $depoService;
	
	/** @var Depo */
	private $depo = null;
	
	/**
	 * AddDepoControl constructor.
	 * @param NextrasUser $user
	 * @param DepoFormFactory $depoFormFactory
	 * @param DepoService $depoService
	 */
	public function __construct(NextrasUser $user, DepoFormFactory $depoFormFactory, DepoService $depoService)
	{
		$this->user = $user;
		$this->depoFormFactory = $depoFormFactory;
		$this->depoService = $depoService;
	}
	
	
	public function render()
	{
		$this->template->setFile(__DIR__ . '/depoAE.latte');
		$this->template->render();
	}
	
	public function createComponentDepoForm()
	{
		$form = $this->depoFormFactory->create();
		if ($this->getPresenter()->getAction() == 'edit') {
			$form = $this->makeEditForm($form);
		} else {
			$form = $this->makeAddForm($form);
		}
		
		return $form;
	}
	
	private function makeAddForm(Form $form)
	{
		$form->onSuccess[] = function (BaseForm $form) {
			$depo = $this->depoService->add($form->getValues());
			$this->getPresenter()->flashMessage('Depozitum vytvořeno', 'success');
			$this->getPresenter()->redirect(':Depo:Default:default', ['id' => $depo->id]);
		};
		
		return $form;
	}
	
	private function makeEditForm(Form $form)
	{
		if ($this->depo == null) {
			throw new \LogicException('Depo not injected');
		}
		
		$form->getComponent('submit')->caption = 'Uložit';
		$form->getComponent('capacity')
			->addRule(Form::MIN, 'Kapacita nemůže být nižší než počet zvířat v depozitu', $this->depo->animals->count());
		
		$form->setDefaults([
			'name' => $this->depo->name,
			'capacity' => $this->depo->capacity
		]);
		
		$form->onSuccess[] = function (BaseForm $form) {
			$this->depo = $this->depoService->edit($this->depo, $form->getValues());
			$this->getPresenter()->flashMessage('Depozitum upraveno', 'success');
			$this->getPresenter()->redirect(':Depo:Default:default', ['id' => $this->depo->id]);
		};
		
		return $form;
	}
	
	/**
	 * @param Depo $depo
	 */
	public function setDepo(Depo $depo)
	{
		$this->depo = $depo;
	}
	
	
}

interface IDepoAEControlFactory
{
	/**
	 * @return DepoAEControl
	 */
	public function create();
}