<?php

namespace Kocky\Components\Control\Animal\Roster\Item;


use Kocky\Model\Entity\Animal;
use Nette\Application\UI\Control;

class AnimalRosterItemControl extends Control
{
	/**
	 * @var Animal
	 */
	protected $animal = null;
	
	public function render()
	{
		$this->template->animal = $this->animal;
		
		$this->template->setFile(__DIR__ . '/animalRosterItem.latte');
		$this->template->render();
	}
	
	/**
	 * @param Animal $animal
	 */
	public function setAnimal(Animal $animal)
	{
		$this->animal = $animal;
	}
	
	public function createComponentSelectRole()
	{
		
	}
}

interface IAnimalRosterItemControlFactory
{
	/**
	 * @return AnimalRosterItemControl
	 */
	public function create();
}