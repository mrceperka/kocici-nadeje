<?php

namespace Kocky\Components\Control\Animal\Roster;


use Kocky\Components\Control\Animal\Roster\Item\IAnimalRosterItemControlFactory;
use Nette\Application\UI\Control;
use Nette\Application\UI\Multiplier;
use Nextras\Orm\Collection\ICollection;

class AnimalRosterControl extends Control
{
	/** @var  ICollection */
	protected $animals;
	
	/** @var  IAnimalRosterItemControlFactory */
	protected $animalRosterItemFactory;
	
	/**
	 * AnimalRosterControl constructor.
	 * @param IAnimalRosterItemControlFactory $animalRosterItemFactory
	 */
	public function __construct(IAnimalRosterItemControlFactory $animalRosterItemFactory)
	{
		$this->animalRosterItemFactory = $animalRosterItemFactory;
	}
	
	
	public function render()
	{
		$this->template->animals = $this->animals;
		
		$this->template->setFile(__DIR__ . '/animalRoster.latte');
		$this->template->render();
	}
	
	public function createComponentRosterItem()
	{
		return new Multiplier(function ($id) {
			$control = $this->animalRosterItemFactory->create();
			foreach ($this->animals as $animal) {
				if($animal->id == $id) {
					$control->setAnimal($animal);
					break;
				}
			}
			return $control;
		});
	}
	
	/**
	 * Animals have to be indexed by theirs id
	 * @param ICollection $animals
	 */
	public function setAnimals(ICollection $animals)
	{
		$this->animals = $animals;
	}
	
	
}

interface IAnimalRosterControlFactory
{
	/**
	 * @return AnimalRosterControl
	 */
	public function create();
}