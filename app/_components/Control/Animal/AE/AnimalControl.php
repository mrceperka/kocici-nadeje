<?php

namespace Kocky\Components\Control\Animal\AE;


use Kocky\Components\Control\Gallery\IGalleryControlFactory;
use Kocky\Components\Form\Animal\AnimalFormFactory;
use Kocky\Components\Form\BaseForm;
use Kocky\Lib\Nette\Security\NextrasUser;
use Kocky\Model\Entity\Animal;
use Kocky\Model\Service\AnimalService;
use Kocky\Model\Service\DepoOverflow;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Utils\Html;

class AnimalAEControl extends Control
{
	/** @var  NextrasUser */
	protected $user;
	
	/** @var  AnimalFormFactory */
	protected $animalFormFactory;
	
	/** @var  AnimalService */
	protected $animalService;
	
	/** @var  Animal */
	protected $animal = null;
	
	/** @var  IGalleryControlFactory */
	protected $animalGalleryControlFactory;
	
	/**
	 * AnimalControl constructor.
	 * @param NextrasUser $user
	 * @param AnimalFormFactory $animalFormFactory
	 * @param AnimalService $animalService
	 * @param IGalleryControlFactory $animalImageControlFactory
	 */
	public function __construct(NextrasUser $user, AnimalFormFactory $animalFormFactory, AnimalService $animalService, IGalleryControlFactory $animalImageControlFactory)
	{
		$this->user = $user;
		$this->animalFormFactory = $animalFormFactory;
		$this->animalService = $animalService;
		$this->animalGalleryControlFactory = $animalImageControlFactory;
	}
	
	
	public function render()
	{
		$this->template->action = $this->getPresenter()->getAction();
		$this->template->animal = $this->animal;
		$this->template->setFile(__DIR__ . '/animalAE.latte');
		$this->template->render();
	}
	
	public function createComponentAnimalForm()
	{
		$form = $this->animalFormFactory->create();
		if($this->getPresenter()->getAction() == 'edit') {
			$form = $this->makeEditForm($form);
		} else {
			$form = $this->makeAddForm($form);
		}
		
		return $form;
	}
	
	private function makeAddForm(Form $form) {
		$form->onSuccess[] = function(BaseForm $form){
			try {
				$this->animalService->add($form->getValues());
				$this->getPresenter()->flashMessage('Zvíře bylo vytvořeno', 'success');
				$this->redirect('this');
			} catch (DepoOverflow $e) {
				$form->getComponent('depo')->addError('Depozitum je již plné');
			}
		};
		
		return $form;
	}
	
	private function makeEditForm(Form $form)
	{
		if($this->animal == null) {
			throw new \LogicException('Animal not injected');
		}

		$form->getComponent('submit')->caption = 'Uložit';

		if($this->animal->depo !== null) {
			/** @var Html $item */
			foreach ($form->getComponent('depo')->items as $item) {
				if($item->getAttribute('value') == $this->animal->depo->id) {
					$item->setAttribute('disabled', false);
				}
			}
		}
		
		$form->setDefaults([
			'name' => $this->animal->name,
			'age' => $this->animal->age,
			'gender' => $this->animal->gender,
			'castrated' => $this->animal->castrated,
			'type' => $this->animal->animalType->id,
			'depo' => $this->animal->depo ? $this->animal->depo->id : null,
			'state' => $this->animal->state,
			'handicaps' => $this->animal->handicaps->get()->fetchPairs(null, 'id')
		]);
		
		$form->onSuccess[] = function(BaseForm $form){
			try {
				$this->animalService->edit($this->animal, $form->getValues());
				$this->getPresenter()->flashMessage('Zvíře bylo upraveno', 'success');
				$this->redirect('this');
			} catch (DepoOverflow $e) {
				$form->getComponent('depo')->addError('Depozitum je již plné');
			}
		};
		
		return $form;
	}
	
	public function createComponentAnimalGallery()
	{
		$animalGalleryControl = $this->animalGalleryControlFactory->create();
		$animalGalleryControl->setAnimal($this->animal);
		return $animalGalleryControl;
	}
	
	public function setAnimal(Animal $animal)
	{
		$this->animal = $animal;
	}
}

interface IAnimalAEControlFactory
{
	/**
	 * @return AnimalAEControl
	 */
	public function create();
}