<?php
namespace Kocky\Components\Control\Animal\Teaser\Item;

use Kocky\Model\Entity\Animal;
use Nette\Application\UI\Control;

class AnimalTeaserItemControl extends Control
{
	/** @var  Animal */
	public $animal;
	
	public function render()
	{
		$this->template->animal = $this->animal;
		$this->template->setFile(__DIR__ . '/animalTeaserItem.latte');
		$this->template->render();
	}
	
	/**
	 * @param Animal $animal
	 */
	public function setAnimal(Animal $animal)
	{
		$this->animal = $animal;
	}
	
}

interface IAnimalTeaserItemControlFactory
{
	/**
	 * @return AnimalTeaserItemControl
	 */
	public function create();
}