<?php
namespace Kocky\Components\Control\Animal\Teaser;

use Kocky\Components\Control\Animal\Teaser\Item\IAnimalTeaserItemControlFactory;
use Nette\Application\UI\Control;
use Nette\Application\UI\Multiplier;
use Nextras\Orm\Collection\ICollection;

class AnimalTeaserControl extends Control
{
	/** @var  ICollection */
	public $animals;
	
	/** @var  IAnimalTeaserItemControlFactory */
	public $animalTeaserItemControlFactory;
	
	/**
	 * AnimalTeaserControl constructor.
	 * @param IAnimalTeaserItemControlFactory $animalTeaserItemControlFactory
	 */
	public function __construct(IAnimalTeaserItemControlFactory $animalTeaserItemControlFactory)
	{
		$this->animalTeaserItemControlFactory = $animalTeaserItemControlFactory;
	}
	
	
	public function render()
	{
		$this->template->animals = $this->animals;
		$this->template->setFile(__DIR__ . '/animalTeaser.latte');
		$this->template->render();
	}
	
	public function setAnimals(ICollection $animals)
	{
		$this->animals = $animals;
	}
	
	public function createComponentTeaserItem()
	{
		return new Multiplier(function($id) {
			foreach ($this->animals as $animal) {
				if($animal->id == $id) {
					$atic = $this->animalTeaserItemControlFactory->create();
					$atic->setAnimal($animal);
					return $atic;
				}
			}
		});
	}
}

interface IAnimalTeaserControlFactory
{
	/**
	 * @return AnimalTeaserControl
	 */
	public function create();
}