<?php

namespace Kocky\Components\Control\Story;


use Kocky\Components\Form\Story\StoryFormFactory;
use Kocky\Lib\Authenticator\BaseAuthorizator;
use Kocky\Lib\Nette\Security\NextrasUser;
use Kocky\Model\Entity\Animal;
use Kocky\Model\Entity\Story;
use Kocky\Model\Service\StoryService;
use Nette\Application\UI\Control;
use Nette\Application\UI\Multiplier;
use Nette\Forms\Form;
use Nextras\Application\UI\SecuredLinksControlTrait;

class StoryControl extends Control
{
	use SecuredLinksControlTrait;
	
	/** @var  Animal */
	protected $animal;
	
	/** @var  Story */
	protected $story;
	
	/** @var  StoryFormFactory */
	protected $storyFormFactory;
	
	/** @var StoryService */
	protected $storyService;
	
	/** @var  NextrasUser */
	protected $user;
	
	/** @var  BaseAuthorizator */
	protected $authorizator;
	
	/**
	 * StoryControl constructor.
	 * @param StoryFormFactory $storyFormFactory
	 * @param StoryService $storyService
	 * @param NextrasUser $user
	 * @param BaseAuthorizator $authorizator
	 */
	public function __construct(StoryFormFactory $storyFormFactory, StoryService $storyService, NextrasUser $user, BaseAuthorizator $authorizator)
	{
		$this->storyFormFactory = $storyFormFactory;
		$this->storyService = $storyService;
		$this->user = $user;
		$this->authorizator = $authorizator;
	}
	
	
	public function render()
	{
		$this->template->stories = $this->animal->stories;
		$this->template->animal = $this->animal;
		
		$this->template->authorizator = $this->authorizator;
		$this->template->setFile(__DIR__ . '/story.latte');
		$this->template->render();
	}
	
	public function createComponentAddStoryForm()
	{
		return $this->storyFormFactory->create();
	}
	
	public function createComponentEdit()
	{
		return new Multiplier(function ($id) {
			$this->story = $this->storyService->getStoryRepo()->getById($id);
			$form = $this->storyFormFactory->create();
			$form->getComponent('submit')->caption = 'Uložit';
			
			$form->setDefaults([
				'text' => $this->story->text
			]);
			
			$form->onSuccess[] = function (Form $form) {
				if($this->authorizator->isAllowed($this->user, $this->animal, 'edit')) {
					$this->storyService->editStory($this->story, $form->getValues());
					$this->getPresenter()->flashMessage('Informace upravena');
				} else {
					$this->getPresenter()->flashMessage('Nedostatečná oprávnění', 'error');
				}
				$this->redirect('this#info' . $this->story->id);
				
			};
			return $form;
		});
	}
	
	/**
	 * @secured
	 * @param $id
	 */
	public function handleDelete($id)
	{
		if ($this->authorizator->isAllowed($this->user, $this->animal, 'edit')) {
			/** @var Story $story */
			$story = $this->storyService->getStoryRepo()->getById($id);
			if ($story) {
				$this->getPresenter()->flashMessage('Informace smazána', 'success');
				$this->storyService->deleteAnimalStory($this->animal, $story);
			} else {
				$this->getPresenter()->flashMessage('Informace neexistuje', 'error');
			}
		} else {
			$this->getPresenter()->flashMessage('Nedostatečná oprávnění', 'error');
		}
		$this->redirect('this');
	}
	
	public function setAnimal(Animal $animal)
	{
		$this->animal = $animal;
	}
	
	public function getForm()
	{
		return $this->getComponent('addStoryForm');
	}
	
}

interface IStoryControlFactory
{
	/**
	 * @return StoryControl
	 */
	public function create();
}