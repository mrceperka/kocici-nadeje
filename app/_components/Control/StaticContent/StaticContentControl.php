<?php

namespace Kocky\Components\Control\StaticContent;

use Kocky\Components\Form\StaticContent\StaticContentFormFactory;
use Kocky\Lib\Authenticator\BaseAuthorizator;
use Kocky\Lib\Nette\Security\NextrasUser;
use Kocky\Model\Entity\StaticContent;
use Kocky\Model\Service\StaticContentService;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Application\UI\Multiplier;


class StaticContentControl extends Control
{
	/** @var  StaticContent */
	protected $content = null;
	
	/** @var  StaticContentService */
	protected $staticContentService;
	
	/** @var  BaseAuthorizator */
	protected $authorizator;
	
	/** @var  NextrasUser */
	protected $user;
	
	/** @var StaticContentFormFactory */
	protected $staticContentFormFactory;
	
	/**
	 * StaticContentControl constructor.
	 * @param StaticContentService $staticContentService
	 * @param BaseAuthorizator $authorizator
	 * @param NextrasUser $user
	 * @param StaticContentFormFactory $staticContentFormFactory
	 */
	public function __construct(StaticContentService $staticContentService, BaseAuthorizator $authorizator, NextrasUser $user, StaticContentFormFactory $staticContentFormFactory)
	{
		$this->staticContentService = $staticContentService;
		$this->authorizator = $authorizator;
		$this->user = $user;
		$this->staticContentFormFactory = $staticContentFormFactory;
	}
	
	
	public function render()
	{
		$this->template->content = $this->content;
		$this->template->authorizator = $this->authorizator;
		
		$this->template->setFile(__DIR__ . '/staticContent.latte');
		$this->template->render();
	}
	
	public function createComponentEdit()
	{
		return new Multiplier(function ($id) {
			$form = $this->staticContentFormFactory->create();
			$form->getComponent('submit')->caption = 'Uložit';
			$form->setDefaults([
				'text' => $this->content->text,
			]);
			$form->onSuccess[] = function (Form $form) {
				if ($this->authorizator->isAllowed($this->user, $this->content, 'edit')) {
					$this->content = $this->staticContentService->edit($this->content, $form->getValues());
					$this->getPresenter()->flashMessage('Obsah upraven');
				} else {
					$this->getPresenter()->flashMessage('Nedostatečná oprávnění', 'error');
				}
				$this->redirect('this#info-content-' . $this->content->id);
				
			};
			return $form;
		});
	}
	
	/**
	 * @param StaticContent $content
	 */
	public function setContent(StaticContent $content)
	{
		$this->content = $content;
	}
	
	public function setWrapperClasses(array $classes)
	{
		$this->config['wrapper']['classes'] = $classes;
	}
	
	
}

interface IStaticContentControlFactory
{
	/**
	 * @return StaticContentControl
	 */
	public function create();
}