<?php

namespace Kocky;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;
use Nextras\Routing\StaticRouter;


class RouterFactory
{
	/**
	 * @return Nette\Application\IRouter
	 */
	public function create()
	{
		$router = new RouteList;
		
		$router[] = new StaticRouter([
			'Common:Register:default' => 'registrace',
			'Common:Sign:in' => 'prihlaseni',
			'Account:Default:default' => 'ucet',

		]);
		
		
		$router[] = new Route('<presenter>[/<action>][/<id>][/<id2>]', 'Common:Homepage:default');
		return $router;
	}

}
