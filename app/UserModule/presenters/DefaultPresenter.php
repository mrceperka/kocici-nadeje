<?php
namespace Kocky\UserModule\Presenters;

use Kocky\Components\Control\User\Detail\IUserDetailControlFactory;
use Kocky\Components\Control\User\Roster\IUserRosterControlFactory;
use Kocky\Model\Entity\User;
use Kocky\Model\Repository\UserRepository;
use Nextras\Orm\Collection\ArrayCollection;
use Nextras\Orm\Collection\ICollection;

class DefaultPresenter extends BasePresenter
{
	/** @var  User */
	public $u;
	
	/** @var  ICollection */
	public $users;
	
	/** @var  UserRepository @inject */
	public $userRepo;
	
	/** @var  IUserRosterControlFactory @inject */
	public $userRosterControlFactory;
	
	/** @var  IUserDetailControlFactory @inject */
	public $userDetailControlFactory;
	
	public function actionDefault($id)
	{
		$this->u = $this->userRepo->getById($id);
	}
	
	public function actionList()
	{
		if ($this->authorizator->isAllowed($this->user, 'user', 'edit') === false) {
			$this->flashMessage('Nedostatečná oprávnění');
			$this->redirect(':Common:Homepage:default');
		}
		
		$this->users = new ArrayCollection(
			$this->userRepo->findAllWithDeleted()->fetchPairs('id'),
			$this->userRepo
		);
	}
	
	public function renderDefault($id)
	{
		$this->template->u = $this->userRepo->getById($id);
	}
	
	public function createComponentUserList()
	{
		$urc = $this->userRosterControlFactory->create();
		$urc->setUsers($this->users);
		return $urc;
	}
	
	public function createComponentUserDetail()
	{
		$udc = $this->userDetailControlFactory->create();
		$udc->setUser($this->u);
		
		return $udc;
	}
	
}