<?php
namespace Kocky\Model;

/**
 * Class ModelException
 * @package Kocky\Model
 */
class ModelException extends \Exception
{
}

class AtLeastOneNeeded extends ModelException
{
	
}