<?php
namespace Kocky\Model\Repository;


use Kocky\Model\ARepository;
use Kocky\Model\Entity\Address;
use Kocky\Model\Traits\Repository\DeletableRepository;

class AddressRepository extends ARepository
{
	use DeletableRepository;
	
	static function getEntityClassNames()
	{
		return [Address::class];
	}
}