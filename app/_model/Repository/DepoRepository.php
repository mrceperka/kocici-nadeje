<?php
namespace Kocky\Model\Repository;


use Kocky\Model\ARepository;
use Kocky\Model\Entity\Depo;
use Kocky\Model\Traits\Repository\DeletableRepository;

/**
 * Class DepoRepository
 * @package Kocky\Model\Repository
 *
 */
class DepoRepository extends ARepository
{
	use DeletableRepository;
	static function getEntityClassNames()
	{
		return [Depo::class];
	}
}