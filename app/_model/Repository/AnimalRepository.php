<?php
namespace Kocky\Model\Repository;


use Kocky\Model\ARepository;
use Kocky\Model\Entity\Animal;
use Kocky\Model\Traits\Repository\DeletableRepository;
use Nextras\Orm\Collection\ICollection;

/**
 * Class AnimalRepository
 * @package Kocky\Model\Repository
 *
 * @method ICollection|Animal[] getAddableAnimals()
 */
class AnimalRepository extends ARepository
{
	use DeletableRepository;
	
	static function getEntityClassNames()
	{
		return [Animal::class];
	}
	
	public function findLatest()
	{
		return $this->findAll()->limitBy(10);
		//return $this->findBy(['created<=']);
	}
	
	public function findLatestWithPicture()
	{
		return $this->findLatest()->findBy(['profileImage!=' => null])->limitBy(6);
		//return $this->findBy(['created<=']);
	}
}