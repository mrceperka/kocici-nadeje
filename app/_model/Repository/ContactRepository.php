<?php
namespace Kocky\Model\Repository;


use Kocky\Model\ARepository;
use Kocky\Model\Entity\Contact;
use Kocky\Model\Traits\Repository\DeletableRepository;

class ContactRepository extends ARepository
{
	use DeletableRepository;
	
	static function getEntityClassNames()
	{
		return [Contact::class];
	}
}