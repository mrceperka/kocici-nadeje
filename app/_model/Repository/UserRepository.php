<?php
namespace Kocky\Model\Repository;


use Kocky\Model\ARepository;
use Kocky\Model\Entity\User;
use Kocky\Model\Traits\Repository\DeletableRepository;

class UserRepository extends ARepository
{
	use DeletableRepository;
	static function getEntityClassNames()
	{
		return [User::class];
	}
	
	public function getOneByEmail($email)
	{
		return $this->getBy(['this->email' => $email]);
	}
}