<?php
namespace Kocky\Model\Repository;


use Kocky\Model\ARepository;
use Kocky\Model\Entity\Handicap;
use Kocky\Model\Traits\Repository\DeletableRepository;

class HandicapRepository extends ARepository
{
	use DeletableRepository;
	
	static function getEntityClassNames()
	{
		return [Handicap::class];
	}
}