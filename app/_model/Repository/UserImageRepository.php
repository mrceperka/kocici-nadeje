<?php
namespace Kocky\Model\Repository;


use Kocky\Model\ARepository;
use Kocky\Model\Entity\UserImage;
use Kocky\Model\Traits\Repository\DeletableRepository;

class UserImageRepository extends ARepository
{
	use DeletableRepository;
	
	static function getEntityClassNames()
	{
		return [UserImage::class];
	}
}