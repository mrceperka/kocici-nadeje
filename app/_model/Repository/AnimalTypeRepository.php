<?php
namespace Kocky\Model\Repository;


use Kocky\Model\ARepository;
use Kocky\Model\Entity\AnimalType;
use Kocky\Model\Traits\Repository\DeletableRepository;

class AnimalTypeRepository extends ARepository
{
	use DeletableRepository;
	
	static function getEntityClassNames()
	{
		return [AnimalType::class];
	}
}