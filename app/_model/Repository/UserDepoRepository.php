<?php
namespace Kocky\Model\Repository;


use Kocky\Model\ARepository;
use Kocky\Model\Entity\UserDepo;
use Kocky\Model\Traits\Repository\DeletableRepository;

class UserDepoRepository extends ARepository
{
	use DeletableRepository;
	static function getEntityClassNames()
	{
		return [UserDepo::class];
	}
}