<?php
namespace Kocky\Model\Repository;


use Kocky\Model\ARepository;
use Kocky\Model\Entity\Story;
use Kocky\Model\Traits\Repository\DeletableRepository;

class StoryRepository extends ARepository
{
	use DeletableRepository;
	
	static function getEntityClassNames()
	{
		return [Story::class];
	}
}