<?php
namespace Kocky\Model\Repository;


use Kocky\Model\ARepository;
use Kocky\Model\Entity\StaticContent;

class StaticContentRepository extends ARepository
{
	
	static function getEntityClassNames()
	{
		return [StaticContent::class];
	}
	
	/**
	 * @return mixed|\Nextras\Orm\Collection\ICollection
	 */
	public function getWholeFooterSection()
	{
		return $this->findBy(['section' => StaticContent::FOOTER]);
	}
	
	public function getFooterSection($position)
	{
		return $this->getWholeFooterSection()->getBy(['position' => $position]);
	}
}