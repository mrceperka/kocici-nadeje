<?php
namespace Kocky\Model\Service;

use Kocky\Model\Entity\Handicap;
use Kocky\Model\Repository\HandicapRepository;

class HandicapService
{
	/** @var  HandicapRepository */
	private $handicapRepo;
	
	/**
	 * HandicapService constructor.
	 * @param HandicapRepository $handicapRepo
	 */
	public function __construct(HandicapRepository $handicapRepo)
	{
		$this->handicapRepo = $handicapRepo;
	}
	
	
	public function add($values)
	{
		$hd = new Handicap();
		$hd->name = $values->name;
		$hd->description = $values->description;
		$hd->severity = $values->severity;
		
		$this->handicapRepo->persistAndFlush($hd);
		return $hd;
	}
	
	public function edit(Handicap $hd, $values)
	{
		$hd->name = $values->name;
		$hd->description = $values->description;
		$hd->severity = $values->severity;
		
		$this->handicapRepo->persistAndFlush($hd);
		
		return $hd;
	}
	
}