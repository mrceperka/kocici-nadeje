<?php
namespace Kocky\Model\Service;

use Kocky\Model\Entity\Address;
use Kocky\Model\Repository\AddressRepository;

class AddressService
{
	/** @var  AddressRepository */
	private $addressRepo;
	
	/**
	 * AddressService constructor.
	 * @param AddressRepository $addressRepo
	 */
	public function __construct(AddressRepository $addressRepo)
	{
		$this->addressRepo = $addressRepo;
	}
	
	public function edit(Address $address, $values)
	{
		$address->city = $values->city;
		$address->zip = $values->zip;
		$address->street = $values->street;
		$address->country = $values->country;
		
		$this->addressRepo->persistAndFlush($address);
		
		return $address;
	}
	
	
}