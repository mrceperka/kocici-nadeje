<?php
namespace Kocky\Model\Service;

use Kocky\Lib\Nette\Security\NextrasUser;
use Kocky\Model\Entity\Animal;
use Kocky\Model\Entity\Story;
use Kocky\Model\Repository\AnimalRepository;
use Kocky\Model\Repository\StoryRepository;

class StoryService
{
	/** @var  AnimalRepository */
	private $animalRepo;
	
	/** @var  StoryRepository */
	private $storyRepo;
	
	/** @var  NextrasUser */
	private $user;
	
	/**
	 * StoryService constructor.
	 * @param AnimalRepository $animalRepo
	 * @param StoryRepository $storyRepo
	 * @param NextrasUser $user
	 */
	public function __construct(AnimalRepository $animalRepo, StoryRepository $storyRepo, NextrasUser $user)
	{
		$this->animalRepo = $animalRepo;
		$this->storyRepo = $storyRepo;
		$this->user = $user;
	}
	
	
	public function addAnimalStory(Animal $animal, $values)
	{
		$story = new Story();
		$story->text = $values->text;
		$story->createdBy = $this->user->getEntity();
		$animal->stories->add($story);
		$this->animalRepo->persistAndFlush($animal);
		
		return $story;
	}
	
	public function deleteAnimalStory(Animal $animal, Story $story)
	{
		$animal->stories->remove($story);
		$this->animalRepo->persistAndFlush($animal);
		$this->storyRepo->removeAndFlush($story);
	}
	
	public function editStory(Story $story, $values)
	{
		$story->text = $values->text;
		$this->storyRepo->persistAndFlush($story);
		return $story;
	}
	
	/**
	 * @return AnimalRepository
	 */
	public function getAnimalRepo(): AnimalRepository
	{
		return $this->animalRepo;
	}
	
	/**
	 * @return StoryRepository
	 */
	public function getStoryRepo(): StoryRepository
	{
		return $this->storyRepo;
	}
	
	
	
	
	
	
}