<?php
namespace Kocky\Model\Service;

use CPTeam\Image\Saver\ImageSaver;
use Kocky\Lib\Nette\Security\NextrasUser;
use Kocky\Model\Entity\Animal;
use Kocky\Model\Entity\Depo;
use Kocky\Model\Entity\Story;
use Kocky\Model\Entity\UserImage;
use Kocky\Model\Repository\AnimalRepository;
use Kocky\Model\Repository\AnimalTypeRepository;
use Kocky\Model\Repository\DepoRepository;
use Kocky\Model\Repository\HandicapRepository;
use Kocky\Model\Repository\UserRepository;
use Nette\Http\FileUpload;

class AnimalService
{
	/** @var  NextrasUser */
	
	private $user;
	/** @var  AnimalRepository */
	private $animalRepo;
	
	/** @var  DepoRepository */
	private $depoRepo;
	
	/** @var  UserRepository */
	private $userRepo;
	
	/** @var  AnimalTypeRepository */
	private $animalTypeRepo;
	
	/** @var  HandicapRepository */
	private $handicapRepo;
	
	/** @var  ImageSaver */
	private $imageSaver;
	
	/**
	 * AnimalService constructor.
	 * @param NextrasUser $user
	 * @param AnimalRepository $animalRepo
	 * @param DepoRepository $depoRepo
	 * @param UserRepository $userRepo
	 * @param AnimalTypeRepository $animalTypeRepo
	 * @param HandicapRepository $handicapRepo
	 * @param ImageSaver $imageSaver
	 */
	public function __construct(NextrasUser $user, AnimalRepository $animalRepo, DepoRepository $depoRepo, UserRepository $userRepo, AnimalTypeRepository $animalTypeRepo, HandicapRepository $handicapRepo, ImageSaver $imageSaver)
	{
		$this->user = $user;
		$this->animalRepo = $animalRepo;
		$this->depoRepo = $depoRepo;
		$this->userRepo = $userRepo;
		$this->animalTypeRepo = $animalTypeRepo;
		$this->handicapRepo = $handicapRepo;
		$this->imageSaver = $imageSaver;
	}
	
	
	public function add($values)
	{
		$animal = new Animal();
		$animal->name = $values->name;
		$animal->age = $values->age;
		$animal->gender = $values->gender;
		$animal->state = $values->state;
		$animal->castrated = $values->castrated;
		$animal->animalType = $this->animalTypeRepo->getById($values->type);
		$animal->createdBy = $this->user->getEntity();
		
		if ($values->depo) {
			/** @var Depo $depo */
			$depo = $this->depoRepo->getById($values->depo);
			if (!$this->canMoveAnimalToDepo($animal, $depo)) {
				throw new DepoOverflow();
			}
			$animal->depo = $depo;
		}
		
		foreach ($this->handicapRepo->findById($values->handicaps) as $hd) {
			$animal->handicaps->add($hd);
		}
		
		$this->addAnimalImages($animal, $values->images);

		if($values->info) {
			$story = new Story();
			$story->text = $values->info;
			$story->createdBy = $this->user->getEntity();
			$animal->stories->add($story);
		}
		
		$this->animalRepo->persistAndFlush($animal);
		
		return $animal;
	}
	
	public function edit(Animal $animal, $values)
	{
		
		$depo = $this->depoRepo->getById($values->depo);
		if (!$this->canMoveAnimalToDepo($animal, $depo)) {
			throw new DepoOverflow();
		}
		$animal->name = $values->name;
		$animal->age = $values->age;
		$animal->gender = $values->gender;
		$animal->state = $values->state;
		$animal->castrated = $values->castrated;
		$animal->animalType = $this->animalTypeRepo->getById($values->type);
		$animal->depo = $depo;
		
		$animal->handicaps->set($this->handicapRepo->findById($values->handicaps)->fetchAll());
		
		$this->addAnimalImages($animal, $values->images);
		
		$this->userRepo->persistAndFlush($this->user->getEntity());
		$this->animalRepo->persistAndFlush($animal);
		
		
		return $animal;
	}
	
	public function setProfileImage(Animal $animal, UserImage $image)
	{
		$animal->profileImage = $image;
		return $this->animalRepo->persistAndFlush($animal);
	}
	
	public function deleteImage(Animal $animal, UserImage $image)
	{
		$animal->images->remove($image);
		if ($animal->profileImage !== null && $animal->profileImage->id === $image->id) {
			$animal->profileImage = null;
		}
		$this->animalRepo->persistAndFlush($animal);
	}
	
	/**
	 * @param Animal $animal
	 * @param Depo $depo
	 * @return bool
	 */
	public function canMoveAnimalToDepo(Animal $animal, Depo $depo)
	{
		return $depo->freeCapacity() !== 0 || $animal->isInDepo($depo);
	}
	
	/**
	 * @return AnimalRepository
	 */
	public function getAnimalRepo()
	{
		return $this->animalRepo;
	}
	
	/**
	 * @return DepoRepository
	 */
	public function getDepoRepo()
	{
		return $this->depoRepo;
	}
	
	private function addAnimalImages(Animal $animal, $images)
	{
		/** @var FileUpload $image */
		foreach ($images as $image) {
			$result = $this->imageSaver->save($image);
			
			$uimg = new UserImage();
			$uimg->name = $image->getName();
			$uimg->user = $this->user->getEntity();
			$uimg->hash = $result->getBasename();
			
			$this->user->getEntity()->images->add($uimg);
			
			// autoset profile image if none present
			if($animal->profileImage === null) {
				$animal = $this->setProfileImage($animal, $uimg);
			}
			
			$animal->images->add($uimg);
		}
	}
	
	
}