<?php
namespace Kocky\Model\Service;

use Kocky\Model\Entity\AnimalType;
use Kocky\Model\Repository\AnimalTypeRepository;

class AnimalTypeService
{
	/** @var  AnimalTypeRepository */
	private $animalTypRepo;
	
	/**
	 * AnimalTypeService constructor.
	 * @param AnimalTypeRepository $animalTypRepo
	 */
	public function __construct(AnimalTypeRepository $animalTypRepo)
	{
		$this->animalTypRepo = $animalTypRepo;
	}
	
	public function add($values)
	{
		$at = new AnimalType();
		$at->name = $values->name;
		$at->popularity = $values->popularity;
		
		$this->animalTypRepo->persistAndFlush($at);
		return $at;
	}
	
	public function edit(AnimalType $at, $values)
	{
		$at->name = $values->name;
		$at->popularity = $values->popularity;
		
		$this->animalTypRepo->persistAndFlush($at);
		return $at;
	}
	
}