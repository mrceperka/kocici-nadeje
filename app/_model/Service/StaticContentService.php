<?php
namespace Kocky\Model\Service;

use Kocky\Model\Entity\StaticContent;
use Kocky\Model\Repository\StaticContentRepository;

class StaticContentService
{
	/** @var  StaticContentRepository */
	private $staticContentRepo;
	
	/**
	 * StaticContentService constructor.
	 * @param StaticContentRepository $staticContentRepo
	 */
	public function __construct(StaticContentRepository $staticContentRepo)
	{
		$this->staticContentRepo = $staticContentRepo;
	}
	
	/**
	 * @param StaticContent $content
	 * @param $values
	 * @return StaticContent
	 */
	public function edit(StaticContent $content, $values)
	{
		$content->text = $values->text;
		$this->staticContentRepo->persistAndFlush($content);
		return $content;
	}
	
}