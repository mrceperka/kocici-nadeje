<?php
namespace Kocky\Model\Service;

use Kocky\Lib\Nette\Security\NextrasUser;
use Kocky\Model\Entity\Contact;
use Kocky\Model\Entity\User;
use Kocky\Model\ModelException;
use Kocky\Model\Repository\ContactRepository;
use Kocky\Model\Repository\UserRepository;

class UserService
{
	/** @var  UserRepository */
	private $userRepo;
	
	/** @var  ContactRepository */
	private $contactRepo;
	
	/** @var  NextrasUser */
	private $user;
	
	/**
	 * UserService constructor.
	 * @param UserRepository $userRepo
	 * @param ContactRepository $contactRepo
	 * @param NextrasUser $user
	 */
	public function __construct(UserRepository $userRepo, ContactRepository $contactRepo, NextrasUser $user)
	{
		$this->userRepo = $userRepo;
		$this->contactRepo = $contactRepo;
		$this->user = $user;
	}
	
	
	public function register($values)
	{
		if ($this->userRepo->getOneByEmail($values->email)) throw new EmailAlreadyUsedException();
		
		$user = new User();
		$user->email = $values->email;
		$user->role = User::ROLE_USER;
		$user->setPassword($values->password);
		
		return $this->userRepo->persistAndFlush($user);
	}
	
	public function addContact(User $user, $values)
	{
		$ct = new Contact();
		$ct->name = $values->name;
		$ct->telephone = $values->telephone;
		$ct->email = $values->email;
		$ct->facebook = $values->facebook;
		$ct->url = $values->url;
		$ct->createdBy = $user;
		
		$user->contacts->add($ct);
		return $this->userRepo->persistAndFlush($user);
	}
	
	public function deleteContact(User $user, Contact $contact)
	{
		$user->contacts->remove($contact);
		$this->userRepo->persistAndFlush($user);
		
		$this->contactRepo->removeAndFlush($contact);
	}
	
	public function setRoles(User $user, $values)
	{
		if (in_array(User::ROLE_ADMIN, $user->role) === true && in_array(User::ROLE_ADMIN, $values->roles) === false) {
			$user->role = array_merge($values->roles, [User::ROLE_ADMIN]);
		} else {
			$user->role = $values->roles;
		}
		
		$user->setAsModified('role');
		return $this->userRepo->persistAndFlush($user);
	}
	
	public function deactivate(User $user)
	{
		$user->deletedAt = 'now';
		return $this->userRepo->persistAndFlush($user);
	}
	
	public function activate(User $user)
	{
		$user->deletedAt = null;
		return $this->userRepo->persistAndFlush($user);
	}
}

class EmailAlreadyUsedException extends ModelException
{
}