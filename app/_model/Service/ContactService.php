<?php
namespace Kocky\Model\Service;

use Kocky\Model\Entity\Contact;
use Kocky\Model\Repository\ContactRepository;

class ContactService
{
	/** @var  ContactRepository */
	private $contactRepo;
	
	/**
	 * ContactService constructor.
	 * @param ContactRepository $contactRepo
	 */
	public function __construct(ContactRepository $contactRepo)
	{
		$this->contactRepo = $contactRepo;
	}
	
	public function edit(Contact $contact, $values)
	{
		$contact->name = $values->name;
		$contact->telephone = $values->telephone;
		$contact->email = $values->email;
		$contact->facebook = $values->facebook;
		$contact->url = $values->url;
		
		$this->contactRepo->persistAndFlush($contact);
		return $contact;
	}
	
}