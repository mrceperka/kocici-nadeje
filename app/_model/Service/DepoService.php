<?php
namespace Kocky\Model\Service;

use Kocky\Lib\Nette\Security\NextrasUser;
use Kocky\Model\AtLeastOneNeeded;
use Kocky\Model\Entity\Address;
use Kocky\Model\Entity\Animal;
use Kocky\Model\Entity\Contact;
use Kocky\Model\Entity\Depo;
use Kocky\Model\Entity\User;
use Kocky\Model\Entity\UserDepo;
use Kocky\Model\ModelException;
use Kocky\Model\Repository\AnimalRepository;
use Kocky\Model\Repository\ContactRepository;
use Kocky\Model\Repository\DepoRepository;
use Kocky\Model\Repository\UserDepoRepository;
use Kocky\Model\Repository\UserRepository;
use Nextras\Orm\Collection\ArrayCollection;

class DepoService
{
	/** @var  NextrasUser */
	private $user;
	
	/** @var  DepoRepository */
	private $depoRepo;
	
	/** @var  UserDepoRepository */
	private $userDepoRepo;
	
	/** @var  ContactRepository */
	private $contactRepo;
	
	/** @var  AnimalRepository */
	private $animalRepo;
	
	/** @var  UserRepository */
	private $userRepo;
	
	/**
	 * DepoService constructor.
	 * @param NextrasUser $user
	 * @param DepoRepository $depoRepo
	 * @param UserDepoRepository $userDepoRepo
	 * @param ContactRepository $contactRepo
	 * @param AnimalRepository $animalRepo
	 * @param UserRepository $userRepo
	 */
	public function __construct(NextrasUser $user, DepoRepository $depoRepo, UserDepoRepository $userDepoRepo, ContactRepository $contactRepo, AnimalRepository $animalRepo, UserRepository $userRepo)
	{
		$this->user = $user;
		$this->depoRepo = $depoRepo;
		$this->userDepoRepo = $userDepoRepo;
		$this->contactRepo = $contactRepo;
		$this->animalRepo = $animalRepo;
		$this->userRepo = $userRepo;
	}
	
	
	public function add($values)
	{
		$ud = new UserDepo();
		$ud->user = $this->user->getEntity();
		$ud->relation = UserDepo::RELATION_OWNER;
		
		
		$depo = new Depo();
		$depo->name = $values->name;
		$depo->capacity = $values->capacity;
		
		$depo->userDepos->add($ud);
		
		$this->depoRepo->persistAndFlush($depo);
		
		return $depo;
	}
	
	public function addAddress(Depo $depo, $values)
	{
		if ($depo->address == null) {
			$adr = new Address();
			$adr->country = $values->country;
			$adr->city = $values->city;
			$adr->street = empty($values->street) ? '' : $values->street;
			$adr->zip = $values->zip;
			
			$depo->address = $adr;
			
			$this->depoRepo->persistAndFlush($depo);
			
			return $adr;
		} else {
			throw new AddressAlreadyExists();
		}
		
	}
	
	public function addMembers(Depo $depo, $values)
	{
		foreach ($values->users as $id) {
			
			$user = $this->userRepo->getById($id);
			
			$udp = new UserDepo();
			$udp->user = $user;
			$udp->depo = $depo;
			$udp->relation = UserDepo::RELATION_MEMBER;
			
			$depo->userDepos->add($udp);
		}
		
		return $this->depoRepo->persistAndFlush($depo);
	}
	
	public function editMemberRelation(UserDepo $udp, $values)
	{
		$udp->relation = $values->relation;
		return $this->userDepoRepo->persistAndFlush($udp);
	}
	
	public function addContact(Depo $depo, $values)
	{
		$ct = new Contact();
		$ct->name = $values->name;
		$ct->telephone = $values->telephone;
		$ct->email = $values->email;
		$ct->facebook = $values->facebook;
		$ct->url = $values->url;
		$ct->createdBy = $this->user->getEntity();
		
		$depo->contacts->add($ct);
		$this->depoRepo->persistAndFlush($depo);
		
		return $depo;
	}
	
	public function addAnimals(Depo $depo, $values)
	{
		if ($depo->animals->count() + count($values->animals) > $depo->capacity) {
			throw new DepoOverflow();
		} else {
			foreach ($values->animals as $animal) {
				$depo->animals->add($animal);
			}
			$this->depoRepo->persistAndFlush($depo);
		}
		
		return $depo;
	}
	
	public function edit(Depo $depo, $values)
	{
		$depo->name = $values->name;
		$depo->capacity = $values->capacity;
		
		$this->depoRepo->persistAndFlush($depo);
		
		return $depo;
	}
	
	public function deleteAnimal(Depo $depo, Animal $animal)
	{
		$depo->animals->remove($animal);
		$this->animalRepo->persistAndFlush($animal);
		
		return $depo;
	}
	
	public function deleteContact(Depo $depo, Contact $contact)
	{
		$depo->contacts->remove($contact);
		$this->depoRepo->persistAndFlush($depo);
		
		$this->contactRepo->removeAndFlush($contact);
	}
	
	public function deleteMember(UserDepo $udp)
	{
		if ($udp->depo->userDepos->count() > 1) {
			$this->userDepoRepo->removeAndFlush($udp);
		} else {
			throw new AtLeastOneNeeded();
		}
	}
	
	
	public function getUsersAddableToDepo(Depo $depo)
	{
		/** @var User[] $users */
		$users = $this->userDepoRepo->findBy(['depo' => $depo])->fetchPairs(null, 'user');
		$ids = [];
		foreach ($users as $user) {
			$ids[] = $user->id;
		}
		
		if ($ids) {
			return $this->userRepo->findBy(['id!=' => $ids]);
		} else {
			return new ArrayCollection([], $this->userRepo);
		}
		
	}
	
	/**
	 * @return DepoRepository
	 */
	public function getDepoRepo()
	{
		return $this->depoRepo;
	}
	
	/**
	 * @return UserDepoRepository
	 */
	public function getUserDepoRepo()
	{
		return $this->userDepoRepo;
	}
}

class AddressAlreadyExists extends ModelException
{
}

class CapacityCannotBeLowered extends ModelException
{
}

class DepoOverflow extends ModelException
{
}