<?php

namespace Kocky\Model\Mapper;

use Kocky\Model\AMapper;
use Kocky\Model\Entity\Depo;

class AnimalMapper extends AMapper
{
	public function getAddableAnimals(Depo $depo)
	{
		$builder = $this->builder()
			->select('*')
			->from('%table', 'a', 'animal')
			->where(
				'(
					a.depo_id IS NULL OR 
					a.depo_id IN (
						SELECT id FROM %table AS udp
						WHERE udp.user_id IN %i[]
					)
				)
				',
				'user_depo',
				$depo->getMembers()->fetchPairs(null, 'id')
			);
		
		
		//select only animals that are not staged in this depo
		if($depo->animals->count()) {
			$aIds = [];
			foreach ($depo->animals as $animal) {
				$aIds[] = $animal->id;
			}
			$builder->andWhere('a.id NOT IN %i[]', $aIds);
		}
		
		return $this->connection->queryArgs(
			$builder->getQuerySql(),
			$builder->getQueryParameters()
		);
	}
	
}
