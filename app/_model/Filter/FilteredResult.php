<?php

namespace Kocky\Model\Filter;

use Kocky\Model\Filter\Strategy\IStrategy;
use Nextras\Dbal\Connection;

class FilteredResult
{
	/** @var  Connection */
	public $conn;
	
	/**
	 * AnimalFilterFactory constructor.
	 * @param Connection $conn
	 */
	public function __construct(Connection $conn)
	{
		$this->conn = $conn;
	}
	
	public function getResult(IStrategy $strategy, $params)
	{
		$qb = $strategy->getFilter($this->conn, $params);
		return $this->conn->queryArgs(
			$qb->getQuerySql(),
			$qb->getQueryParameters()
		);
	}
}