<?php

namespace Kocky\Model\Filter\Strategy;

use Nextras\Dbal\Connection;

class AnimalFilterStrategy implements IStrategy
{
	/**
	 * @param Connection $conn
	 * @param $params
	 * @return \Nextras\Dbal\QueryBuilder\QueryBuilder
	 */
	public function getFilter(Connection $conn, $params)
	{
		$builder = $conn->createQueryBuilder();
		// table useres, aliased a
		$builder->from('users');
		return $builder;
	}
}

interface IAnimalFilterStrategy
{
	/**
	 * @return AnimalFilterStrategy
	 */
	public function create();
}