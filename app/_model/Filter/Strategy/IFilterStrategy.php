<?php
namespace Kocky\Model\Filter\Strategy;
use Nextras\Dbal\Connection;

interface IStrategy
{
	/**
	 * @param Connection $conn
	 * @param $params
	 * @return \Nextras\Dbal\QueryBuilder\QueryBuilder
	 */
    public function getFilter(Connection $conn, $params);
}