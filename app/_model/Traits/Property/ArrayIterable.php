<?php

namespace Kocky\Model\Traits\Property;

/**
 * Class ArrayIterable
 * @package Kocky\Model\Traits\Property
 */
trait ArrayIterable
{
	protected $__iterator = 0;
	protected $__arrayName = 'data';

	/**
	 *
	 */
	function rewind() {
		$this->__iterator = 0;
	}

	/**
	 * @return mixed
	 */
	function current() {
		return $this->{$this->__arrayName}[$this->__iterator];
	}

	/**
	 * @return int
	 */
	function key() {
		return $this->__iterator;
	}

	/**
	 *
	 */
	function next() {
		++$this->__iterator;
	}

	/**
	 * @return bool
	 */
	function valid() {
		return isset($this->{$this->__arrayName}[$this->__iterator]);
	}

}