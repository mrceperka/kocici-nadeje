<?php
namespace Kocky\Model\Traits\Entity;


trait DeletableEntity
{
	/**
	 * Delete current Entity and write it to database
	 */
	public function delete($now = TRUE)
	{
		$this->setValue('deletedAt', 'now');
		
		if($now) {
			$this->getRepository()->persistAndFlush($this);
		}
	}
	
	/**
	 * @return bool	if Entity is deleted
	 */
	public function isDeleted()
	{
		return $this->deletedAt !== null;
	}
	
}