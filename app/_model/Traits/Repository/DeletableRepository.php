<?php
namespace Kocky\Model\Traits\Repository;
/**
 * Class DeletableRepository
 * @package App\Model\Traits\Repository
 */
trait DeletableRepository
{
	
	/**
	 * @return \Nextras\Orm\Collection\ICollection
	 */
	public function findAllWithDeleted()
	{
		return parent::findAll();
	}
	
	/**
	 * @return \Nextras\Orm\Collection\ICollection
	 */
	public function findAll()
	{
		return parent::findAll()->findBy(['deletedAt' => null]);
	}
}