<?php
namespace Kocky\Model;


/**
 * Class Orm
 * @package Kocky\Model\Orm
 *
 * @property-read Repository\UserRepository $userRepo
 * @property-read Repository\DepoRepository $depoRepo
 * @property-read Repository\UserDepoRepository $userDepoRepo
 * @property-read Repository\AnimalRepository $animalRepo
 * @property-read Repository\AnimalTypeRepository $animalTypeRepo
 * @property-read Repository\HandicapRepository $handicapRepo
 * @property-read Repository\UserImageRepository $userImageRepo
 * @property-read Repository\ContactRepository $contactRepo
 * @property-read Repository\AddressRepository $addressRepo
 * @property-read Repository\StoryRepository $storyRepo
 * @property-read Repository\StaticContentRepository $staticContentRepo
 *
 *
 */
class Orm extends \Nextras\Orm\Model\Model
{
	
}