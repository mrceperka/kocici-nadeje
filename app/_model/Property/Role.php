<?php

namespace Kocky\Model\Property;


use Kocky\Model\ModelException;
use Kocky\Model\Traits\Property\ArrayIterable;
use Nextras\Orm\Entity\IEntity;
use Nextras\Orm\Entity\IPropertyContainer;
use Nextras\Orm\Entity\Reflection\PropertyMetadata;

/**
 * Class Set
 * @package Kocky\Model\Property
 */
class Role implements IPropertyContainer, \Iterator, \JsonSerializable
{
	use ArrayIterable;
	
	/**
	 * @var array[]
	 */
	private $data = [];
	
	/**
	 * Set constructor.
	 *
	 * @param IEntity $entity
	 * @param PropertyMetadata $propertyMetadata
	 */
	public function __construct(IEntity $entity, PropertyMetadata $propertyMetadata)
	{
		
	}
	
	/**
	 *
	 */
	public function getRawValue()
	{
		return rtrim(implode(",", $this->data), ',');
	}
	
	/**
	 * @param mixed $value
	 *
	 * @throws \Nette\Utils\JsonException
	 */
	public function setRawValue($value)
	{
		
		if ($value) {
			$this->data = explode(",", $value);
		}
	}
	
	/**
	 * @inheritDoc
	 */
	public function setInjectedValue($value)
	{
		if (is_array($value) || $value instanceof \JsonSerializable) {
			$this->data = $value;
		} else {
			$this->setRawValue($value);
		}
	}
	
	
	/**
	 * @inheritDoc
	 */
	public function &getInjectedValue()
	{
		return $this->data;
	}
	
	/**
	 * @inheritDoc
	 */
	public function hasInjectedValue()
	{
		return true;
	}
	
	
	/**
	 * @inheritDoc
	 */
	function jsonSerialize()
	{
		return $this->data;
	}
	
	
}


class RoleException extends ModelException
{
	
}