<?php
namespace Kocky\Model\Entity;

use Kocky\Model\AEntity;
use Kocky\Model\Property\Role;
use Kocky\Model\Traits\Entity\DeletableEntity;
use Nette\Security\Passwords;
use Nextras\Orm\Collection\ArrayCollection;
use Nextras\Orm\Relationships\ManyHasMany;
use Nextras\Orm\Relationships\OneHasMany;

/**
 * Primary
 * @property int 				$id                            {primary}
 * Properties
 * @property string 			$email
 * @property-read string 		$password
 * @property string|null        $role                          {container Role}
 *
 * Relationship
 * @property OneHasMany|UserDepo[]	$userDepos                 {1:m UserDepo::$user}
 * @property OneHasMany|Animal[]	$createdAnimals                 {1:m Animal::$createdBy}
 * @property OneHasMany|Contact[]	$createdContacts                 {1:m Contact::$createdBy}
 * @property OneHasMany|UserImage[]   $images                        {1:m UserImage::$user}
 * @property OneHasMany|Story[]   $stories                        {1:m Story::$createdBy}
 * @property ManyHasMany|Contact[]   $contacts               {m:n Contact::$users, isMain=true}
 *
 *
 * Create
 * @property-read \DateTime 	$createdAt                     {default 'now'}
 * Delete
 * @property \DateTime|NULL		$deletedAt
 */
class User extends AEntity
{
	const ROLE_USER = 'user';
	const ROLE_ADMIN = 'admin';
	const ROLE_MODERATOR = 'moderator';
	const ROLE_DEPOZITOR = 'depozitor';
	
	use DeletableEntity;
	
	/**
	 *
	 * @param $password
	 */
	public function setPassword($password)
	{
		$this->setReadOnlyValue('password', Passwords::hash($password));
	}
	
	/**
	 * @param string $password in plaintext to verify with entity password
	 * @return bool
	 */
	public function verifyPassword($password) {
		if($this->getProperty('password') == false) {
			throw new \LogicException('Password is not set');
		}
		
		return Passwords::verify($password, $this->getProperty('password'));
	}
	
	public function ownsDepo(Depo $depo = null)
	{
		foreach($this->userDepos as $udp) {
			if($depo !== null) {
				if($udp->depo->id === $depo->id) return true;
			} else {
				if ($udp->relation === UserDepo::RELATION_OWNER) return true;
			}
		}
		return false;
	}
	
	public function getDepos()
	{
		$result = [];
		foreach($this->userDepos as $udp) {
			$result[] = $udp->depo;
		}
		
		return new ArrayCollection($result, $this->getModel()->depoRepo);
	}
	
	public function ownsDepoWithFreeCapacity()
	{
		foreach($this->userDepos as $udp) {
			if($udp->depo->animals->count() < $udp->depo->capacity && $udp->relation === UserDepo::RELATION_OWNER) {
				return true;
			}
		}
		return false;
	}
}