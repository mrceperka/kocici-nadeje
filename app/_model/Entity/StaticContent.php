<?php
namespace Kocky\Model\Entity;


use Kocky\Model\AEntity;

/**
 * Primary
 * @property int					                $id			{primary}
 *
 * Properties
 * @property string|null					        $text
 * @property int					        		$section
 * @property int									$position		{default 1}
 *
 * Update
 * @property \DateTime|NULL		$updatedAt
 */
class StaticContent extends AEntity
{
	const HEADER = 1;
	const RIGHT_SIDEBAR = 2;
	const LEFT_SIDEBAR = 3;
	const FOOTER = 4;
	
}