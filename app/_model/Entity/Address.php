<?php

namespace Kocky\Model\Entity;

use Kocky\Model\AEntity;
use Kocky\Model\Traits\Entity\DeletableEntity;

/**
 * Primary
 * @property int 				$id                            {primary}
 *
 * Properties
 * @property string				$country
 * @property string 			$city
 * @property string 			$street
 * @property string 			$zip
 *
 * Relationship
 * Create
 * @property-read \DateTime 	$createdAt                     {default 'now'}
 * Delete
 * @property \DateTime|NULL		$deletedAt
 */
class Address extends AEntity
{
	const COUNTRY_CS = 'CS';
	const COUNTRY_SK = 'SK';
	
	use DeletableEntity;
	
	/*
	public function setterCountry($value)
	{
		if(!in_array($value, [self::COUNTRY_CS, self::COUNTRY_SK])) throw new \InvalidArgumentException($value . ' not allowed');
		$this->setRawValue('country', $value);
	}
	*/
}