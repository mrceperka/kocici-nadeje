<?php

namespace Kocky\Model\Entity;


use Kocky\Model\AEntity;
use Kocky\Model\Traits\Entity\DeletableEntity;
use Nextras\Orm\Relationships\ManyHasMany;
use Nextras\Orm\Relationships\ManyHasOne;

/**
 * Primary
 * @property int $id            {primary}
 *
 * Properties
 * @property string|null $name
 * @property string|null $telephone
 * @property string|null $email
 * @property string|null $facebook
 * @property string|null $url
 *
 *
 * Relations
 * @property ManyHasMany|User[] $users        {m:n User::$contacts}
 * @property ManyHasOne|User		$createdBy						{m:1 User::$createdContacts}
 *
 * Create
 * @property-read \DateTime $createdAt                     {default 'now'}
 * Delete
 * @property \DateTime|NULL $deletedAt
 */
class Contact extends AEntity
{
	use DeletableEntity;
}