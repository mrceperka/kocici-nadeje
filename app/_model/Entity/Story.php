<?php
namespace Kocky\Model\Entity;


use Kocky\Model\AEntity;
use Kocky\Model\Traits\Entity\DeletableEntity;
use Nextras\Orm\Relationships\ManyHasOne;

/**
 * Primary
 * @property int					                $id			{primary}
 *
 * Properties
 * @property string					                $text
 * @property int									$type   {default self::ANIMAL_STORY}
 *
 * Relations
 * @property ManyHasOne|User		$createdBy					{m:1 User::$stories}
 *
 * Create
 * @property-read \DateTime 	$createdAt                     {default 'now'}
 *
 * Update
 * @property \DateTime|NULL 	$updatedAt
 *
 * Delete
 * @property \DateTime|NULL		$deletedAt
 */
class Story extends AEntity
{
	const ANIMAL_STORY = 1;
	
	use DeletableEntity;
}