<?php
namespace Kocky\Model\Entity;


use CPTeam\Image\Saver\Traits\TSrcGetter;
use Kocky\Model\AEntity;
use Kocky\Model\Traits\Entity\DeletableEntity;
use Nextras\Orm\Relationships\ManyHasOne;

/**
 * Primary
 * @property int 				$id                            {primary}
 *
 * Properties
 * @property string 				$hash
 * @property string					$name
 * @property string					$type						{default self::TYPE_ORIGINAL}
 * @property int|null              $sourceX
 * @property int|null              $sourceY
 * @property int|null              $sourceWidth
 * @property int|null              $sourceHeight
 *
 * Relationship
 * @property User|ManyHasOne       $user				{m:1 User::$images}
 * Create
 * @property-read \DateTime 	$createdAt                     {default 'now'}
 * Delete
 * @property \DateTime|NULL		$deletedAt
 */
class UserImage extends AEntity
{
	const TYPE_ORIGINAL = 'original';
	use DeletableEntity;
}