<?php
namespace Kocky\Model\Entity;


use Kocky\Model\AEntity;
use Kocky\Model\Traits\Entity\DeletableEntity;
use Nextras\Orm\Collection\ArrayCollection;
use Nextras\Orm\Relationships\ManyHasMany;
use Nextras\Orm\Relationships\ManyHasOne;

/**
 * Primary
 * @property int 				$id                            {primary}
 *
 * Properties
 * @property string 			$name
 * @property int 				$age
 * @property int 				$gender							{default self::GENDER_UNKNOWN}
 * @property string 			$state							{default self::STATE_NEW}
 * @property bool 				$castrated						{default false}
 *
 * Relationship
 * @property ManyHasOne|Depo|null	$depo                 		{m:1 Depo::$animals}
 * @property ManyHasOne|AnimalType|null	$animalType               {m:1 AnimalType::$animals}
 * @property ManyHasMany|Handicap[]	$handicaps                   {m:n Handicap::$animals, isMain=true}
 * @property ManyHasOne|User		$createdBy						{m:1 User::$createdAnimals}
 * @property ManyHasMany|Story[]		$stories					{m:n Story, isMain=true, oneSided=true}
 * @property ManyHasMany|UserImage[]	$images                   {m:n UserImage, isMain=true, oneSided=true}
 * @property ManyHasOne|UserImage|null	$profileImage                   {m:1 UserImage, oneSided=true}
 *
 * Create
 * @property-read \DateTime 	$createdAt                     {default 'now'}
 * Delete
 * @property \DateTime|NULL		$deletedAt
 */
class Animal extends AEntity
{
	const GENDER_UNKNOWN = 0;
	const GENDER_MALE = 1;
	const GENDER_FEMALE = 2;
	
	const STATE_NEW = 'new';
	const STATE_TAKEAWAY_READY = 'takeaway_ready';
	const STATE_VIRTUAL_READY = 'virtual_ready';
	const STATE_PLACED = 'placed';

	use DeletableEntity;
	
	public function getHandicaps()
	{
		$result = [];
		foreach($this->handicaps as $hd) {
			$result[] = $hd;
		}
		
		return new ArrayCollection($result, $this->getModel()->handicapRepo);
	}
	
	public function getEstimatedAge()
	{
		//now - createdAt + age
		return (new \DateTime())->format('Y') - $this->createdAt->format('Y') + $this->age;
	}
	
	public function isInDepo(Depo $depo)
	{
		if($this->depo === null) {
			return false;
		} else {
			return $this->depo->id == $depo->id;
		}
	}
	
}