<?php
namespace Kocky\Model\Entity;


use Kocky\Model\AEntity;
use Kocky\Model\Traits\Entity\DeletableEntity;
use Nextras\Orm\Relationships\ManyHasOne;

/**
 * Primary
 * @property int					                $id			{primary}
 *
 * Properties
 * @property int					                $relation   {default self::RELATION_MEMBER}
 *
 * Relations
 * @property User|ManyHasOne		                $user		{m:1 User::$userDepos}
 * @property Depo|ManyHasOne		            	$depo		{m:1 Depo::$userDepos}
 *
 * Create
 * @property-read \DateTime 	$createdAt                     {default 'now'}
 * Delete
 * @property \DateTime|NULL		$deletedAt
 */
class UserDepo extends AEntity
{
	const RELATION_OWNER = 1;
	const RELATION_MEMBER = 2;
	
	use DeletableEntity;
}