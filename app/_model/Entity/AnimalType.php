<?php
namespace Kocky\Model\Entity;


use Kocky\Model\AEntity;
use Kocky\Model\Traits\Entity\DeletableEntity;
use Nextras\Orm\Relationships\OneHasMany;

/**
 * Primary
 * @property int 				$id                            {primary}
 *
 * Properties
 * @property string 			$name
 * @property int				$popularity						{default 1}
 *
 * Relationship
 * @property OneHasMany|Animal[]|null	$animals                {1:m Animal::$animalType}
 * Create
 * @property-read \DateTime 	$createdAt                     {default 'now'}
 * Delete
 * @property \DateTime|NULL		$deletedAt
 */
class AnimalType extends AEntity
{
	use DeletableEntity;
}