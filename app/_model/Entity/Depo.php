<?php
namespace Kocky\Model\Entity;


use Kocky\Model\AEntity;
use Kocky\Model\Traits\Entity\DeletableEntity;
use Nextras\Orm\Collection\ArrayCollection;
use Nextras\Orm\Relationships\ManyHasMany;
use Nextras\Orm\Relationships\OneHasMany;
use Nextras\Orm\Relationships\OneHasOne;

/**
 * Primary
 * @property int $id                            {primary}
 *
 * Properties
 * @property string $name
 * @property int $capacity
 *
 * Relationship
 * @property OneHasMany|UserDepo[] $userDepos                 {1:m UserDepo::$depo}
 * @property OneHasMany|Animal[] $animals                   {1:m Animal::$depo}
 * @property OneHasOne|Address|null $address                    {1:1 Address, isMain=true, oneSided=true}
 * @property ManyHasMany|Contact[]	$contacts				{m:n Contact, isMain=true, oneSided=true}
 *
 * Create
 * @property-read \DateTime $createdAt                     {default 'now'}
 * Delete
 * @property \DateTime|NULL $deletedAt
 */
class Depo extends AEntity
{
	
	use DeletableEntity;
	
	/**
	 * @return ArrayCollection
	 */
	public function getOwners()
	{
		$result = [];
		foreach ($this->userDepos as $udp) {
			if($udp->relation == UserDepo::RELATION_OWNER) {
				$result[] = $udp->user;
			}
		}
		return new ArrayCollection($result, $this->getModel()->userRepo);
	}
	
	public function hasOwner(User $user)
	{
		/** @var User $owner */
		foreach ($this->getOwners() as $owner) {
			if ($user->id == $owner->id) return true;
		}
		return false;
	}
	
	public function getMembers()
	{
		$result = [];
		foreach ($this->userDepos as $udp) {
			$result[] = $udp->user;
		}
		return new ArrayCollection($result, $this->getModel()->userRepo);
	}
	
	public function hasMember(User $user)
	{
		/** @var User $member */
		foreach ($this->getMembers() as $member) {
			if ($user->id == $member->id) return true;
		}
		return false;
	}
	
	public function getAddableAnimals()
	{
		return $this->getModel()->animalRepo->getAddableAnimals($this);
	}
	
	public function freeCapacity()
	{
		return $this->capacity - $this->animals->count();
	}
	
	
}