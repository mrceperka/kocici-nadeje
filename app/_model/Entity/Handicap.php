<?php
namespace Kocky\Model\Entity;


use Kocky\Model\AEntity;
use Kocky\Model\Traits\Entity\DeletableEntity;
use Nextras\Orm\Relationships\ManyHasMany;

/**
 * Primary
 * @property int 				$id                            {primary}
 *
 * Properties
 * @property string 			$name
 * @property int 				$severity
 * @property string|null 		$description
 *
 * Relationship
 * @property ManyHasMany|Animal[]	$animals                   {m:n Animal::$handicaps}
 *
 * Create
 * @property-read \DateTime 	$createdAt                     {default 'now'}
 * Delete
 * @property \DateTime|NULL		$deletedAt
 */
class Handicap extends AEntity
{
	
	use DeletableEntity;
	
	
}