<?php
namespace Kocky\HandicapModule\Presenters;

use Kocky\Components\Control\AnimalType\IHandicapControlFactory;
use Kocky\Model\Entity\Handicap;
use Kocky\Model\Repository\HandicapRepository;
use Nette\Application\BadRequestException;

class DefaultPresenter extends BasePresenter
{
	
	/** @var  HandicapRepository @inject */
	public $handicapRepo;
	
	/** @var  IHandicapControlFactory @inject */
	public $handicapControlFactory;
	
	/** @var  Handicap */
	public $handicap;
	
	public function actionAdd()
	{
		if(!$this->user->isInRole('admin')) {
			throw new BadRequestException(403, 'Forbidden');
		}
	}
	
	public function actionList()
	{
		$this->template->handicaps = $this->handicapRepo->findAll();
	}
	
	public function actionEdit($id)
	{
		$this->handicap = $this->handicapRepo->getById($id);
		if(!$this->handicap) {
			throw new BadRequestException(404, 'Handicap does not exist');
		}
	}
	
	public function renderEdit($id)
	{
		$this->template->handicap = $this->handicap;
	}
	
	public function createComponentAddHandicap()
	{
		return $this->handicapControlFactory->create();
	}
	
	public function createComponentEditHandicap()
	{
		$hdc = $this->handicapControlFactory->create();
		$hdc->setHandicap($this->handicap);
		return $hdc;
	}
}