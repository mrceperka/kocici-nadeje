<?php
namespace Kocky\HandicapModule\Presenters;

abstract class BasePresenter extends \Kocky\CommonModule\Presenters\BasePresenter
{
	public function startup()
	{
		parent::startup();
		
		if(!$this->user->isInRole('admin')) {
			$this->redirect(':Common:Homepage:default');
		}
	}
}