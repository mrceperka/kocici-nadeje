<?php

namespace Kocky\AccountModule\Presenters;



use Kocky\Components\Control\Account\Contact\IUserContactListControlFactory;

class DefaultPresenter extends BasePresenter
{
	/** @var  IUserContactListControlFactory @inject */
	public $userContactListControlFactory;
	
	public function actionDefault()
	{
		
	}
	
	public function createComponentContactList()
	{
		return $this->userContactListControlFactory->create();
	}
}
