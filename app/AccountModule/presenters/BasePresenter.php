<?php

namespace Kocky\AccountModule\Presenters;



abstract class BasePresenter extends \Kocky\CommonModule\Presenters\BasePresenter
{
	public function startup()
	{
		parent::startup();
		if(!$this->user->isLoggedIn()) {
			$this->redirect(':Common:Homepage:default');
		}
	}
}
