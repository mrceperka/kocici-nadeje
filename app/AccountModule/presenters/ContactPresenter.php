<?php

namespace Kocky\AccountModule\Presenters;


use Kocky\Components\Control\Account\Contact\AE\IUserContactAEControlFactory;
use Kocky\Model\Entity\Contact;
use Kocky\Model\Repository\ContactRepository;
use Kocky\Model\Service\UserService;
use Nette\Application\BadRequestException;

class ContactPresenter extends BasePresenter
{
	/** @var  IUserContactAEControlFactory @inject */
	public $userContactControlFactory;
	
	/** @var ContactRepository @inject */
	public $contactRepo;
	
	/** @var  UserService @inject */
	public $userService;
	
	/** @var  Contact */
	public $contact;
	
	public function actionAdd()
	{
		
	}
	
	public function actionEdit($id)
	{
		$this->contact = $this->contactRepo->getById($id);
		if (!$this->contact) {
			throw new BadRequestException('Contact does not exist');
		}
		if (!$this->authorizator->isAllowed($this->user, $this->contact, 'edit')) {
			$this->flashMessage('Nemáte takový kontakt', 'error');
			$this->redirect(':Account:Default:default');
		}
	}
	
	public function createComponentAddUserContact()
	{
		return $this->userContactControlFactory->create();
	}
	
	public function createComponentEditUserContact()
	{
		$ucc = $this->userContactControlFactory->create();
		$ucc->setContact($this->contact);
		return $ucc;
	}
}
