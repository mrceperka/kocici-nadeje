<?php
namespace Kocky\AnimalModule\Presenters;

use Kocky\Components\Control\Animal\AE\IAnimalAEControlFactory;
use Kocky\Components\Control\Animal\Roster\IAnimalRosterControlFactory;
use Kocky\Components\Control\Gallery\GalleryControl;
use Kocky\Components\Control\Gallery\IGalleryControlFactory;
use Kocky\Components\Control\Story\IStoryControlFactory;
use Kocky\Components\Control\Story\StoryControl;
use Kocky\Components\Filter\Animal\AnimalFilterControl;
use Kocky\Components\Filter\Animal\IAnimalFilterControlFactory;
use Kocky\Model\Entity\Animal;
use Kocky\Model\Repository\AnimalRepository;
use Kocky\Model\Service\StoryService;
use Nette\Application\BadRequestException;
use Nette\Application\UI\Form;
use Nette\Forms\ISubmitterControl;
use Nextras\Orm\Collection\ArrayCollection;
use Nextras\Orm\Collection\ICollection;

class DefaultPresenter extends BasePresenter
{
	/** @var  Animal */
	public $animal;
	
	/** @var ICollection */
	public $animals;
	
	/** @var  IAnimalAEControlFactory @inject */
	public $animalControlFactory;
	
	/** @var  AnimalRepository @inject */
	public $animalRepo;
	
	/** @var  IAnimalFilterControlFactory @inject */
	public $animalFilterControlFactory;
	
	/** @var IStoryControlFactory @inject */
	public $storyControlFactory;
	
	/** @var  StoryService @inject */
	public $storyService;
	
	/** @var  IGalleryControlFactory @inject */
	public $galleryControlFactory;
	
	/** @var  IAnimalRosterControlFactory @inject */
	public $animalListControlFactory;
	
	
	public function actionDefault($id)
	{
		$this->animal = $this->animalRepo->getById($id);
		if (!$this->animal) throw new BadRequestException('Animal not found');
	}
	
	public function actionList()
	{
		$this->animals = new ArrayCollection(
			$this->animalRepo->findAll()->fetchPairs('id'),
			$this->animalRepo
		);
	}
	
	public function actionAdd()
	{
		if ($this->authorizator->isAllowed($this->user, 'animal', 'add') === false) {
			$this->flashMessage('Nedostatečná oprávnění', 'error');
			$this->redirect('list');
		} else {
			if ($this->user->getEntity()->ownsDepoWithFreeCapacity() === false && $this->user->isInRole('admin') === false) {
				$this->flashMessage('Všechna vaše depozita jsou plná', 'error');
				$this->redirect('list');
			}
		}
	}
	
	public function actionEdit($id)
	{
		$this->animal = $this->animalRepo->getById($id);
		if (!$this->animal) throw new BadRequestException('Animal not found');
		if (!$this->authorizator->isAllowed($this->user, $this->animal, 'edit')) {
			$this->flashMessage('Nedostatečná oprávnění', 'error');
			$this->redirect('default', $id);
		}
	}
	
	public function actionFilter()
	{
		
	}
	
	
	public function renderDefault($id)
	{
		$this->template->animal = $this->animal;
	}
	
	public function renderEdit($id)
	{
		$this->template->animal = $this->animal;
	}
	
	
	public function createComponentAddAnimal()
	{
		return $this->animalControlFactory->create();
	}
	
	public function createComponentEditAnimal()
	{
		$anc = $this->animalControlFactory->create();
		$anc->setAnimal($this->animal);
		return $anc;
	}
	
	public function createComponentFilter()
	{
		/** @var AnimalFilterControl $filter */
		$filter = $this->animalFilterControlFactory->create();
		$filter->getComponent('filter')->onSuccess[] = function (Form $form) {
			$submit = $form->isSubmitted();
			if ($submit instanceof ISubmitterControl) {
				if ($submit->getName() === 'submit') {
					$qb = \Kocky\Components\Filter\Animal\QueryBuilder::build($form, $this->animalRepo);
					$this->animals = new ArrayCollection(
						$this->animalRepo->getMapper()->toCollection($qb)->fetchPairs('id'),
						$this->animalRepo
					);
				} else {
					$this->redirect('this', []);
				}
			} else {
				$this->redirect('this', []);
			}
		};
		return $filter;
	}
	
	public function createComponentAnimalStory()
	{
		/** @var StoryControl $storyControl */
		$storyControl = $this->storyControlFactory->create();
		$storyControl->setAnimal($this->animal);
		$storyControl->getForm()->onSuccess[] = function (Form $form) {
			if ($this->authorizator->isAllowed($this->user, $this->animal, 'edit')) {
				$this->storyService->addAnimalStory($this->animal, $form->getValues());
				$this->flashMessage('Informace přidána');
			} else {
				$this->flashMessage('Nedostatečná oprávnění', 'error');
			}
			$this->redirect('this');
		};
		return $storyControl;
	}
	
	public function createComponentAnimalGallery()
	{
		/** @var GalleryControl $galleryControl */
		$galleryControl = $this->galleryControlFactory->create();
		$galleryControl->setAnimal($this->animal);
		return $galleryControl;
	}
	
	public function createComponentAnimalList()
	{
		$control = $this->animalListControlFactory->create();
		$control->setAnimals($this->animals);
		return $control;
	}
}