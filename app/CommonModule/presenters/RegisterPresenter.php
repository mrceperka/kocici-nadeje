<?php

namespace Kocky\CommonModule\Presenters;
use Kocky\Components\Form\BaseForm;
use Kocky\Components\Form\Register\RegisterFormFactory;
use Kocky\Model\Service\EmailAlreadyUsedException;
use Kocky\Model\Service\UserService;

class RegisterPresenter extends BasePresenter
{
	/** @var  RegisterFormFactory @inject */
	public $registerFormFactory;
	
	/** @var  UserService @inject */
	public $userService;
	
	public function actionDefault()
	{
		
	}
	
	public function createComponentRegisterForm()
	{
		$form = $this->registerFormFactory->create();
		$form->onSuccess[] = function (BaseForm $form) {
			try {
				$this->userService->register($form->getValues());
				$this->redirect(':Common:Homepage:default');
			} catch (EmailAlreadyUsedException $e) {
				$form->addCommonError('Zadaný email je již používán');
			}
		};
		return $form;
	}
}