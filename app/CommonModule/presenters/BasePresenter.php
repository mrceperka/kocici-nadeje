<?php

namespace Kocky\CommonModule\Presenters;

use Kocky\Components\Control\StaticContent\IStaticContentControlFactory;
use Kocky\Lib\Authenticator\BaseAuthorizator;
use Kocky\Model\Orm;
use Kocky\Model\Repository\StaticContentRepository;
use Nette\Application\UI\Multiplier;
use Nextras\Application\UI\SecuredLinksPresenterTrait;

abstract class BasePresenter extends \Nette\Application\UI\Presenter
{

	use SecuredLinksPresenterTrait;

	/** @var \Kocky\Model\Orm */
	public $orm;

	/** @var BaseAuthorizator */
	public $authorizator;

	/** @var StaticContentRepository */
	public $staticContentRepo;

	/** @var  IStaticContentControlFactory */
	public $staticContentFactory;

	public function startup()
	{
		parent::startup();
		$this->orm = $this->context->getByType(Orm::class);
		$this->authorizator = $this->context->getByType(BaseAuthorizator::class);
		$this->staticContentRepo = $this->context->getByType(StaticContentRepository::class);
		$this->staticContentFactory = $this->context->getByType(IStaticContentControlFactory::class);
	}

	public function beforeRender()
	{
		parent::beforeRender();
		$this->template->authorizator = $this->authorizator;
		$this->template->domain = $this->context->getParameters()['domain'];
	}

	public function createComponentStaticContent()
	{
		return new Multiplier(function ($section) {
			$exploded = explode('_', $section);
			if (count($exploded) < 2) {
				throw new \LogicException('Explode failed');
			}
			list($section, $position) = $exploded;

			if ($section === 'footer') {
				$content = $this->staticContentRepo->getFooterSection($position);
			} else {
				throw new \LogicException('Not supported yet');
			}
			$control = $this->staticContentFactory->create();
			$control->setContent($content);

			return $control;
		});
	}
}