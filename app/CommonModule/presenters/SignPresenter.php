<?php

namespace Kocky\CommonModule\Presenters;


use Kocky\Components\Form\BaseForm;
use Kocky\Components\Form\Login\LoginFormFactory;
use Kocky\Lib\Authenticator\BaseAuthenticator;
use Nette\Security\AuthenticationException;

class SignPresenter extends BasePresenter
{
	/** @var  LoginFormFactory @inject */
	public $loginFormFactory;
	
	/** @var  BaseAuthenticator @inject */
	public $authenticator;
	
	public function actionIn()
	{
		
	}
	
	public function actionOut()
	{
		$this->user->logout(true);
		$this->redirect(':Common:Homepage:default');
	}
	
	public function createComponentLoginForm()
	{
		$form = $this->loginFormFactory->create();
		$form->onSuccess[] = function (BaseForm $form) {
			try {
				$identity = $this->authenticator->authenticate($form->getValues(true));
				$this->user->login($identity);
				$this->redirect(':Common:Homepage:default');
			} catch (AuthenticationException $e) {
				$form->addCommonError('Přihlášení se nezdařilo, neplatný email nebo heslo');
			}
		};
		
		return $form;
	}
}