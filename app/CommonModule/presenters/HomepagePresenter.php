<?php

namespace Kocky\CommonModule\Presenters;

use Kocky\Components\Control\Animal\Teaser\IAnimalTeaserControlFactory;
use Kocky\Model\Repository\AnimalRepository;

class HomepagePresenter extends BasePresenter
{
	/** @var  IAnimalTeaserControlFactory @inject */
	public $animalTeaserControlFactory;
	
	/** @var  AnimalRepository @inject */
	public $animalRepo;
	
	public function createComponentAnimalTeaser()
	{
		$atc = $this->animalTeaserControlFactory->create();
		$atc->setAnimals($this->animalRepo->findLatestWithPicture());
		return $atc;
	}
}
