#!/usr/bin/env bash

composer install

writableDir() {
    if [ -e $1 ]
    then
        echo "$1 Already exists\n"
    else
        mkdir $1 -m 775
    fi

    chmod 775 -R $1
    chown dockeruser:dockeruser -R $1

    cd $1 && umask 002
}

ROOT=/var/www/kocky

TEMP_FPM_DIR=$ROOT/temp
TEMP_CLI_DIR=$ROOT/tempcli
LOG_DIR=$ROOT/log
MIGRATIONS_DIR=$ROOT/migrations

ADMINER_PATH=$ROOT/adminer.php
ADMINER_WWW_PATH=$ROOT/www/adminer.php

writableDir $TEMP_FPM_DIR
writableDir $TEMP_CLI_DIR
writableDir $LOG_DIR
writableDir $MIGRATIONS_DIR

writableDir $TEMP_FPM_DIR/cache
writableDir $TEMP_CLI_DIR/cache

cd $ROOT && composer install --no-interaction


rm -rf $TEMP_FPM_DIR/cache/*
rm -rf $TEMP_CLI_DIR/cache/*


if [ $DEBUG_MODE = "1" ]
then
	if [ -e $ADMINER_WWW_PATH ]; then
		echo "Adminer already exists"
	else
		ln -s $ADMINER_PATH $ADMINER_WWW_PATH
	fi
else
	if [ -e $ADMINER_WWW_PATH ]; then
		rm $ADMINER_WWW_PATH
	fi
fi

if [ $DEBUG_MODE = "1" ]; then
    echo "Debug Mode: ENABLED"
else
    echo "Debug Mode: DISABLED"
fi



php www/index.php migrations:continue

php-fpm