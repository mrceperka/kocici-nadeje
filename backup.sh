#!/usr/bin/env bash
source ./.env
source ./.env.local

docker exec -it kocky-db mysqldump -u${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE}
