<?php
require_once 'colors.php';
require_once 'helpers.php';

echo yellow('|==================================') . PHP_EOL;

if(!isset($_SERVER['argv'][1])) {
	echo red('Pass path to configuration file or pass JSON directly') . PHP_EOL;
	exit;
}

$jsonFileOrJson = $_SERVER['argv'][1];
$json = false;
if(file_exists($jsonFileOrJson) && is_file($jsonFileOrJson) && is_readable($jsonFileOrJson)) {
	$json = json_decode(file_get_contents($jsonFileOrJson));
} else {
	$json = json_decode($jsonFileOrJson);
}

if($json === null) {
	echo red('Passed JSON is not a valid JSON') . PHP_EOL;
	exit;
}


/** @var mysqli $link */
$link = mysqli_connect(
	getenv('KOCKY_MYSQL_HOST'),
	getenv('KOCKY_MYSQL_USER'),
	getenv('KOCKY_MYSQL_PASSWORD')
);

while(!$link) {
	echo 'Error: Unable to connect to MySQL.' . PHP_EOL;
	echo 'Debugging errno: ' . mysqli_connect_errno() . PHP_EOL;
	echo 'Debugging error: ' . mysqli_connect_error() . PHP_EOL;
	echo 'Will try again after 1 second' . PHP_EOL;
	sleep(1);
	$link = mysqli_connect(
		getenv('KOCKY_MYSQL_HOST'),
		getenv('KOCKY_MYSQL_USER'),
		getenv('KOCKY_MYSQL_PASSWORD'),
		getenv('KOCKY_MYSQL_DATABASE')
	);
}
$sql = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '" . getenv('KOCKY_MYSQL_DATABASE') . "';";
$r = $link->query($sql);
if ($r->num_rows > 0) {
	echo blue('[DB]') . ' Database: ' . getenv('KOCKY_MYSQL_DATABASE') . ' exists!' . PHP_EOL;
} else {
	echo blue('[DB]') . ' Creating database: ' . getenv('KOCKY_MYSQL_DATABASE') . PHP_EOL;
	$sql = 'CREATE DATABASE IF NOT EXISTS ' . getenv('KOCKY_MYSQL_DATABASE') . ';';
	$r = $link->query($sql);
	echo info($r !== false, '[DB]') . PHP_EOL;
}
echo blue('[DB][SELECTED]') . ' ' . getenv('KOCKY_MYSQL_DATABASE') . PHP_EOL;
$link->select_db(getenv('KOCKY_MYSQL_DATABASE'));

if($json->schema !== null && $json->schema->init === true) {
	echo blue('[CONFIG][schema][init]') . ' true' . PHP_EOL;
	echo blue('[CONFIG][schema][file]') . ' Trying to init sql from "' . $json->schema->file . '"' . PHP_EOL;
	
	$path = $json->schema->file;
	if(is_string($path)) {
		if(file_exists($path) && is_readable($path) && is_file($path)) {
			$sql = file_get_contents($path);
			$r = $link->multi_query($sql);
			echo info($r !== false, '[CONFIG][schema][file][query]') . PHP_EOL;
		} else {
			echo red('[CONFIG][schema][file]') . ' File: "' . $path . '" not found' . PHP_EOL;
			exit;
		}
	} else {
		echo red('[CONFIG][schema][file]') . ' Must be a string' . PHP_EOL;
		exit;
	}
} else {
	echo yellow("Skipping config->schema") . PHP_EOL;
}

if($json->symlinks && is_array($json->symlinks)) {
	foreach ($json->symlinks as $symlink) {
		if(is_string($symlink->src) && is_string($symlink->dst)) {
			if(file_exists(__DIR__ . DIRECTORY_SEPARATOR . $symlink->src)) {
				echo green('Source ' . $symlink->src . ' OK') . PHP_EOL;
				if(file_exists(__DIR__ . DIRECTORY_SEPARATOR . $symlink->dst)) {
					echo yellow('Destination ' . $symlink->dst . ' already exists') . PHP_EOL;
				} else {
					$src = __DIR__ . DIRECTORY_SEPARATOR . $symlink->src;
					$dst = __DIR__ . DIRECTORY_SEPARATOR . $symlink->dst;
					echo blue('[CONFIG][symlinks] Creating ' . $src . ' to ' . $dst) . PHP_EOL;
					shell_exec('ln -s ' . $src . ' ' . $dst);
				}
			}
		} else {
			echo red('[CONFIG][symlinks] "src" and "dst" has to be set') . PHP_EOL;
		}
	}
} else {
	echo yellow("Skipping config->symlinks") . PHP_EOL;
}

echo 'Exiting...' . PHP_EOL;
mysqli_close($link);

echo yellow('|==================================') . PHP_EOL;