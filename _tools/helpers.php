<?php
require_once 'colors.php';
function info($predicate, $prefix, $successText = 'Success', $failureText = 'Failure') {
	if($predicate) {
		return blue($prefix) . ' ' . green($successText);
	} else {
		return blue($prefix) . ' ' . red($failureText);
	}
}