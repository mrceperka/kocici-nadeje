<?php
const RED = "\e[31m";
const BLUE = "\e[34m";
const GREEN = "\e[32m";
const YELLOW = "\e[33m";
const DEFAULT_COLOR = "\e[39m";

function red($string)
{
	return RED . $string . DEFAULT_COLOR;
}

function blue($string)
{
	return BLUE . $string . DEFAULT_COLOR;
}

function green($string)
{
	return GREEN . $string . DEFAULT_COLOR;
}

function yellow($string)
{
	return YELLOW . $string . DEFAULT_COLOR;
}