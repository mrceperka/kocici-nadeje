#!/usr/bin/env bash

ACTION=$1

DOCKER_TOOLS_VERSION=1.5.3

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ ! -e $DIR/_docker/tools/functions.sh ]
then
	echo "Docker-Tools missing!"
	echo "Download docker-tools"
	git clone https://gitlab.com/Luky/docker.git $DIR/_docker/tools >> $DIR/docker.sh.log
fi

cd $DIR/_docker/tools

git remote -v >> $DIR/docker.sh.log
git pull origin tags/$DOCKER_TOOLS_VERSION >> $DIR/docker.sh.log

cd $DIR

source _docker/tools/functions.sh

touchFile ./.env.local
touchFile ./app/config/config.local.neon

# -----------------------------------------------------------------------------------------------------
# Parse Arguments
# -----------------------------------------------------------------------------------------------------

OPT_FORCE=0
OPT_BUILD=0

OPT_FOLLOW=1

OPT_PRODUCTION=0

for var in "$@"
do
	case "$var" in
		-n|--no-follow)
			OPT_FOLLOW=0
			;;
		--production)
			OPT_PRODUCTION=1
			;;
		-f|--force)
			OPT_FORCE=1
			;;
		-b|--build)
			OPT_BUILD=1
			;;
		*)
			;;
	esac
done

DOCKER_COMPOSE_FILE=0

if [[ $OPT_PRODUCTION == "1" ]]; then
	DOCKER_COMPOSE_FILE=$DIR/dockerCompose.yml
else
	DOCKER_COMPOSE_FILE=$DIR/dockerCompose.yml
fi


# -----------------------------------------------------------------------------------------------------
# Base Actions
# -----------------------------------------------------------------------------------------------------

initAction $ACTION
actionExec $*

# -----------------------------------------------------------------------------------------------------
# Run Action
# -----------------------------------------------------------------------------------------------------

if [[ $ACTION == "run" ]]
then
	dockerComposeStop $OPT_FORCE

	injector app root www-data
	injector nginx root www-data
	#injector node root www-data

	if [[ $OPT_BUILD == "1" ]]; then
	    e "System" "Build containers"
        docker-compose -f $DOCKER_COMPOSE_FILE build

		if [ $DOCKER_HUB_LOGIN = 0 ] || [ $DOCKER_HUB_PASSWORD = 0 ]; then
			e "Ship" "Missing credentials in config.sh. Ship skipped."
		else
			docker login -u $DOCKER_HUB_LOGIN -p $DOCKER_HUB_PASSWORD
			NAME=kocky/app
      TAG=gitlab
      FINAL=${NAME}:${TAG}
      docker build ./_docker/app --tag ${FINAL}
      docker push ${FINAL}
	    docker logout
		fi
	fi


	dockerComposeUp

	dockerIntranet kocky-nginx --ip 172.21.92.2

	if [[ $OPT_FOLLOW == "1" ]]; then
		dockerLogs 10
	fi


	exit 0
fi


e "System" "Unknown argument '$ACTION', valid are these:"
e "System" "  run"
e "System" "  stop"
e "System" "  stopall"
e "System" "  network"
e "System" ""
exit 1